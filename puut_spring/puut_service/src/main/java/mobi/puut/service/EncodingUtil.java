package mobi.puut.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.springframework.stereotype.Service;

@Service
public class EncodingUtil {
    
    public String hash(String str)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] message = str.getBytes("utf-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(message, 0, message.length);
        byte messageDigest[] = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < messageDigest.length;i++) {
            String h = Integer.toHexString(0xFF & messageDigest[i]);
            while (h.length() < 2) {
                h = "0" + h;
            }
            hexString.append(h);
        }

        return hexString.toString();
    }
    
}