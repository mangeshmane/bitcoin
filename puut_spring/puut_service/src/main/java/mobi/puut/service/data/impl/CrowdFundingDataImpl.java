package mobi.puut.service.data.impl;

import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.CrowdFundingData;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crowdfunding.Area;
import mobi.puut.service.entities.crowdfunding.CrowdList;
import mobi.puut.service.entities.crowdfunding.Phase;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;



@Repository("crowdFundingData")
public class CrowdFundingDataImpl implements CrowdFundingData {
    
    
    @Autowired
    SessionFactory session;
    
    //@Autowired
    ServicesUtil servicesUtil=new ServicesUtil();
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Transactional (rollbackFor=Exception.class)
    public void add(CrowdList crowdList) {
        this.init();
        
        session.getCurrentSession().save(crowdList);
    }
    
    @Transactional (rollbackFor=Exception.class)
    public List getCrowdFounding(User user) {
        this.init();
        
        return session.getCurrentSession()
                .createQuery("from CrowdList where user = :user and expiration >= :date")
                .setParameter("user", user)
                .setParameter("date", servicesUtil.getCurrentDateTime())
                .list();
    }
    @Transactional (rollbackFor=Exception.class)
    public List getPreviousCrowd(User user) {
        this.init();
        
        return session.getCurrentSession()
                .createQuery("from CrowdList where user = :user and expiration < :date")
                .setParameter("user", user)
                .setParameter("date", servicesUtil.getCurrentDateTime())
                .list();
    }
    
    @Transactional (rollbackFor=Exception.class)
    public List getProjecttitle(Long id) {
        this.init();

        return session.getCurrentSession()
                .createQuery("from CrowdList where id = :id")
                .setParameter("id", id)
                .list();
    }
    
    @Transactional (rollbackFor=Exception.class)
    public boolean checkTitle(String projecttitle) {
        this.init();
        
        CrowdList crowdList = (CrowdList) session.getCurrentSession()
                .createQuery("from CrowdList where projecttitle = :projecttitle")
                .setParameter("projecttitle", projecttitle)
                .uniqueResult();
        if(crowdList==null)
            return false;
        else
            return true;
    }
    @Transactional (rollbackFor=Exception.class)
    public boolean checkphone(String phone) {
        this.init();
        
        CrowdList crowdList = (CrowdList) session.getCurrentSession()
                .createQuery("from CrowdList where phone = :phone")
                .setParameter("phone", phone)
                .uniqueResult();
        if(crowdList==null)
            return false;
        else
            return true;
    }
    
    @Transactional (rollbackFor=Exception.class)
    public AccountSetting getAccountSettings(User user) {
        this.init();
        
        AccountSetting setting = (AccountSetting) session.getCurrentSession()
                .createQuery("from AccountSetting where user = :user")
                .setParameter("user", user)
                .uniqueResult();
        return setting;
    }
    
    @Transactional (rollbackFor=Exception.class)
    public List<Phase> getPhase() {
        this.init();
        
        return session.getCurrentSession()
                .createQuery("from Phase")
                .list();
    }
    
    @Transactional (rollbackFor=Exception.class)
    public List<Area> getArea() {
        this.init();

        return session.getCurrentSession()
                .createQuery("from Area")
                .list();
    }
    
    @Transactional (rollbackFor=Exception.class)
    public List<Currency> getCurrency() {
        this.init();

        return session.getCurrentSession()
                .createQuery("from Currency")
                .list();
    }
    
    @Transactional (rollbackFor=Exception.class)
    public CrowdList getDifferentDate(Long id) {
        this.init();

        CrowdList crowdList = (CrowdList) session.getCurrentSession()
                .createQuery("from CrowdList where id = :id")
                .setParameter("id", id)
                .uniqueResult();
        return crowdList;
    }
}
