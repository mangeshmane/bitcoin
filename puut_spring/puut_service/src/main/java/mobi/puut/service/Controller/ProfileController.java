/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.mail.MessagingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Login;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.ProfileService;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Development
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @RequestMapping(value = "/login", method = RequestMethod.OPTIONS )
    public ResponseEntity handle() {
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public User getProfile() throws IOException {
        try {
            return profileService.getProfile();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/editprofile", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result editProfile(@RequestBody User request)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        try {
            return profileService.editProfile(request);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Login checkLogin() {
        try {
            return profileService.checkLogin();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

   
    @RequestMapping(value = "/remind/{login:.+}", method = RequestMethod.PUT)
    public  @ResponseBody Result remindPassword(@PathVariable String login) throws MessagingException {
        try {
            return profileService.remindPassword(login);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @RequestMapping(value = "/remind", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON)
    public  Result remindPassword1(@RequestBody User user) throws MessagingException {
        try {
            return profileService.remindPassword1(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }
    
    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/settings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public AccountSetting getProfileSettings() {
        try {
            return profileService.getProfileSettings();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/settings", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result saveProfileSettings(@RequestBody AccountSetting settings) {
        try {
            return profileService.saveProfileSettings(settings);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/preference", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Preference getProfilePreference() {
        try {
            return profileService.getProfilePreference();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/preference", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public Result saveProfilePreference(@RequestBody Preference preference) {
        try {
            return profileService.saveProfilePreference(preference);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/languages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Language> getLanguages() {
        try {
            return profileService.getLanguages();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }

    }
	

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/accountpicture", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadAccountPicture(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadAccountPicture(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/uploadid", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadUploadIds(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadUploadIds(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/ubills", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadUbill(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadUbill(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/socialc", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadSocialcs(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadSocialcs(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/coverimg", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadCoverimage(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadCoverimage(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/dlicense", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadDlicenses(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadDlicenses(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/upload/passport", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA)
    public Response uploadPassports(@RequestParam("file") MultipartFile uploadedInputStream) throws IOException {
        try {
            return profileService.uploadPassports(uploadedInputStream);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/update/pin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result updatePin(@RequestBody User user) {
        try {
            return profileService.updatePin(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/update/password", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result updatePassword(@RequestBody User user) {
        try {
            return profileService.updatePassword(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/submitpin/{pin}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    public Result verifyPin(@PathVariable("pin") final String pin) {
        try {
            return profileService.verifyPin(pin);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(ProfileController.class).error(exception);
            throw new Error(exception);
        }
    }

 
}
