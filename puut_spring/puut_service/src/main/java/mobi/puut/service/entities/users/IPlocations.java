package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="iplocations")
public class IPlocations implements Serializable {
	
	private static final long serialVersionUID=1L;
	
	@Id
	@Column(name="geoname_id",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	
	@NotNull
	@Column(name="cityname")
	private String city_name;
	
	public String getCity_name() {
		return city_name;
	}


	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}


	@NotNull
	@Column(name="locale_code")
	private String locale_code;
	
	@NotNull
	@Column(name="continent_code")
	private char continent_code;
	
	
	@NotNull
	@Column(name="continent_name")
	private String continent_name;
	
	
	@NotNull
	@Column(name="country_iso_code")
	private char country_iso_code;
	
	
	@NotNull
	@Column(name="country_name")
	private String country_name;
	
	
	@NotNull
	@Column(name="subdivision1_iso_code")
	private String subdivision1_iso_code;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLocale_code() {
		return locale_code;
	}


	public void setLocale_code(String locale_code) {
		this.locale_code = locale_code;
	}


	public char getContinent_code() {
		return continent_code;
	}


	public void setContinent_code(char continent_code) {
		this.continent_code = continent_code;
	}


	public String getContinent_name() {
		return continent_name;
	}


	public void setContinent_name(String continent_name) {
		this.continent_name = continent_name;
	}


	public char getCountry_iso_code() {
		return country_iso_code;
	}


	public void setCountry_iso_code(char country_iso_code) {
		this.country_iso_code = country_iso_code;
	}


	public String getCountry_name() {
		return country_name;
	}


	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}


	public String getSubdivision1_iso_code() {
		return subdivision1_iso_code;
	}


	public void setSubdivision1_iso_code(String subdivision1_iso_code) {
		this.subdivision1_iso_code = subdivision1_iso_code;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
