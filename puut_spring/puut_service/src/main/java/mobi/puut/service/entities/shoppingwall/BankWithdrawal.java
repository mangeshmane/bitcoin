package mobi.puut.service.entities.shoppingwall;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlRootElement;

import mobi.puut.service.entities.cloudmoney.Currency;

//@XmlRootElement
public class BankWithdrawal {
    
    private String swift;
    private String beneficiary;
    private String beneficiary_address;
    private String iban_number;
    private String bic_code;
    private String account;
    private String transfer_type;
    private BigDecimal amount;
    private Currency currency;
    private String purpose;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public String getBeneficiary_address() {
        return beneficiary_address;
    }

    public void setBeneficiary_address(String beneficiary_address) {
        this.beneficiary_address = beneficiary_address;
    }

    public String getBic_code() {
        return bic_code;
    }

    public void setBic_code(String bic_code) {
        this.bic_code = bic_code;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getIban_number() {
        return iban_number;
    }

    public void setIban_number(String iban_number) {
        this.iban_number = iban_number;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }

    public String getTransfer_type() {
        return transfer_type;
    }

    public void setTransfer_type(String transfer_type) {
        this.transfer_type = transfer_type;
    }
}
