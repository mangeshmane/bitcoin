/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.ws.rs.core.MediaType;

import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crowdfunding.Area;
import mobi.puut.service.entities.crowdfunding.CrowdList;
import mobi.puut.service.entities.crowdfunding.Phase;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/crowdfunding")
public class CrowdFundingController {

    @Autowired
    mobi.puut.service.services.CrowdFundingService crowdFundingService;

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/currencies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Currency> getCurrency() {
        try {
            return crowdFundingService.getCurrency();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/crowdlist", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public void add(@RequestBody CrowdList crowdList) {
        try {
            crowdFundingService.add(crowdList);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/crowdfundings", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public List getCrowdFounding(@RequestBody User user) {
        try {
            return crowdFundingService.getCrowdFounding(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/previouscrowd", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public List getPreviousCrowd(@RequestBody User user) {
        try {
            return crowdFundingService.getPreviousCrowd(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/projecttitle/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public List getProjecttitle(@PathVariable("id") Long id) {
        try {
            return crowdFundingService.getProjecttitle(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/checktitle/{projecttitle}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON, produces=MediaType.APPLICATION_JSON)
    public boolean getProjecttitle(@PathVariable("projecttitle") String projecttitle) {
        try {
            return crowdFundingService.checkTitle(projecttitle);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/checkphone/{phone}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON, produces=MediaType.APPLICATION_JSON)
    public boolean checkphone(@PathVariable("phone") String phone) {
        try {
            return crowdFundingService.checkphone(phone);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/settings", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public AccountSetting getAccountSettings(@RequestBody User user) {
        try {
            return crowdFundingService.getAccountSettings(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/phases", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Phase> getPhase() {
        try {
            return crowdFundingService.getPhase();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/areas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Area> getArea() {
        try {
            return crowdFundingService.getArea();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/differentdate/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public CrowdList getDifferentDate(@PathVariable("id") Long id) {
        try {
            return crowdFundingService.getDifferentDate(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/socialfinance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public Result getSocialFinance() {
        try {
            return crowdFundingService.getSocialFinance();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/currentcampaign", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List getCurrentCampaign() {
        try {
            return crowdFundingService.getCurrentCampaign();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/viewproject/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List viewProject(@PathVariable("id") Long id) {
        try {
            return crowdFundingService.viewProject(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/invest/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public CrowdList invest(@PathVariable("id") Long id) {
        try {
            return crowdFundingService.invest(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/invest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result investPost(@RequestBody Transaction transaction) {
        try {
            return crowdFundingService.investPost(transaction);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/newcampaign", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result newcampaign(@RequestBody CrowdList list) {
        try {
            return crowdFundingService.newcampaign(list);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium')")
    @RequestMapping(value = "/previouscampaign", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List previouscampaign() {
        try {
            return crowdFundingService.previouscampaign();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(CrowdFundingController.class).error(exception);
            throw new Error(exception);
        }

    }
}
