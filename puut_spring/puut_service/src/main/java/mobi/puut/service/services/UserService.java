package mobi.puut.service.services;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;

import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;


public interface UserService {

//    @GET
//    @Path("getall")
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<User> getUsers();

 
    User getUser(@PathParam("id") Long id);

    Personal updateImage(MultipartFile file);
    Coverimage updateCoverImage(MultipartFile file);
    
    

//    
//    
//    
//    @POST
//    @Path("adduser")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    Response add(User user);
//    
//    @POST
//    @Path("register")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    Response register(@Context HttpServletRequest request,User user);
//    																									
//    @POST
//    @Path("test")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    String test(@Context HttpServletRequest request);
//    																							
//    
//    @GET
//    @Path("getByUsername/{username}")
//    @Produces(MediaType.APPLICATION_JSON)
//    User getByUsername(@PathParam("username") String username);
//    
//    @PUT
//    @Path("update/{id}")
//    @Produces(MediaType.APPLICATION_JSON)      
//    Response update(@PathParam("id") Long id,User user);
//    
//    
//   @GET
//   @Path("getRole/{username}")
//   @Produces(MediaType.APPLICATION_JSON)
//   UserRole getRole(@PathParam("username") User user);
//    
//   
//   
//      
//   
  
}
