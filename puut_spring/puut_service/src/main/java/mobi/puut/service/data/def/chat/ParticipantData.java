package mobi.puut.service.data.def.chat;

import java.util.List;

import mobi.puut.service.entities.chat.Participant;
import mobi.puut.service.entities.users.User;

public interface ParticipantData {

	 public void save(Participant p);
	 public void update(Participant p);
	 public Participant find(long id);
	 public List<Participant> findAll();
	 public List<Participant> findByConv(long convId);
	 public List<Participant> findWithConvAndUser(long convId, long userId);
	 public Integer countParticipants(long convId);
	 public List<Participant> findByUser(User user);
	 
	 
	
}
