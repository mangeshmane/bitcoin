package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="accountsettings")
public class AccountSetting  implements Serializable{

	private static final long serialVersionUID=1L;
	
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="user")
	private User user;

	
	@Column(name="firstname")
	private Boolean firstname;
	
	@Column(name="middlename")
	private Boolean middlename;
	
	@Column(name="lastname")
	private Boolean lastname;
	
	@Column(name="phone")
	private Boolean phone;
	
	@Column(name="birthdate")
	private Boolean birthdate;

	@Column(name="gender")
	private Boolean gender;
	
	@Column(name="country")
	private Boolean country;
	
	@Column(name="puutnumber")
	private Boolean puutnumber;
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="notification")
	private Notification notification;
	
	@Transient
	private Language language;

	@Transient
	private Preference preference;

	public Long getId() {
		return id;
	}
	
	

	public Notification getNotification() {
		return notification;
	}



	public void setNotification(Notification notification) {
		this.notification = notification;
	}



	public Language getLanguage() {
		return language;
	}



	public void setLanguage(Language language) {
		this.language = language;
	}



	public Preference getPreference() {
		return preference;
	}



	public void setPreference(Preference preference) {
		this.preference = preference;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getFirstname() {
		return firstname;
	}

	public void setFirstname(Boolean firstname) {
		this.firstname = firstname;
	}

	public Boolean getMiddlename() {
		return middlename;
	}

	public void setMiddlename(Boolean middlename) {
		this.middlename = middlename;
	}

	public Boolean getLastname() {
		return lastname;
	}

	public void setLastname(Boolean lastname) {
		this.lastname = lastname;
	}

	public Boolean getPhone() {
		return phone;
	}

	public void setPhone(Boolean phone) {
		this.phone = phone;
	}

	public Boolean getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Boolean birthdate) {
		this.birthdate = birthdate;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Boolean getCountry() {
		return country;
	}

	public void setCountry(Boolean country) {
		this.country = country;
	}

	public Boolean getPuutnumber() {
		return puutnumber;
	}

	public void setPuutnumber(Boolean puutnumber) {
		this.puutnumber = puutnumber;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
