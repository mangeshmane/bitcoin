
package mobi.puut.service.data;


import java.util.List;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.ApiKeys;
import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Dlicenses;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Passports;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.ResetPassword;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.Socialcs;
import mobi.puut.service.entities.users.Ubill;
import mobi.puut.service.entities.users.Uploadids;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;


public interface UserData {
	
	public void addUser(User user);
	public List<User> getAllUsers();
	public User getById(Long id);
	public User getByUsername(String name);   
    public void updateById(User user);
    public void delete(User user);
    public UserRole getRole(User username);
    public Passports getPassports(User user);
	public User getUserByPhone(String phone);
	public Cryptoinfo getDefualtWallet(User phoneUser);
	public void updateImage(Personal personal);
	public void updateCoverImage(Coverimage coverimage);
	public User get(Long user);
	public Personal getPersonal(User user);
	public ApiKeys getApiKeyByUser(String key);
	public void add(AccountSetting settings);
	public AccountSetting getSettings(User user);
	public Role getRole(String username);
	public User getUser(Long valueOf);
	public List<User> getByTerm(String term);
    public List<User> getByTermBis(String term);
	public void edit(User user);
	public User getUserByPuutNumber(String puut_number);
	public void editCoverimage(Coverimage coverimage);
	public Dlicenses getDlicenses(User user);
    public Ubill getUbill(User user);
    public Socialcs getSocialcs(User user);
    public void editUploadids(Uploadids uploadids); 
    public void editPersonal(Personal personal);
    public void editPassports(Passports passports);
    public void add(ResetPassword resetPassword);
    public Preference getPreference(Preference preference);
    public Preference getPreference(User user);
    public void edit(AccountSetting account_setting);
    public void addPreference(Preference preference);
    public Uploadids getUploadids(User user);
    public void editSocialcs(Socialcs socialcs);
    public void editDlicenses(Dlicenses dlicenses);
    public Coverimage getCoverimage(User user);
    public void editUbill(Ubill ubill);
    public User getUserByPhone(String phone, String login);
	List<User> getUsersByRole(Long role_id);
	  public Language getLanguageById(Long id);
	public void add(User user);
	public void add(UserRole userrole);
}
