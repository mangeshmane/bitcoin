package mobi.puut.service.services.crypto.def;

import java.text.ParseException;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mobi.puut.service.crypto.util.WalletModel;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crypto.btc.SendMoney;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.GenerateWallet;





public interface WalletService {


	public Result generateAddress(GenerateWallet generateWallet);
	public List getWalletTransactionById(long id);
	public List<Cryptoinfo> getAllWallets();
	 public Cryptoinfo getWalletInfo(Long walletId);
	 public Result sendMoney(final Long walletId,final SendMoney sendMoney);
	 public Result deleteWalletInfoById(Long id);
	 public String getWalletBalanceById(long id);
	 public String getWalletsCount();
	 public Cryptoinfo getWalletInfoByCurrencyAndAddress(String currencyName, String address);
	 public Cryptoinfo updateDefaultWallet(Long walletid);
	 public List getWalleTransactionsById(long id) ;
	public List<Currency> currencys();

	public WalletModel getWalletModel(Long walletId);
	public List getTransactionsByMonth(long id, String date) throws ParseException;
	 public List<Cryptostatus> getTransactionsBetweenDates(long id, String startDate, String endDate) throws ParseException;
	 
}
