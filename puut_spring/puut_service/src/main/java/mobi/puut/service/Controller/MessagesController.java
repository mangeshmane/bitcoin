/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.services.MessagesService;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/messages")
public class MessagesController {

    @Autowired
    private MessagesService messagesService;

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/inbox/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Message> getInbox(@PathVariable("id") Long id) {
        try {
            return messagesService.getInbox(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(MessagesController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/sent/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Message> getSent(@PathVariable("id") Long id) {
        try {
            return messagesService.getSent(id);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(MessagesController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/send", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public Result sendMessage(@RequestBody Message message) {
        try {
            return messagesService.sendMessage(message);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(MessagesController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/getUnreadMesseges", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List getUnreadMessages() {
        try {
            return messagesService.getUnreadMessages();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(MessagesController.class).error(exception);
            throw new Error(exception);
        }

    }

     @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient')")
    @RequestMapping(value = "/getunreadmessagescount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public String getUnreadMessagesCount() {
        try {
            return messagesService.getUnreadMessagesCount();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(MessagesController.class).error(exception);
            throw new Error(exception);
        }

    }

}
