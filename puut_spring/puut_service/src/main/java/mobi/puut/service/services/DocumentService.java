package mobi.puut.service.services;

import java.util.List;

import javax.ws.rs.core.Response;

import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Document;

public interface DocumentService{

	public List<Type>  getTypes();
	
	public List<Subtype>  getSubTypes(Long type_id);
	
	public List<Category>  getCategories(Long type_id);
	
	public List<Status>  getStatuses();
	
	public List<Availability> getAvailabilities();
	
	public List<Document> getDocument(Long type);
	
	public List<Document> getFriendBusinessCard(Long id);
	
	public Result shareDocument(Long id,Long status);
	
	public List<Document> getDocumentsByAvailability(Long type,Long availability);
	
	public List<Document> getDocumentsByStatus(Long type,Long status);
	
	public List<Document> getValidDocuments(Long type);
	
	public List<Document> getExpiredDocuments(Long type);
	
	public Response getLinearCode(Long id);
	
	public Response getQRCode(Long id);
	
	
	public Document getGraphicImage(Long document_id,int width,int height);

	
	public Response uploadFile(String name,
			String cslogan,
			String designation,
			String cname,
			String address,
			String phone,
			String mobile,
			String fax,
			String email,
			String town,
			String web,
			String post,
			String login
			);
	
	
	public Response createDocument(Document document);
	
	public Response createDocument(Long id,Document document);
	
	public Response addCart(Long id,String name,String number);
	
	
	
	
}
