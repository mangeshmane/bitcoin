package mobi.puut.service.services.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.CloudMoneyData;
import mobi.puut.service.data.DocumentData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.BankWithdrawal;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.CloudMoneyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service
public class CloudMoneyServiceImpl implements CloudMoneyService {

    @Autowired
    CloudMoneyData cloudMoneyData;

    @Autowired
    UserData usersData;

    @Autowired
    DocumentData documentsData;

    @Autowired
    ServicesUtil servicesUtil;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    public List<Currency> getCurrencies() {
        this.init();

        return cloudMoneyData.getCurrencies();
    }

    public List<User> getAgents() {
        this.init();

        return usersData.getUsersByRole(6L);
    }

    public List<CloudMoney> getAccountBalance() {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return cloudMoneyData.getBalance(login);
    }

    public List<Transaction> getTransactions() {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        List<Transaction> history = cloudMoneyData.getTransactions(login);

        for (Transaction trans : history) {
            if (trans.getUserfrom().getUsername().equals(login)) {
                trans.setAmount("-" + trans.getAmount());
                trans.setPuutnumber(trans.getUserto().getPuutnumber());
            } else {
                trans.setPuutnumber(trans.getUserfrom().getPuutnumber());
            }
        }

        return history;
    }

    public List<Document> getBills() {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return documentsData.getDocuments(login, 10L, 2L);
    }

    public List<Document> getCreditCards() {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        List<Document> list = documentsData.getDocuments(login, 1L, new Date());

        for (Document doc : list) {
            doc.setUserpuutnumber(doc.getUser().getPuutnumber());
            doc.setUserlogin(doc.getUser().getUsername());
        }

        return list;
    }

    public Result sendMoney(Transaction transaction) {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        Result result = new Result();
        result.setResult(servicesUtil.getSendMoneyErrors(transaction));

        if (result.getSuccess()) {
            result = cloudMoneyData.sendMoney(transaction, result, login);
        }

        return result;
    }

    public Result withdrawMoney(BankWithdrawal bankWithdrawal){
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        Result result = new Result();
        result.setResult(servicesUtil.getWithdrawalErrors(bankWithdrawal));

        if (result.getSuccess()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat fileFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            Date date = new Date();
            String current = dateFormat.format(date);
            String file_current = fileFormat.format(date);

            StringBuilder line = new StringBuilder();
            line.append(login).append(",");
            line.append(bankWithdrawal.getSwift()).append(",");
            line.append(bankWithdrawal.getBeneficiary()).append(",");
            line.append(bankWithdrawal.getBeneficiary_address()).append(",");
            line.append(bankWithdrawal.getIban_number()).append(",");
            line.append(bankWithdrawal.getBic_code()).append(",");
            line.append(bankWithdrawal.getAccount()).append(",");
            line.append(bankWithdrawal.getTransfer_type()).append(",");
            line.append(bankWithdrawal.getAmount().toPlainString()).append(",");
            line.append(bankWithdrawal.getCurrency().getSymbol()).append(",");
            line.append(current).append(",");
            line.append(bankWithdrawal.getPurpose());

            try {
            String directory = (new File(".")).getCanonicalPath() + "\\webapps\\puut_service\\withdraw";
            File file = new File(directory);
            if (!file.exists()) {
                if (file.mkdirs()) {
                    file = new File(directory + "\\" + file_current + ".csv");
                    if (file.createNewFile()) {
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(line.toString());
                        bw.close();
                    } else {
                        result.setError("WITHDRAWALERROR");
                        result.setSuccess(false);
                    }
                } else {
                    result.setError("WITHDRAWALERROR");
                    result.setSuccess(false);
                }
            }
            }catch(Exception e)
            {

            }
           
        }
        

        return result;
    }

}
