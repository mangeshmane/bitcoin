package mobi.puut.service.crypto.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Repository;

@Repository("serviceUtil")
public class ServiceUtil {

	public String formatTransactionDateTime(String updatedTime) throws ParseException {
		// TODO Auto-generated method stub
		
		Date formatteddate=null;
		String finalDate=null;
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy",Locale.ENGLISH);
		formatteddate=dateFormat.parse(updatedTime);
		DateFormat newDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		finalDate=newDateFormat.format(formatteddate);
		return finalDate;
		
		
		
	
	}

}
