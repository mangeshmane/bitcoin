package mobi.puut.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

@Service("patternUtil")
public class PatternUtil {

    private Pattern pattern;
    private Matcher matcher;

    public PatternUtil() {
    }

    public boolean validate(final String text, final String expression) {
        pattern = Pattern.compile(expression);
        matcher = pattern.matcher(text);
        return !matcher.matches();
    }
    
}