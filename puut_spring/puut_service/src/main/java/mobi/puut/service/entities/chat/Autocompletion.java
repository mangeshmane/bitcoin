/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.entities.chat;

/**
 *
 * @author Development
 */
public class Autocompletion {
    private String fullnames;
    private String identifier;
    
    public String getFullnames(){
        return fullnames;
    }
    
    public void setFullnames(String fullnames){
        this.fullnames = fullnames;
    }
    
    public String getIdentifier(){
        return identifier;
    }
    
    public void setIdentifier(String identifier){
        this.identifier = identifier;
    }
}
