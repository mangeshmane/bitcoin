package mobi.puut.service;

import java.util.Date;
import java.util.Random;
import javax.annotation.PostConstruct;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service
public class PuutUtil {

    @Autowired
    UserData usersData;

    private String characters = "0123456789";
    private int lenght_from = 10;
    private int lenght_till = 12;
    private int attempts = 5;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    private String getRandomString(int lenght) {
        this.init();

        Random rnd = new Random(new Date().getTime());

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lenght; i++) {
            int index = rnd.nextInt(9);
            builder.append(characters.substring(index, index + 1));
        }

        return builder.toString();
    }

    public Boolean checkPuutNumber(String puut_number) {
        this.init();

        User user = usersData.getUserByPuutNumber(puut_number);

        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public String generatePuutNumber() {
        this.init();

        int current = lenght_from;
        int step = 0;

        while (current <= lenght_till) {
            while (step < attempts) {
                String result = getRandomString(current);
                if (checkPuutNumber(result)) {
                    step = step + 1;
                } else {
                    return result;
                }
            }
            step = 0;
            current = current + 1;
        }

        return "";
    }
}
