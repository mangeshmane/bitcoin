package mobi.puut.service.services.impl;

import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.CloudMoneyData;
import mobi.puut.service.data.CrowdFundingData;
import mobi.puut.service.data.DocumentData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crowdfunding.Area;
import mobi.puut.service.entities.crowdfunding.CrowdList;
import mobi.puut.service.entities.crowdfunding.Phase;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@Service("crowdFundingService")
public class CrowdFundingServiceImpl implements mobi.puut.service.services.CrowdFundingService {
  
    @Autowired
    CrowdFundingData crowdFundingData; 
    
    @Autowired
    CloudMoneyData cloudMoneyData;
    
    @Autowired
    UserData usersData;
    
    @Autowired
    DocumentData documentsData;
    
    @Autowired
    ServicesUtil servicesUtil;

    public List<Currency> getCurrency() {
        this.init();
        
        return crowdFundingData.getCurrency();
    }
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }    

    public void add(CrowdList crowdList) {
        this.init();
        crowdFundingData.add(crowdList);
    }
    
    public List getCrowdFounding(User user){
        this.init();
        return crowdFundingData.getCrowdFounding(user);
    }
    
    public List getPreviousCrowd(User user){
        this.init();
        return crowdFundingData.getPreviousCrowd(user);
    }
    
    public List getProjecttitle(Long id){
        this.init();
        return crowdFundingData.getProjecttitle(id);
    }
    
    public boolean checkTitle(String projecttitle){
        this.init();
        return crowdFundingData.checkTitle(projecttitle);
    }
    
    public boolean checkphone(String phone){
        this.init();
        return crowdFundingData.checkphone(phone);
    }

    public AccountSetting getAccountSettings(User user){
        this.init();
         return crowdFundingData.getAccountSettings(user);
    }   

    public List<Phase> getPhase(){
        this.init();
        return crowdFundingData.getPhase();
    }

    public List<Area> getArea(){
        this.init();
        return crowdFundingData.getArea();
    }

    public CrowdList getDifferentDate(Long id){
        this.init();
        return crowdFundingData.getDifferentDate(id);
    }

   
    public mobi.puut.service.entities.documents.Result getSocialFinance() {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        Role role = usersData.getRole(login);
        
        Result result = new Result();
                
        if(role.getName().equals("puutclient")) {
            result.setError("ACCESSDENIED");
            result.setSuccess(false);
        } else {
            result.setSuccess(true);
        }
        
        return result;
    }

    @Override
    public List getCurrentCampaign() {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
        
        if(user!=null) {
            List list = getCrowdFounding(user);
            return list;
        }
        
        return null;
    }

    @Override
    public List viewProject(Long id) {
        this.init();
        return getProjecttitle(id);
    }

    @Override
    public CrowdList invest(Long id) {
        this.init();
        return crowdFundingData.getDifferentDate(id);
    }

    @Override
    public Result investPost(Transaction transaction) {
        this.init();
        
        User to = transaction.getUserto();
        
        Result result = new Result();
                
        result = cloudMoneyData.sendMoney(transaction, result, to.getUsername());

        return result;
    }

    @Override
    public Result newcampaign(CrowdList crowdList) {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
        
        Result result = new Result();
        
        result.setSuccess(false);
        
        if(crowdList.getProjeclocation().isEmpty()){
            result.setError("projeclocation may not be emty");
            return result;
        }
        if(crowdList.getPromotername().isEmpty()){
            result.setError("Promotername may not be empty");
            return result;
        }
        if(crowdList.getEmail().isEmpty()){
            result.setError("email may not be empty");
            return result;
        }
        if(!crowdList.getExpiration().isEmpty()){
            if(!servicesUtil.dateValid(crowdList.getExpiration())){
             result.setError("expiration Inccorect date and time format");
             return result;
            }
        }else {
            result.setError("expiration may not be empty");
            return result;
        }
        
        if(crowdList.getAmount()==null){
            result.setError("amount may not be empty");
            return result;
        }
        if(!crowdList.getElevatorpitch().isEmpty()){
            if(crowdList.getElevatorpitch().length()>2000){
                result.setError("elevatorpitch Project description cannot be more than 2000 characters");
                return result;
            }
        }else{
            result.setError("elevatorpitch may not be empty");
            return result;
        }
        if(!crowdList.getIdealinvestor().isEmpty()){
            if(crowdList.getIdealinvestor().length()>500){
                result.setError("idealinvestor Describe your ideal investor cannot be more than 500 characters");
                return result;
            }
        }else{
            result.setError("idealinvestor may not be empty");
            return result;
        }
        if(!crowdList.getReturninvestor().isEmpty()){
            if(crowdList.getReturninvestor().length()>500){
                result.setError("returninvestor What would you give an investor cannot be more than 500 characters");
                return result;
            }
        }else{
            result.setError("returninvestor may not be empty");
            return result;
        }
        if(!crowdList.getPhone().isEmpty()){
            if(this.checkphone(crowdList.getPhone())){
                result.setError("phone This number is already in use please choose another one");
                return result;
            }
        }else{
            result.setError("phone may not be empty");
            return result;
        }
        if(!crowdList.getProjecttitle().isEmpty()){
            if(this.checkTitle(crowdList.getProjecttitle())){
                result.setError("projecttitle This title is already in use please choose another one");
                return result;
            }
        }else {
            result.setError("projecttitle may not be emty");
            return result;
        }
        
        
        crowdList.setUser(user);
        this.add(crowdList);
        
        result.setSuccess(true);
        return result;
    }

    @Override
    public List previouscampaign() {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
        
        return getPreviousCrowd(user);
    }
    
}
    


