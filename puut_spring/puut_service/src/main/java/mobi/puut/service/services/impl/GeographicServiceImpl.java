package mobi.puut.service.services.impl;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mobi.puut.service.data.GeographicData;
import mobi.puut.service.entities.geographic.IPCheck;
import mobi.puut.service.entities.users.Country;
import mobi.puut.service.entities.users.IPlocations;
import mobi.puut.service.services.GeographicService;

@Service("geographicService")
public class GeographicServiceImpl implements GeographicService {
    
    @Autowired
    GeographicData geographicData;
    
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public List<Country> getCountries() {
        this.init();
        
        return geographicData.getCountries();
    }

    @Override
    public List<IPlocations> getCities() {
        this.init();
        
        return geographicData.getCities();
    }

@Transactional
    public List<IPlocations> getCitiesByCountry(char country_iso_code) {
        this.init();
        
        return geographicData.getCitiesByCountry(country_iso_code);     
    }
    
    
    @Override
     public IPCheck getIP(String ipnumber) {
        this.init();
         
        return geographicData.getIP(ipnumber);
    }

    @Override
    public Country getCountryByIso(String iso) {
        this.init();
        return geographicData.getCountryByIso(iso);
    }
    
}