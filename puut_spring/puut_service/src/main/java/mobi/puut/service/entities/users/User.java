package mobi.puut.service.entities.users;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="user")
public class User {

    @Id
    @Column(name="id",nullable=false,unique=true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

	
    @Column(name="username")
    private String username;
	
    @Column(name="password")
    private String password;

    @Column(name="phoneno")
    private String phoneno;

    @ManyToOne(targetEntity = IPlocations.class)
    @JoinColumn(name="cityIPlocations")
    private IPlocations city;
	
	
	
    @Column(name="pin")
    private String pin;

    @Column(name="firstname")
    private String firstname;

    @Column(name="lastname")
    private String lastname;

    @Column(name="middlename")
    private String middlename;


    @Column(name="birthdate")
    private String birthdate;


   
    @Column(name="puutnumber")
    private String puutnumber;


    @Column(name="address")
    private String address;


    @Column(name="postcode")
    private String postcode;

    @Column(name="education")
    private String education;

    @Column(name="occupation")
    private String occupation;


    @Column(name="iban")
    private String iban;

    @Column(name="email")
    private String email;

    @Column(name="civilstatus")
    private String civilstatus;


    
    @Column(name="status")
    private String status;

    @Column(name="ipaddress")
    private String ipaddress;



    @Column(name="socialnr")
    private String socialnr;


    @ManyToOne(targetEntity = Gender.class)
    @JoinColumn(name="gender")
    private Gender gender;



    @Column(name="hometown")
    private String hometown;


    @Column(name="enabled")
    private Boolean enabled;


    @Column(name="disabled")
    private Boolean disabled;



    @ManyToOne(targetEntity = Accounttype.class)
    @JoinColumn(name="accounttype")
    private Accounttype accounttype;

    @ManyToOne(targetEntity = Language.class)
    @JoinColumn(name="language")
    private Language language;


    @ManyToOne(targetEntity = Country.class)
    @JoinColumn(name="country")
    private Country country;


	
	
    @Column(name="country_iso")
    private String country_iso;

    public String getCountry_iso() {
            return country_iso;
    }

    public void setCountry_iso(String country_iso) {
            this.country_iso = country_iso;
    }

    @Column(name="authtoken")
    private String authtoken;


        //@OneToMany(cascade= CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="user")
        @Transient
        private List<Friend> friends;

        @Transient
        private byte[] pimage;


        @Transient
        private byte[] cimage;

        @Transient
        private String fullname;

        @Transient
        private String confirmpin1;

        @Transient
        private String confirmpin2;



        @Transient
        private String emailid;





        public String getEmailid() {
                return emailid;
        }

        public void setEmailid(String emailid) {
                this.emailid = emailid;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getPin() {
                return pin;
        }

        public void setPin(String pin) {
                this.pin = pin;
        }

        public String getFirstname() {
                return firstname;
        }

        public void setFirstname(String firstname) {
                this.firstname = firstname;
        }

        public String getLastname() {
                return lastname;
        }

        public void setLastname(String lastname) {
                this.lastname = lastname;
        }

        public String getMiddlename() {
                return middlename;
        }

        public void setMiddlename(String middlename) {
                this.middlename = middlename;
        }

        public String getBirthdate() {
                return birthdate;
        }

        public void setBirthdate(String birthdate) {
                this.birthdate = birthdate;
        }

        public String getPuutnumber() {
                return puutnumber;
        }

        public void setPuutnumber(String puutnumber) {
                this.puutnumber = puutnumber;
        }

        public String getAddress() {
                return address;
        }

        public void setAddress(String address) {
                this.address = address;
        }

        public String getPostcode() {
                return postcode;
        }

        public void setPostcode(String postcode) {
                this.postcode = postcode;
        }

        public String getEducation() {
                return education;
        }

        public void setEducation(String education) {
                this.education = education;
        }

        public String getOccupation() {
                return occupation;
        }

        public void setOccupation(String occupation) {
                this.occupation = occupation;
        }

        public String getIban() {
                return iban;
        }

        public void setIban(String iban) {
                this.iban = iban;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getCivilstatus() {
                return civilstatus;
        }

        public void setCivilstatus(String civilstatus) {
                this.civilstatus = civilstatus;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getIpaddress() {
                return ipaddress;
        }

        public void setIpaddress(String ipaddress) {
                this.ipaddress = ipaddress;
        }

        public String getSocialnr() {
                return socialnr;
        }

        public void setSocialnr(String socialnr) {
                this.socialnr = socialnr;
        }

        public Gender getGender() {
                return gender;
        }

        public void setGender(Gender gender) {
                this.gender = gender;
        }

        public String getHometown() {
                return hometown;
        }

        public void setHometown(String hometown) {
                this.hometown = hometown;
        }

        public Boolean getEnabled() {
                return enabled;
        }

        public void setEnabled(Boolean enabled) {
                this.enabled = enabled;
        }

        public Boolean getDisabled() {
                return disabled;
        }

        public void setDisabled(Boolean disabled) {
                this.disabled = disabled;
        }

        public Accounttype getAccounttype() {
                return accounttype;
        }

        public void setAccounttype(Accounttype accounttype) {
                this.accounttype = accounttype;
        }

        public Language getLanguage() {
                return language;
        }

        public void setLanguage(Language language) {
                this.language = language;
        }

        public Country getCountry() {
                return country;
        }

        public void setCountry(Country country) {
                this.country = country;
        }

        public List<Friend> getFriends() {
                return friends;
        }

        public void setFriends(List<Friend> friends) {
                this.friends = friends;
        }

        public byte[] getPimage() {
                return pimage;
        }

        public void setPimage(byte[] pimage) {
                this.pimage = pimage;
        }

        public byte[] getCimage() {
                return cimage;
        }

        public void setCimage(byte[] cimage) {
                this.cimage = cimage;
        }

        public String getFullname() {
                return fullname;
        }

        public void setFullname(String fullname) {
                this.fullname = fullname;
        }

        public String getConfirmpin1() {
                return confirmpin1;
        }

        public void setConfirmpin1(String confirmpin1) {
                this.confirmpin1 = confirmpin1;
        }

        public String getConfirmpin2() {
                return confirmpin2;
        }

        public void setConfirmpin2(String confirmpin2) {
                this.confirmpin2 = confirmpin2;
        }

        public String getConfirmpassword1() {
                return confirmpassword1;
        }

        public void setConfirmpassword1(String confirmpassword1) {
                this.confirmpassword1 = confirmpassword1;
        }

        public String getConfirmpassword2() {
                return confirmpassword2;
        }

        public void setConfirmpassword2(String confirmpassword2) {
                this.confirmpassword2 = confirmpassword2;
        }

        @Transient
        private String confirmpassword1;

        @Transient
        private String confirmpassword2;



        @Column(name="image")
        private byte[] image;




    public byte[] getImage() {
                return image;
        }

        public void setImage(byte[] image) {
                this.image = image;
        }

        public IPlocations getCity() {
                return city;
        }

        public void setCity(IPlocations city) {
                this.city = city;
        }

        public String getPhoneno() {
                return phoneno;
        }

        public void setPhoneno(String phoneno) {
                this.phoneno = phoneno;
        }

        public User() {
    }

    public User(Long id, String name, String password) {
        this.id = id;
        this.username = name;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.username = name;
    }

    public String getName() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }






    public String getAuthtoken() {
                return authtoken;
        }

        public void setAuthtoken(String authtoken) {
                this.authtoken = authtoken;
        }

        public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("{id=%s,name=%s}", id, username);
    }

    
}