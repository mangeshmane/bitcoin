package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="preferences")
public class Preference implements Serializable{
	
	private static final long serialVersionUID=1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="user")
	private User user;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="language")
	private Language language;
	
	
	@Column(name="sport")
	private boolean isSport;

	@Column(name="travel")
	private boolean isTravel;
	
	@Column(name="business")
	private boolean isbusiness;
	
	@Column(name="culter")
	private boolean isCulter;

	
	@Column(name="news")
	private boolean isNews;
	
	@Column(name="education")
	private boolean isEducation;
	
	@Column(name="shopping")
	private boolean isShopping;
	
	@Column(name="home")
	private boolean isHome;
	
	
	@Column(name="health")
	private boolean ishealth;
	
	
	@Column(name="email")
	private boolean isEmail;
	
	
	@Column(name="puut")
	private boolean isPuut;
	
	@Column(name="sms")
	private boolean isSms;
	
	
	@Column(name="lang")
	private boolean isLang;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Language getLanguage() {
		return language;
	}


	public void setLanguage(Language language) {
		this.language = language;
	}


	public boolean isSport() {
		return isSport;
	}


	public void setSport(boolean isSport) {
		this.isSport = isSport;
	}


	public boolean isTravel() {
		return isTravel;
	}


	public void setTravel(boolean isTravel) {
		this.isTravel = isTravel;
	}


	public boolean isIsbusiness() {
		return isbusiness;
	}


	public void setIsbusiness(boolean isbusiness) {
		this.isbusiness = isbusiness;
	}


	public boolean isCulter() {
		return isCulter;
	}


	public void setCulter(boolean isCulter) {
		this.isCulter = isCulter;
	}


	public boolean isNews() {
		return isNews;
	}


	public void setNews(boolean isNews) {
		this.isNews = isNews;
	}


	public boolean isEducation() {
		return isEducation;
	}


	public void setEducation(boolean isEducation) {
		this.isEducation = isEducation;
	}


	public boolean isShopping() {
		return isShopping;
	}


	public void setShopping(boolean isShopping) {
		this.isShopping = isShopping;
	}


	public boolean isHome() {
		return isHome;
	}


	public void setHome(boolean isHome) {
		this.isHome = isHome;
	}


	public boolean isIshealth() {
		return ishealth;
	}


	public void setIshealth(boolean ishealth) {
		this.ishealth = ishealth;
	}


	public boolean isEmail() {
		return isEmail;
	}


	public void setEmail(boolean isEmail) {
		this.isEmail = isEmail;
	}


	public boolean isPuut() {
		return isPuut;
	}


	public void setPuut(boolean isPuut) {
		this.isPuut = isPuut;
	}


	public boolean isSms() {
		return isSms;
	}


	public void setSms(boolean isSms) {
		this.isSms = isSms;
	}


	public boolean isLang() {
		return isLang;
	}


	public void setLang(boolean isLang) {
		this.isLang = isLang;
	}
	
	
	
	
	

}
