package mobi.puut.service.entities.users;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name="cryptostatus")
public class Cryptostatus implements Serializable{

	private static final long serialVersionUID=1L;
	
	@Id
	@Column(name="id",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(name="user")
	private Long user;
	
	@Column(name="balance")
	private float balance;
	
	
	@Column(name="address")
	@Size(min = 5,max = 90,message="Address must be between 5 and 90 characters.")
	private String address;
	
	
	@Column(name="transaction")
	@Size(min = 5,max = 90,message="transaction history must be between 5 and 90 characters.")
	private String cryptotransactions;
	
	@Column(name="walletid")
	private Long walletid;
	
	@Column(name="transactionmessage")
	private String transactionmessage;
	
	
	@Column(name="lastupdated")
	private String lastupdated;
	
	
	@ManyToOne(cascade=(CascadeType.REFRESH))
	@JoinColumn(name="transactiontype")
	private TransactionTypes transactiontype;
	
	@Column(name="incomingaddress")
	private String incomingaddress;
	
	
	@Column(name="outgoingaddress")
	private String outgoingaddress;
	
	
	@Column(name="coinin")
	private float coinin;
	
	
	@Column(name="coinout")
	private float coinout;
	
	@Column(name="user_id")
	private Long user_id;
	
	
	
	
	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	@Transient
	private User profile;
	
	@Transient
	private byte[] logo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCryptotransactions() {
		return cryptotransactions;
	}

	public void setCryptotransactions(String cryptotransactions) {
		this.cryptotransactions = cryptotransactions;
	}

	public Long getWalletid() {
		return walletid;
	}

	public void setWalletid(Long walletid) {
		this.walletid = walletid;
	}

	public String getTransactionmessage() {
		return transactionmessage;
	}

	public void setTransactionmessage(String transactionmessage) {
		this.transactionmessage = transactionmessage;
	}

	public String getLastupdated() {
		return lastupdated;
	}

	public void setLastupdated(String lastupdated) {
		this.lastupdated = lastupdated;
	}

	public TransactionTypes getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(TransactionTypes transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getIncomingaddress() {
		return incomingaddress;
	}

	public void setIncomingaddress(String incomingaddress) {
		this.incomingaddress = incomingaddress;
	}

	public String getOutgoingaddress() {
		return outgoingaddress;
	}

	public void setOutgoingaddress(String outgoingaddress) {
		this.outgoingaddress = outgoingaddress;
	}

	public float getCoinin() {
		return coinin;
	}

	public void setCoinin(float coinin) {
		this.coinin = coinin;
	}

	public float getCoinout() {
		return coinout;
	}

	public void setCoinout(float coinout) {
		this.coinout = coinout;
	}

	public User getProfile() {
		return profile;
	}

	public void setProfile(User profile) {
		this.profile = profile;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
