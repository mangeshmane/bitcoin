package mobi.puut.service.entities.shoppingwall;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="shoppinglistusers")
public class ShoppingListUser implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="shoppinglist")
    private ShoppingList shoppinglist;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name="user")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ShoppingList getShoppinglist() {
        return shoppinglist;
    }

    public void setShoppinglist(ShoppingList shoppinglist) {
        this.shoppinglist = shoppinglist;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}