package mobi.puut.service.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.multipart.MultipartFile;

import mobi.puut.service.data.PhotosData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.photo.*;
import mobi.puut.service.services.PhotosService;
import mobi.puut.service.services.PhotosUtil;

@Repository("photosService")
public class PhotosServiceImpl implements PhotosService{

	
@Autowired
PhotosData photosData;
	

@Autowired
PhotosUtil photosUtil;

@Autowired
UserData userData;

@PostConstruct
public void init() {
	SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
}

@Transactional
@Override
public Photo getPhotoInformation(Long id) {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	
	return photosData.getPhoto(id,login);
}


@Transactional
@Override
public List<PhotoAlbum> getAlbums() {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	
	return photosData.getAlbums(login);
	
}

@Transactional
@Override
public List<PhotoAlbum> getFriendAlbums(Long friend_id) {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	
	return photosData.getFriendAlbums(friend_id,login);
}


@Transactional
@Override
public List<Photo> getPhotos(Long album_id, int width, int height) {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	List<Photo> photos=photosData.getPhotos(album_id,login);
	
	for(Photo photo : photos) {
		if(photo.getPicture() != null) {
		
		InputStream in=new ByteArrayInputStream(photo.getPicture());
		BufferedImage img;
		try {
			img = ImageIO.read(in);
			int type =img.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : img.getType();
			Logger.getLogger(PhotosServiceImpl.class).error("width : "+width+"height : "+height);
			BufferedImage buf_img=ImageUtil.resizeImage(img,type,width,height);
			
			ByteArrayOutputStream bas =new ByteArrayOutputStream();
			ImageIO.write(buf_img, "jpg",bas);
			
			
			bas.flush();
			byte[] byte_img=bas.toByteArray();
			bas.close();
			photo.setPicture(byte_img);
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}	
		
	}

	return photos;
}


@Transactional
@Override
public Result sharePhoto(Long id, Long status) {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	return photosData.sharePhoto(id,status,login);
}

@Override
public Result voteForPhoto(Long id, Long status) {
	// TODO Auto-generated method stub
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();	
	return photosData.voteForPhoto(id,status,login);
}



@Override
public Response uploadFile(MultipartFile uploadInputStream, Long id) {
	// TODO Auto-generated method stub
	
	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	
	User user =userData.getByUsername(login);
	PhotoAlbum photo_album=photosData.get(id,user);
	
	if(photo_album == null) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	}
	
	photosUtil.setPhoto(Boolean.TRUE);
	
	
	String photo_name=photo_album.getTitle()+ThreadLocalRandom.current().nextInt();

	if(photo_name.isEmpty()) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	}
	
	Photo photo=new Photo();
	
	photo.setDescription(photo_name);
	try {
		photo.setPicture(uploadInputStream.getBytes());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	photo.setPhotoalbum(photo_album);
	photo.setShared(Boolean.FALSE);
	
	photosData.add(photo);
	
	String output="File Has Uploaded";
			
	return Response.status(200).entity(output).build();
}


@Transactional
@Override
public Result createAlbum(PhotoAlbum photoAlbum) {
	// TODO Auto-generated method stub

	this.init();
	String login=SecurityContextHolder.getContext().getAuthentication().getName();
	
	User user =userData.getByUsername(login);
	
	photoAlbum.setUser(user);
	
	return photosData.createAlbum(photoAlbum,login);
}




	
}
