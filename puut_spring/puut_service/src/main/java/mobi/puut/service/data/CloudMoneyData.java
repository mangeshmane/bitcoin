package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Transaction;

public interface CloudMoneyData {

	Currency getCurrencyByCode(String string);

	List<Currency> getCurrencies();

	  public List<CloudMoney> getBalance(String login);

	 public List<Transaction> getTransactions(String login);

	public Result sendMoney(Transaction transaction, Result result, String login);
}