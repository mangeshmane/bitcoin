package mobi.puut.service.config;

import java.io.StringWriter;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import org.springframework.web.socket.WebSocketMessage;

public class MessageEncoder implements Encoder.Text<WebsocketMessage> {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String encode(WebsocketMessage wsMsg) throws EncodeException {
		// TODO Auto-generated method stub
		StringWriter writer =new StringWriter();
		
		JsonGenerator jsonGen=Json.createGenerator(writer)
				.writeStartObject()
				.write("name",wsMsg.getName());
		
		
		for(int i = 0;i<wsMsg.getParams().size();i++) {
			jsonGen.write("params"+i, wsMsg.getParams().get(i));
		}
				
jsonGen.writeEnd()
.flush();


return writer.toString();				
	}
	

}
