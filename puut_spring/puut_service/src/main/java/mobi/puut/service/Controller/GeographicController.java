/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.mail.MessagingException;
import javax.ws.rs.core.MediaType;

import mobi.puut.service.entities.geographic.IPCheck;
import mobi.puut.service.entities.users.Country;
import mobi.puut.service.entities.users.IPlocations;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.GeographicService;
import mobi.puut.service.services.crypto.impl.WalletServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Development
 */
@RestController
@RequestMapping("/countries")
public class GeographicController {

    @Autowired
    private GeographicService geographicService;

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient') or hasAuthority('puutpartner')")
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<Country> getCountries() {
        try {
            return geographicService.getCountries();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(GeographicController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient') or hasAuthority('puutpartner')")
    @RequestMapping(value = "/cities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<IPlocations> getCities() {
        try {
            return geographicService.getCities();
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(GeographicController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient') or hasAuthority('puutpartner')")
    @RequestMapping(value = "/cities/{country}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public List<IPlocations> getCitiesByCountry(@PathVariable("country") char country_iso_code) {
        try {
            return geographicService.getCitiesByCountry(country_iso_code);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(GeographicController.class).error(exception);
            throw new Error(exception);
        }

    }

    @PreAuthorize("hasAuthority ('puutpremium') or hasAuthority('puutclient') or hasAuthority('puutpartner')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public IPCheck getIP(@PathVariable String ipnumber) {
        try {
            return geographicService.getIP(ipnumber);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(GeographicController.class).error(exception);
            throw new Error(exception);
        }

    }
}
