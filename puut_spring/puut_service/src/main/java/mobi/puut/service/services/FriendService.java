package mobi.puut.service.services;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.Friend;


public interface FriendService {
	
	public List<Friend>  getFriends();
	public List<Friend>  getInvitations();
	public Result  acceptInvitations(@PathVariable("id") Long id);
	public User  getFriendProfile(@PathVariable("id") Long id);
	public List<User> searchFriend(@RequestBody User user);
	public Result deleteFriend(@RequestBody List<User> users);
	
}
