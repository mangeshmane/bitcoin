package mobi.puut.service.data;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.User;

@Repository("friendData")
@Transactional
public class FriendDataImpl implements FriendData {

	@Autowired
	SessionFactory sessionFactory;

	@PostConstruct
	public void init() {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	@Transactional
	@Override
	public List<Friend> getFriends(User user) {
		// TODO Auto-generated method stub
		this.init();
		Session session = sessionFactory.getCurrentSession();
		List<Friend> friends = session
				.createQuery("from Friend as friend where friend.user=:user and friend.accepted=:accepted")
				.setParameter("user", user).setParameter("accepted", true).getResultList();

		return friends;
	}
	
	   @Transactional(rollbackFor = Exception.class)
	    public List<Friend> getFriendsByUserId(User user) {
	        this.init();

	        Session session = sessionFactory.getCurrentSession();

	        List<Friend> friends = session.createQuery("from Friend friend where friend.user=:user and friend.accepted=:accepted")
	                .setParameter("user", user)
	                .setParameter("accepted", true)
	                .getResultList();

	        return friends;
	    }

	@Transactional
	@Override
	public List<Friend> getInvitations(User user) {
		// TODO Auto-generated method stub
		this.init();
		Session session = sessionFactory.getCurrentSession();
		List<Friend> friends = session
				.createQuery("from Friend as friend where friend.user=:user and friend.accepted=:accepted")
				.setParameter("user", user).setParameter("accepted", true).getResultList();

		return friends;

	}

	@Override
	public Result acceptInvitations(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getFriendProfile(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Result deleteFriend(List<User> users, String login) {
		// TODO Auto-generated method stub
		this.init();
		Result result = new Result();
		result.setSuccess(true);
		StringBuilder query = new StringBuilder();
		Iterator iterator = users.iterator();

		while (iterator.hasNext()) {
			String friend_login = ((User) iterator.next()).getUsername();
			if (query.toString().isEmpty()) {
				query.append("from Friend as f where f.user.username =:username and (f.friend.login ='")
						.append(friend_login).append("'");
			} else {
				query.append("or f.friend.username ='").append(friend_login).append("'");
			}
		}

		if (!query.toString().isEmpty()) {
			query.append(")");

		}

		if (!query.toString().isEmpty()) {
			List<Friend> friends = sessionFactory.getCurrentSession().createQuery(query.toString())
					.setParameter("username", login).list();

			iterator = friends.iterator();
			while (iterator.hasNext()) {
				sessionFactory.getCurrentSession().delete((Friend) iterator.next());
			}
		} else {
			result.setSuccess(false);
			result.setError("No friends deleted");
		}

		return result;
	}

	@Transactional
	@Override
	public Friend get(Long id, User user) {
		// TODO Auto-generated method stub
		this.init();
		Session session = sessionFactory.getCurrentSession();
		Friend friend = (Friend) session.createQuery("from Friend as friend where friend.user=:user and friend.id=:id")
				.setParameter("user", user).setParameter("id", id).getSingleResult();
		return friend;
	}

	@Transactional
	@Override
	public Friend get(Long id, String login, int accepted) {
		// TODO Auto-generated method stub
		this.init();
		Session session = sessionFactory.getCurrentSession();
		Friend friend = (Friend) session
				.createQuery("from Friend as friend where friend.user.username=:login and friend.id=:id")
				.setParameter("login", login).setParameter("id", id).getSingleResult();
		return friend;
	}

	@Transactional
	@Override
	public void edit(Friend friend) {
		// TODO Auto-generated method stub
		this.init();
		sessionFactory.getCurrentSession().saveOrUpdate(friend);
	}

	@Transactional
	@Override
	public List<User> searchFriend(User user, String login) {
		// TODO Auto-generated method stub

		this.init();

		Map map = new HashMap();

		String query = "";
		String name = "";

		if (user.getUsername() != null) {

			if (!user.getUsername().isEmpty()) {
				name = user.getUsername();
				query = "from User as profile where username like :name";
				map.put("plogin", user.getUsername());

			}
		}

		if (user.getFirstname() != null) {

			if (!user.getFirstname().isEmpty()) {
				name = user.getFirstname();
				if (query.isEmpty()) {
					query = "from User as profile where firstname like :name";
				} else {
					query = query + "and firstname like :name";
				}

				map.put("firstname", user.getFirstname());

			}
		}

		if (user.getLastname() != null) {

			if (!user.getLastname().isEmpty()) {
				name = user.getLastname();
				if (query.isEmpty()) {
					query = "from User as profile where lastname like :name";
				} else {
					query = query + "and lastname like :name";
				}

				map.put("lastname", user.getLastname());

			}
		}

		List<User> list = sessionFactory.getCurrentSession().createQuery(query).setParameter("name", "%" + name + "%")
				.getResultList();

		return list;
	}

	@Transactional
	@Override
	public void add(Friend friend) {
		// TODO Auto-generated method stub
		this.init();
		sessionFactory.getCurrentSession().save(friend);
	}

	@Transactional
	@Override
	public Preference getPreference(User user_profile) {
		// TODO Auto-generated method stub
		this.init();
		Session session = sessionFactory.getCurrentSession();

		Preference preference = (Preference) session
				.createQuery("from Preference as preference where preference.user=:user")
				.setParameter("user", user_profile).getSingleResult();

		return preference;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Preference> getAccept(Preference preuser) {
		this.init();

		return sessionFactory.getCurrentSession()
				.createQuery("select f.prefriend from Friend f where f.preuser=:preuser and f.accepted=:accepted")
				.setParameter("preuser", preuser).setParameter("accepted", true).getResultList();

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Preference> getFind(Preference prefriend) {
		this.init();

		return sessionFactory.getCurrentSession()
				.createQuery("select f.preuser from Friend f where f.prefriend=:prefriend and f.accepted=:accepted")
				.setParameter("prefriend", prefriend).setParameter("accepted", true).getResultList();

	}


	
	@Override
	@Transactional(rollbackFor=Exception.class)
    public Boolean isFriend(User user,User friend) {
        this.init();
        
        List<Friend> friendList = sessionFactory.getCurrentSession().createQuery("select f from Friend f where f.user=:user and f.friend=:friend and f.accepted=:accepted")
                .setParameter("user", user)
                .setParameter("friend", friend)
                .setParameter("accepted",true)
                .getResultList();
        if(friendList.isEmpty() || friendList.size() == 0){
            return false;
        }
        else{
            return true;
        }
       
    }
	

}
