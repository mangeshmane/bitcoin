/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.services.impl;

import javax.annotation.PostConstruct;

import mobi.puut.service.data.ProgramsData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.ProgramsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author Jay
 */
@Service("programsService")
public class ProgramsServiceImpl implements ProgramsService {
    @Autowired 
    ProgramsData programsData;
    
    @Autowired
    UserData usersData;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    
    public Result save(Program program) {
        this.init();
        Result result = new Result();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        
        Program temp = programsData.findByName(program.getName());
        
        if(temp != null){
            result.setSuccess(Boolean.FALSE);
            result.setError("Program name already exists");           
        }else{        
            program.setUser(user);
            programsData.save(program);
            result.setSuccess(Boolean.TRUE);
        }
        
        return result;
    }
    
}
