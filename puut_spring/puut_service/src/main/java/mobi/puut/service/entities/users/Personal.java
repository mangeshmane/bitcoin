package mobi.puut.service.entities.users;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="personal")
public class Personal implements Serializable{

	private static final long serialVersionUID=1L;
	
	@Id
	@Column(name="id",unique=true,nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	 @Column(name="name")
	private String name;
	
	 @Column(name="choose")
	private Boolean choose;
	
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getChoose() {
		return choose;
	}

	public void setChoose(Boolean choose) {
		this.choose = choose;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name="user")
    private User user;
    
    @Column(name="image")
    private byte[] image;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    
}
