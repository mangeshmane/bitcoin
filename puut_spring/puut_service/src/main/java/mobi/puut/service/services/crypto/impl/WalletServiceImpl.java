package mobi.puut.service.services.crypto.impl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import mobi.puut.service.crypto.util.WalletManager;
import mobi.puut.service.crypto.util.WalletModel;
import org.bitcoinj.core.*;
import org.bitcoinj.crypto.DeterministicKey;
import org.bitcoinj.wallet.KeyChain.KeyPurpose;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.crypto.util.RandomString;
import mobi.puut.service.crypto.util.RestService;
import org.bitcoinj.params.TestNet3Params;
import mobi.puut.service.data.CloudMoneyData;
import mobi.puut.service.data.MessagesData;
import mobi.puut.service.data.StatusData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.data.WalletInfoData;
import mobi.puut.service.data.impl.StatusDataImpl;
import mobi.puut.service.data.impl.WalletInfoDataImpl;
import mobi.puut.service.entities.crypto.btc.SendMoney;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Cryptostatus;
import mobi.puut.service.entities.users.GenerateWallet;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Secure;
import mobi.puut.service.entities.users.TransactionTypes;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.crypto.def.WalletService;
import org.apache.log4j.Logger;
import org.bitcoinj.script.Script;
import org.bitcoinj.utils.MonetaryFormat;
import org.springframework.security.access.AccessDeniedException;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service("walletService")
@RestService
public class WalletServiceImpl implements WalletService {

    RandomString randomString = new RandomString();

    @Autowired
    private UserData usersData;

    @Autowired
    ServicesUtil servicesUtil;

    @Autowired
    CloudMoneyData cloudMoneyData;
    
    @Autowired
    MessagesData messagesData;
    //@Autowired - to be corrected
    private StatusData statusData = new StatusDataImpl();

    //@Autowired - to be corrected
    private WalletInfoData walletInfoData = new WalletInfoDataImpl();

    public static final MathContext Default_Context = new MathContext(0, RoundingMode.UNNECESSARY);

    private HashMap<Long, WalletManager> walletManagerMap = new HashMap<Long, WalletManager>();

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Cryptoinfo> getAllWallets() {
        try {
            this.init();
            List<Cryptoinfo> cryptoInfos = this.walletInfoData.getAllWallets();
            return cryptoInfos;
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new Error(exception);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Cryptoinfo getWalletInfo(Long walletId) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = this.usersData.getByUsername(login);
        if (this.walletInfoData.checkWalletAndUser(walletId, user)) {
            return this.walletInfoData.getWalletById(walletId);
        } else {
            throw new AccessDeniedException("Access Denied");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result generateAddress(GenerateWallet generateWallet) {
        this.init();
        Result result = new Result();
        result.setSuccess(Boolean.FALSE);
        Boolean isDefaultAddress = false;
        CountDownLatch finshedSetup = new CountDownLatch(1);
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = this.usersData.getByUsername(login);
        final Cryptoinfo walletInfo = new Cryptoinfo();
        String fileDirectoryName = randomString.nextString();
        String currencyName = generateWallet.getCurrencyName();
        // make in capital order and remove all the white space and invisible characters like "\n"
        String currency = currencyName.toUpperCase().replaceAll("\\s+", "");
        String tempAddress = null;
        switch (currency) {
            case "BITCOIN":
                try {
                    WalletManager walletManager = new WalletManager(fileDirectoryName, "BTC");
                    walletManager.setupWallet();
                    walletManager.addWalletSetupCompletedListener((Wallet wallet) -> {
                        Address address = wallet.currentReceiveAddress();
                       
                       List<ECKey> abc = wallet.getIssuedReceiveKeys();
                      
                       for (ECKey ecKey : abc) {
						System.out.println("key"+ ecKey.getPrivateKeyAsHex());
					   }
                        
                       
                       
                        walletInfo.setCode("BTC");
                        //wallet.importKey(key)
                        walletInfo.setAddress(address.toString());
                        walletInfo.setCurrency(currencyName);
                        walletInfo.setIdentifier(fileDirectoryName);
                       
                        finshedSetup.countDown();
                    });
                    // wait for the completion of the thread
                    finshedSetup.await();
                    List<Cryptoinfo> userWallet = this.walletInfoData.getAddressByUser(user);
                    if (userWallet.size() == 0) {
                        isDefaultAddress = true;
                    }
                    Cryptoinfo newWallet = this.walletInfoData.create(walletInfo.getCode(), walletInfo.getCurrency(), walletInfo.getAddress(), user, isDefaultAddress, walletInfo.getIdentifier());
                    this.walletManagerMap.put(newWallet.getId(), walletManager);
                    //getWalletManager(newWallet.getId());  
                    result.setSuccess(Boolean.TRUE);
                    result.setSuccessMessage("Address generated successfully");                    
                } catch (InterruptedException ex) {
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Failed to generate address");
                }
                break;

            case "ETHEREUM":
                break;

            case "LITECOIN":
                break;

            case "BITCOINCASH":
                break;

            default:
                break;
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deleteWalletInfoById(Long id) {
        this.init();
        Result result = new Result();
        try{
            this.walletInfoData.deleteWalletInfoByWalletId(id);
            result.setSuccess(Boolean.TRUE);
            result.setSuccessMessage("Wallet deleted successfully");
        }catch(Exception e){
             result.setSuccess(Boolean.FALSE);
             result.setError("Failed to delete wallet");
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result sendMoney(Long walletId, SendMoney sendMoney) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = this.usersData.getByUsername(login);
        Result result = new Result();
        Cryptostatus cryptostatus = new Cryptostatus();
       

            String address = null;
            if (sendMoney.getAddress() == null || sendMoney.getAddress().isEmpty()) {
                String phone = sendMoney.getPhone();
                if (phone.equals("") || phone == null) {
                    System.out.println("Phone number can not be empty");
                    throw new AccessDeniedException("Phone number can not be empty");
                }
                User phoneUser = this.usersData.getUserByPhone(phone);
                if (phoneUser == null) {
                    System.out.println("No user found for given number");
                    throw new AccessDeniedException("No user found for given number");
                }
                Cryptoinfo cryptoinfo = this.walletInfoData.getDefaultWallet(phoneUser);
                address = cryptoinfo.getAddress();
            } else {
                address = sendMoney.getAddress();
            }
            String amount = sendMoney.getAmount();

            if (amount == null || amount.isEmpty()) {
                throw new AccessDeniedException("Amount can not be empty");
            }
            if (address == null || address.isEmpty()) {
                throw new AccessDeniedException("Address can not be empty");
            }

            WalletModel model = null;

            WalletManager walletManager = this.getWalletManager(walletId);

            if (walletManager != null) {
                System.out.println("Wallet Manager is not null");
                Wallet wallet = walletManager.getBitcoin().wallet();
                Logger.getLogger(WalletServiceImpl.class).error("Getting wallet object");
                if (Objects.isNull(wallet)) {
                    // return model;
                    System.out.println("Wallet is null");
                }

                if (address != null && !address.isEmpty()) {
                    System.out.println("Before send call");
                    Logger.getLogger(WalletServiceImpl.class).error("Before send call");
                    result = send(user, walletId, wallet, address, amount);                   
                    
                }

                model = walletManager.getModel();
            }

            // use WalletModelWrapper here instead of the walletModel
            return result;
       
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String getWalletBalanceById(long id) {
        
        
        try { 
            this.init();
            String login = SecurityContextHolder.getContext().getAuthentication().getName();
            User user = this.usersData.getByUsername(login);
            DecimalFormat decimalFormat = new DecimalFormat("#0.0000");

            if (this.walletInfoData.checkWalletAndUser(id, user)) {
                WalletModel walletModel = getWalletModel(id);
                String testBalance = decimalFormat.format(0L);
                String balance = decimalFormat.format(walletModel.getBalanceFloatFormat());

                if (testBalance.equals(balance)) {
                    if (walletModel.getTransaction() == null && statusData.getByWalletId(id) != null) {
                        List<Cryptostatus> transactions = statusData.getByWalletId(id);
                        //balance = decimalFormat.format(transactions.get(transactions.size() -1).);
                        if(transactions.size()!=0&&!transactions.isEmpty()){
                        	 Coin coin = Coin.valueOf((long) transactions.get(transactions.size() - 1).getBalance());
                             balance = decimalFormat.format(Float.valueOf(coin.toPlainString()));
                       }
                        
                       
                 
                    }
                }
                return balance;
            } else {
                //return "Access Denied";
                return "No record found";
            }
        }catch (Exception e) {
             Logger.getLogger(WalletServiceImpl.class).error("Got Exception: "+e);
            /*StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(WalletServiceImpl.class).error(exception);
            throw new AccessDeniedException(e.getMessage());*/
        }
        return "No record found";
    }

    @Override 
    @Transactional(rollbackFor = Exception.class) 
    public String getWalletsCount() { 
        this.init();

        List<Cryptoinfo> walletInfos = this.walletInfoData.getAllWallets();
        return String.valueOf(walletInfos.size()); 
    }

    @Override 
    @Transactional(rollbackFor = Exception.class) 
    public Cryptoinfo getWalletInfoByCurrencyAndAddress(String currencyName, String address) { 
        this.init();

        return this.walletInfoData.getWalletInfoByCurrencyAndAddress(currencyName, address); 
    }

    @Override 
    @Transactional(rollbackFor = Exception.class) 
    public WalletModel getWalletModel(Long walletId) { 

        WalletManager walletManager = this.getWalletManager(walletId); 
        WalletModel model = walletManager == null ? null : walletManager.getModel();
        if (Objects.isNull(model)) {
        }

        return model; 
    }

    private Result send(User user, final Long walletId, Wallet wallet, final String address, String amount) { 
        // Address exception cannot happen as we validated it beforehand.

        final Coin balance = wallet.getBalance();
        Result transactionResult = new Result();
        Coin amountCoin = parseCoin(amount);
        final String lastUpdated = new Date().toString();
        TransactionTypes type = new TransactionTypes();

        try {
            System.out.println("In send function");
            Coin amount1 = parseCoin(amount);
            Address destination = Address.fromBase58(TestNet3Params.get(), address);
            Logger.getLogger(WalletServiceImpl.class).error("In send function..");
            SendRequest req = null;
            if (amount1.isGreaterThan(balance)) {
                Logger.getLogger(WalletServiceImpl.class).error("Amount is greater than bal");
            } else if (amount1.equals(balance)) {
                Logger.getLogger(WalletServiceImpl.class).error("Amount is equals bal");
                req = SendRequest.emptyWallet(destination);

            } else {
                Logger.getLogger(WalletServiceImpl.class).error("Amount is less than bal");
                req = SendRequest.to(destination, amount1);
            }

            Wallet.SendResult sendResult = wallet.sendCoins(req);
            CountDownLatch finshedSetup = new CountDownLatch(1);
            final Cryptostatus status = new Cryptostatus();
            final Cryptostatus incomingStatus = new Cryptostatus();
            final Cryptoinfo sourceCryptoInfo = walletInfoData.getWalletById(walletId);
            final Cryptoinfo destinationCryptoInfo = walletInfoData.getByAddress(address);
            WalletManager walletManager1 = null;
            if (destinationCryptoInfo != null) {
                walletManager1 = getWalletManager(destinationCryptoInfo.getId());
            }
            final WalletManager walletManager = walletManager1;
            Futures.addCallback(sendResult.broadcastComplete, new FutureCallback<Transaction>() {
                @Override
                public void onSuccess(@Nullable Transaction result) {
                    //String message = result.toString();
                    final Coin value = result.getValue(wallet);
                    final String message = result.toString();

                    final String transactionmessage = "Outbound payment worth of "
                            + (MonetaryFormat.BTC.format(value)).toString().replaceAll("-", "");
                    final Result successResult = new Result();
                    transactionResult.setSuccess(Boolean.TRUE);
                    transactionResult.setSuccessMessage(transactionmessage);
                    final Coin remainingBalance = balance.subtract(amountCoin);

                    type.setId(2L);
                    status.setAddress(sourceCryptoInfo.getAddress());
                    status.setBalance(remainingBalance.getValue());
                    status.setCryptotransactions(message);
                    status.setTransactionmessage(transactionmessage);
                    status.setTransactiontype(type);
                    status.setUser_id(sourceCryptoInfo.getUser().getId());
                    status.setWalletid(walletId);
                    status.setLastupdated(result.getUpdateTime().toString());
                    status.setIncomingaddress(null);
                    status.setOutgoingaddress(address);
                    status.setCoinout(Float.valueOf(value.toString().replaceAll("-", "")));
                    finshedSetup.countDown();

                    if (destinationCryptoInfo != null) {
                        String incomingTransactionMessage = "Incoming payment worth of "
                                + (MonetaryFormat.BTC.format(value)).toString().replaceAll("-", "");

                        if (walletManager != null) {
                            Wallet wallet = walletManager.getBitcoin().wallet();
                            final Coin currentBalance = wallet.getBalance();
                            final Coin updatedBalance = currentBalance.add(amountCoin);

                            type.setId(1L);

                            incomingStatus.setUser_id(destinationCryptoInfo.getUser().getId());
                            incomingStatus.setWalletid(destinationCryptoInfo.getId());
                            incomingStatus.setAddress(destinationCryptoInfo.getAddress());
                            incomingStatus.setTransactionmessage(incomingTransactionMessage);
                            incomingStatus.setBalance(updatedBalance.getValue());
                            incomingStatus.setCryptotransactions(message);
                            incomingStatus.setLastupdated(result.getUpdateTime().toString());
                            incomingStatus.setTransactiontype(type);
                            incomingStatus.setIncomingaddress(sourceCryptoInfo.getAddress());
                            incomingStatus.setOutgoingaddress(null);
                            incomingStatus.setCoinin(Float.valueOf(value.toString().replaceAll("-", "")));
                            finshedSetup.countDown();

                        }
                    }
                    System.out.println("Transaction Success");
                    Logger.getLogger(WalletServiceImpl.class).error("Transaction success");
                }

                @Override
                public void onFailure(Throwable t) {
                    System.out.println("Transaction failed");
                    Logger.getLogger(WalletServiceImpl.class).error("Transaction failed");
                    String error = "Could not send money. " + t.getMessage();
                    status.setCryptotransactions(error);
                    transactionResult.setSuccess(Boolean.FALSE);
                    transactionResult.setError(error);
                    type.setId(4L);
                    saveTransaction(user.getId(), walletId, address, error, balance, error, lastUpdated, type, null, null, null, null);
                    finshedSetup.countDown();
                }
            });
            finshedSetup.await();
            Cryptoinfo cryptoInfo = walletInfoData.getWalletById(walletId);
            if (!checkTransaction(status.getTransactionmessage(), walletId)) {
                saveTransaction(status.getUser(), walletId, status.getAddress(), status.getCryptotransactions(), balance.subtract(amountCoin), status.getTransactionmessage(), lastUpdated, type, null, address, Coin.parseCoin(String.valueOf(status.getCoinin())), Coin.parseCoin(String.valueOf(status.getCoinout())));

            }

            if (incomingStatus.getUser() != null) {
                if (!checkTransaction(incomingStatus.getTransactionmessage(), incomingStatus.getWalletid())) {
                    saveTransaction(status.getUser(), incomingStatus.getWalletid(), incomingStatus.getAddress(), incomingStatus.getCryptotransactions(), walletManager.getModel().getBalance(), incomingStatus.getTransactionmessage(), lastUpdated, type, incomingStatus.getIncomingaddress(), null, Coin.parseCoin(String.valueOf(incomingStatus.getCoinin())), Coin.parseCoin(String.valueOf(incomingStatus.getCoinout())));
                }
            }
            
            if(transactionResult.getSuccess()){
                     
            		Message message=new Message();
            	
                        message.setTitle("Transaction Success");
                        message.setBody("You have recieved money of amount "+amount+ " BTC from "+ sourceCryptoInfo.getUser().getFirstname() +" "+sourceCryptoInfo.getUser().getLastname());
                        message.setUserfrom(sourceCryptoInfo.getUser());                        
                        if(destinationCryptoInfo != null){                        
                            message.setUserto(destinationCryptoInfo.getUser());
                        }
                        message.setMessagedate(servicesUtil.getFormattedCurrentDateTime());
                        messagesData.add(message);
             }
            return transactionResult;

        } catch (InsufficientMoneyException e) {
            System.out.println("You may have too little money left in the wallet to make a transaction");
            String error = "Could not empty the wallet. "
                    + "You may have too little money left in the wallet to make a transaction.";
            transactionResult.setSuccess(Boolean.FALSE);
            transactionResult.setError(error);
            this.saveTransaction(user.getId(), walletId, address, error, balance, error, lastUpdated, statusData.getTransactionTypeById(4L), null, null, null, null);

        } catch (ECKey.KeyIsEncryptedException e) {
            System.out.println("Could not send money. " + "Remove the wallet password protection.");
            String error = "Could not send money. " + "Remove the wallet password protection.";
            transactionResult.setSuccess(Boolean.FALSE);
            transactionResult.setError(error);
            this.saveTransaction(user.getId(), walletId, address, error, balance, error, lastUpdated, statusData.getTransactionTypeById(4L), null, null, null, null);
        } catch (AddressFormatException e) {
            System.out.println("Could not send money. Invalid address.");
            String error = "Could not send money. Invalid address: " + e.getMessage();
            transactionResult.setSuccess(Boolean.FALSE);
            transactionResult.setError(error);
            this.saveTransaction(user.getId(), walletId, address, error, balance, error, lastUpdated, statusData.getTransactionTypeById(4L), null, null, null, null);
        } catch (Exception e) {
            System.out.println("Could not send money.(exception)" + e.getMessage());
            String error = "Could not send money. " + e.getMessage();
            transactionResult.setSuccess(Boolean.FALSE);
            transactionResult.setError(error);
            this.saveTransaction(user.getId(), walletId, address, error, balance, error, lastUpdated, statusData.getTransactionTypeById(4L), null, null, null, null);
        }
        return transactionResult;
    }

    private Cryptostatus saveTransaction(Long userId, Long walletId, String address, String message, Coin balance, String transactionMessage, String updatedTime, TransactionTypes transactionTypes, String incomingAddress, String outgoingAddress, Coin coinin, Coin coinout) {
        Cryptostatus status = new Cryptostatus();
        status.setAddress(address.length() > 90 ? address.substring(0, 89) : address);

        //status.setId(user.getId().intValue());
        status.setUser_id(userId);
        status.setWalletid(walletId);
        status.setCryptotransactions(message.length() > 90 ? message.substring(0, 89) : message);
        status.setBalance(balance.getValue());
        status.setTransactionmessage(transactionMessage);
        status.setTransactiontype(transactionTypes);
        status.setIncomingaddress(incomingAddress);
        status.setOutgoingaddress(outgoingAddress);

        if (coinin != null) {
            status.setCoinin(coinin.getValue());
        }

        if (coinout != null) {
            status.setCoinout(coinout.getValue());
        }

        try {
            status.setLastupdated(servicesUtil.formatTransactionDateTime(updatedTime));
        } catch (Exception e) {
            Logger.getLogger(WalletServiceImpl.class).error(e.getMessage());
        }
        Logger.getLogger(WalletServiceImpl.class).error("In save status function");
        return this.statusData.saveStatus(status);
    }

    protected Coin parseCoin(final String amountStr) {
        try {
            Coin amount = Coin.parseCoin(amountStr);
            if (amount.getValue() <= 0) {
                throw new IllegalArgumentException("Invalid amount: " + amountStr);
            }
            return amount;
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid amount: " + amountStr);
        }
    }

    private WalletManager getWalletManager(Long walletId) {

        WalletManager walletManager = this.walletManagerMap.get(walletId);

        //walletManager.bitcoin.startAsync(); //THIS ADDED 13-DEC-2017
        if (walletManager == null) {

            Cryptoinfo walletInfo = this.walletInfoData.getById(walletId);
            String code = walletInfo.getCode();

            if (walletInfo != null) {

                walletManager = new WalletManager(walletInfo.getIdentifier(), code);
                walletManager.setupWallet();
                // if(walletManager.getModel().getAddress() != null) {
                this.walletManagerMap.put(walletInfo.getId(), walletManager);
                //  }

            }
        }
        return walletManager;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Cryptoinfo updateDefaultWallet(Long walletid) {
        this.init();
        Cryptoinfo wallet = this.walletInfoData.getWalletById(walletid);
        List<Cryptoinfo> userWallets = this.walletInfoData.getAddressByUser(wallet.getUser());

        for (Cryptoinfo wallets : userWallets) {
            wallets.setMain(Boolean.FALSE);
            this.walletInfoData.updateWallet(wallets);
        }

        wallet.setMain(Boolean.TRUE);
        Cryptoinfo updatedWallet = this.walletInfoData.updateWallet(wallet);
        return updatedWallet;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List getWalleTransactionsById(long id) {
        //WalletModel walletModel = getWalletModel(id);
        WalletManager walletManager = getWalletManagerById(id);
        WalletModel walletModel = walletManager.getModel();
        List<String> resultList = new ArrayList<String>();
        Cryptoinfo cryptoinfo = walletInfoData.getWalletById(id);
        addCryptoHistoryDetails(cryptoinfo, walletManager);
        DecimalFormat decimalFormat = new DecimalFormat("#0.0000");
        List<Cryptostatus> cryptostatuses = this.statusData.getByWalletId(id);
        List<Cryptostatus> transactions = new ArrayList<Cryptostatus>();

        //Float.valueOf(Coin.valueOf((long) transaction.getBalance()).toPlainString())
        for (Cryptostatus cryptostatus : cryptostatuses) {
            Cryptostatus status = getTransactionObject(cryptostatus);
            transactions.add(status);
        }

        return transactions;
    }

    public float getBalanceFloatFormat(float balance) {
        DecimalFormat decimalFormat = new DecimalFormat("#0.0000");
        float bal = balance;
        float fac = (float) Math.pow(10, 8);

        float result = bal / fac;

        String formattedBalance = decimalFormat.format(result);
        return Float.valueOf(formattedBalance);
        //return result;
    }

    public synchronized WalletManager getWalletManagerById(final Long id) {
        WalletManager walletManager = getWalletManager(id);
        return walletManager;
    }

    @Override
    public List<mobi.puut.service.entities.cloudmoney.Currency> currencys() {
        return cloudMoneyData.getCurrencies();
    }

    public Cryptostatus addCryptoHistoryDetails(Cryptoinfo cryptoinfo, WalletManager walletmanager) {
        WalletModel model = walletmanager.getModel();
        Wallet wallet = walletmanager.getBitcoin().wallet();
        List<Transaction> transactions = model.getTransactions();
        Cryptostatus status = new Cryptostatus();
        System.out.println("In History Detail function");
        for (Transaction transaction : transactions) {
            String transactionMessage = transaction.toString();

            if (!this.checkTransaction(transactionMessage, cryptoinfo.getId())) {
                System.out.println("In transaction function");
                String login = SecurityContextHolder.getContext().getAuthentication().getName();
                User user = this.usersData.getByUsername(login);
                Coin value = transaction.getValue(walletmanager.bitcoin.wallet());
                if (value.isPositive()) {
                    String message = "";
                    String fromAddres = "";

                    for (TransactionInput input : transaction.getInputs()) {

                        try {
                            Script script = input.getScriptSig();
                       
							Address frmAddress = script.getFromAddress(new WalletManager().networkParameters);
                            fromAddres = frmAddress.toBase58();
                        } catch (final ScriptException e) {

                        }

                    }

                    if (!fromAddres.equals("")) {
                        message = "Incoming payment of " + MonetaryFormat.BTC.format(value);
                    } else {
                        message = "Incoming payment of " + MonetaryFormat.BTC.format(value);
                    }
                    System.out.println("Incoming  Payment");
                    status = this.saveTransaction(user.getId(), cryptoinfo.getId(), cryptoinfo.getAddress(), transactionMessage, model.getBalance(), message, transaction.getUpdateTime().toString(), statusData.getTransactionTypeById(1L), fromAddres, null, value, null);
                } else if (value.isNegative()) {
                    String toAddress = "";

                    for (TransactionOutput output : transaction.getOutputs()) {
                        try {
                            if (!output.isMine(wallet)) {
                                Script script = output.getScriptPubKey();
                                Address toaddr = script.getToAddress(new WalletManager().networkParameters, true);
                                toAddress = toaddr.toBase58();
                            }
                        } catch (final ScriptException e) {
                        }
                    }

                    System.out.println("Outbound payment");
                    String message = "Outbound payment worth of "
                            + (MonetaryFormat.BTC.format(value)).toString().replaceAll("-", "");
                    //toAddress = ""+address;
                    System.out.println("toAddress" + toAddress);
                    status = this.saveTransaction(user.getId(), cryptoinfo.getId(), cryptoinfo.getAddress(), transactionMessage, model.getBalance(), message, transaction.getUpdateTime().toString(), statusData.getTransactionTypeById(2L), null, toAddress, null, value.multiply(-1L));

                } else {
                    System.out.println("Payment with id" + transaction.getHash());
                    String message = "Payment with id " + transaction.getHash();
                    status = this.saveTransaction(user.getId(), cryptoinfo.getId(), cryptoinfo.getAddress(), transactionMessage, model.getBalance(), message, transaction.getUpdateTime().toString(), statusData.getTransactionTypeById(3L), null, null, null, null);
                }

            }

        }
        return status;
    }

    public boolean checkTransaction(String transactionMessage, Long id) {
        if (statusData.getByTransaction(transactionMessage.length() > 90 ? transactionMessage.substring(0, 89) : transactionMessage, id) != null) {
            return true;
        }
        return false;
    }

    public String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private Boolean checkWalletStatus(User user) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date attemptTime = null;
        Date currentDateTime = new Date();
        List<Secure> secureList = this.walletInfoData.getSecureByUser(user);
        Secure secure = secureList.get(0);
        if (secure.getDisabled()) {
            return false;
        }
        return true;
    }
/*
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List getTransactionsByMonth(long id, String date) throws ParseException {
        this.init();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date formattedDate = dateFormat.parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(formattedDate);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        Date firstDate = cal.getTime();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.add(Calendar.MINUTE, 1439);
        Date lastDate = cal.getTime();
        return getTransaction(id, firstDate, lastDate);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Cryptostatus> getTransactionsBetweenDates(long id, String startDate, String endDate) throws ParseException {
        this.init();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date firstDate = dateFormat.parse(startDate);
        Date lastDate = dateFormat.parse(endDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastDate);
        cal.add(Calendar.MINUTE, 1439);
        lastDate = cal.getTime();
        return getTransaction(id, firstDate, lastDate);
    }

    public List<Cryptostatus> getTransaction(long id, Date firstDate, Date lastDate) throws ParseException {
        String fDate = servicesUtil.formatTransactionDateTime(firstDate.toString());
        String lDate = servicesUtil.formatTransactionDateTime(lastDate.toString());
        DecimalFormat decimalFormat = new DecimalFormat("#0.0000");
        List<Cryptostatus> cryptostatuses = statusData.getTransactionByDate(id, fDate, lDate);
        List<Cryptostatus> statuses = new ArrayList<Cryptostatus>();

        for (Cryptostatus cryptostatuse : cryptostatuses) {
            Cryptostatus status = getTransactionObject(cryptostatuse);
            statuses.add(status);
        }

        return statuses;
    }*/

    public Cryptostatus getTransactionObject(Cryptostatus cryptostatus) {
        Cryptostatus status = new Cryptostatus();
        status.setId(cryptostatus.getId());
        status.setAddress(cryptostatus.getAddress());
        status.setBalance(Float.valueOf(Coin.valueOf((long) cryptostatus.getBalance()).toPlainString()));
        status.setCoinin(Float.valueOf(Coin.valueOf((long) cryptostatus.getCoinin()).toPlainString()));
        status.setCoinout(Float.valueOf(Coin.valueOf((long) cryptostatus.getCoinout()).toPlainString()));
        status.setCryptotransactions(cryptostatus.getCryptotransactions());
        status.setIncomingaddress(cryptostatus.getIncomingaddress());
        status.setLastupdated(cryptostatus.getLastupdated());
        status.setOutgoingaddress(cryptostatus.getOutgoingaddress());
        status.setTransactionmessage(cryptostatus.getTransactionmessage());
        status.setUser_id(cryptostatus.getUser());
        status.setWalletid(cryptostatus.getWalletid());
        User user = usersData.get(cryptostatus.getUser());
        Personal personal = usersData.getPersonal(user);
        user.setPimage(personal.getImage());
        mobi.puut.service.entities.cloudmoney.Currency currency = cloudMoneyData.getCurrencyByCode("BTC");
        status.setProfile(user);
        status.setLogo(currency.getCurrencylogo());
        return status;
    }

  /*  @Override
    @Transactional
    public Result requestbtc(RequestMoney requestMoney) {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        Result result = new Result();
        try{
            String phone = requestMoney.getPhone();
            String amount = requestMoney.getAmount();
            float reqAmount = parseCoin(amount).getValue();
            User toUser = usersData.getUserByPhone(phone);
            User fromUser = usersData.getUserByLogin(login);
            Cryptorequest cryptorequest = new Cryptorequest();
            cryptorequest.setFromuser(fromUser);
            cryptorequest.setTouser(toUser);
            cryptorequest.setRequestbtc(reqAmount);
            walletInfoData.saveBTCRequest(cryptorequest);
            if(requestMoney.getAmount().isEmpty() || requestMoney.getAmount() == null){
                 result.setSuccess(Boolean.FALSE);
                 result.setError("Please Enter Valid Amount");
                 return result;
            }
            
            String toaddress=null;
            User touser = new User();
            User fromuser = new User();
            if(requestMoney.getToAddress()==null){
                if(requestMoney.getPhone()!=null){
                
                touser=usersData.getUserByPhone(requestMoney.getPhone());
                Cryptoinfo cryptoinfo=walletInfoData.getDefaultWallet(touser);
                
                if(cryptoinfo!= null){
                 toaddress=cryptoinfo.getAddress();
                }else{
                    result.setSuccess(Boolean.FALSE);
                    result.setError("No wallet present for given phone number.");
                    return result;
                }
                }
            }else{
                toaddress = requestMoney.getToAddress();
                Cryptoinfo cryptoinfo=walletInfoData.getByAddress(toaddress); 
                 if(cryptoinfo!= null){
                  touser=cryptoinfo.getUser();
                }else{
                    result.setSuccess(Boolean.FALSE);
                    result.setError("No wallet present for given address of target user.");
                    return result;
                }
               
            }
                String fromaddress=requestMoney.getFromAddress();
                Cryptoinfo cryptoinfo1=walletInfoData.getByAddress(fromaddress);
                if(cryptoinfo1!= null){
                    fromuser=cryptoinfo1.getUser();
                }else{
                    result.setSuccess(Boolean.FALSE);
                    result.setError("No wallet present for given address of source user.");
                    return result;
                }
                
                Cryptorequest request=new Cryptorequest();
                request.setRequestbtc(parseCoin(requestMoney.getAmount()).getValue());
                request.setFromaddress(fromaddress);
                request.setToaddress(toaddress);
                request.setFromuser(fromuser);
                request.setTouser(touser);            
                walletInfoData.saveBTCRequest(request);
                
                mobi.puut.service.entities.messages.Message message = new  mobi.puut.service.entities.messages.Message();
                message.setTitle("Money Request");
                message.setBody("You have recieved money request from "+fromuser.getFirstname()+" "+fromuser.getLastname()+ " of amount "+requestMoney.getAmount() +" BTC");
                message.setUserfrom(fromuser);
                message.setUserto(touser);
                message.setMessagedate(servicesUtil.getFormattedCurrentDateTime());
                messagesData.add(message);
                        
                
                result.setSuccess(Boolean.TRUE);
                result.setSuccessMessage("Request updated successfully.");
        }catch(Exception e){
            result.setSuccess(Boolean.FALSE);
            result.setError("Failed to request for given user.");
        }        
        return result;
    }

    @Override
    @Transactional
    public List<Cryptorequest> getRecievedBtcRequest() {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getUserByLogin(login);
        List<Cryptorequest> cryptorequests = this.walletInfoData.getBTCRecievedRequests(user);
        List<Cryptorequest> requests = new ArrayList<Cryptorequest>();
        for (Cryptorequest cryptorequest : cryptorequests) {
            requests.add(getFormattedCryptorequest(cryptorequest));
        }
        return requests;
    }

    
     @Override
    @Transactional
    public List<Cryptorequest> getSentBtcRequest() {
        this.init();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getUserByLogin(login);
        List<Cryptorequest> cryptorequests = this.walletInfoData.getBTCSentRequests(user);
        List<Cryptorequest> requests = new ArrayList<Cryptorequest>();
        for (Cryptorequest cryptorequest : cryptorequests) {
            requests.add(getFormattedCryptorequest(cryptorequest));
        }
        return requests;
    }
    
    
    @Override
    @Transactional
    public Cryptorequest getBTCRequestByid(long id) {
        this.init();
        Cryptorequest cryptorequest = this.walletInfoData.getBTCRequestByid(id);
        return getFormattedCryptorequest(cryptorequest);
    }

    public Cryptorequest getFormattedCryptorequest(Cryptorequest request) {
        this.init();
        Cryptorequest cryptorequest = new Cryptorequest();
        cryptorequest.setId(request.getId());
        cryptorequest.setFromuser(request.getFromuser());
        cryptorequest.setTouser(request.getTouser());
        cryptorequest.setRequestbtc(Float.valueOf(Coin.valueOf((long) request.getRequestbtc()).toPlainString()));
        cryptorequest.setIssent(request.getIssent());
        cryptorequest.setFromaddress(request.getFromaddress());
        cryptorequest.setToaddress(request.getToaddress());
        cryptorequest.setCurrencyLogo(cloudMoneyData.getCurrencyByCode("BTC").getCurrencylogo());
        return cryptorequest;
    }    

    
    @Override
    @Transactional
    public Result updateRequestBTC(long id, String amount) {
        this.init();
        Result result = new Result();
        try{
            Cryptorequest cryptorequest = this.walletInfoData.getBTCRequestByid(id);
            float amt = parseCoin(amount).getValue();
            cryptorequest.setAmountsent(amt);
            cryptorequest.setIssent(Boolean.TRUE);
            this.walletInfoData.updateBTCRequest(cryptorequest);
            result.setSuccess(Boolean.TRUE);
            result.setSuccessMessage("Request updated successfully.");
        }catch(Exception e){
            result.setSuccess(Boolean.FALSE);
            result.setError("Failed to request for givrn user.");
        }
        return result;
    }*/

	@Override
	public List getWalletTransactionById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getTransactionsByMonth(long id, String date) throws ParseException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Cryptostatus> getTransactionsBetweenDates(long id, String startDate, String endDate)
			throws ParseException {
		// TODO Auto-generated method stub
		return null;
	}
}
