
package mobi.puut.service.entities.geographic;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mobi.puut.service.entities.users.IPlocations;


@Entity
@Table(name="ipcheck")
public class IPCheck{
    private static final long serialVersionUID = 1L;

    @Column(name="ip_from")
    private int ip_from;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ip_to")
    private int ip_to;
    @Column(name="network")
    private String network;
    @ManyToOne(targetEntity=IPlocations.class)
    @JoinColumn(name="geoname_id")
    private IPlocations iplocations;
    @Column(name="registered_country_geoname_id")
    private int registered_country_geoname_id;
    @Column(name="represented_country_geoname_id")
    private int represented_country_geoname_id;
    @Column(name="is_anonymous_proxy")
    private boolean is_anonymous_proxy;
    @Column(name="is_satellite_provider")
    private boolean is_satellite_provider;
    @Column(name="postal_code")
    private String postal_code;
    @Column(name="latitude")
    private float latitude;
    @Column(name="longitude")
    private float longitude;
    @Column(name="accuracy_radius")
    private int accuracy_radius;

    public int getIp_from() {
        return ip_from;
    }

    public void setIp_from(int ip_from) {
        this.ip_from = ip_from;
    }

    public int getIp_to() {
        return ip_to;
    }

    public void setIp_to(int ip_to) {
        this.ip_to = ip_to;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public IPlocations getIplocations() {
        return iplocations;
    }

    public void setIplocations(IPlocations iplocations) {
        this.iplocations = iplocations;
    }

    

    public int getRegistered_country_geoname_id() {
        return registered_country_geoname_id;
    }

    public void setRegistered_country_geoname_id(int registered_country_geoname_id) {
        this.registered_country_geoname_id = registered_country_geoname_id;
    }

    public int getRepresented_country_geoname_id() {
        return represented_country_geoname_id;
    }

    public void setRepresented_country_geoname_id(int represented_country_geoname_id) {
        this.represented_country_geoname_id = represented_country_geoname_id;
    }

    public boolean isIs_anonymous_proxy() {
        return is_anonymous_proxy;
    }

    public void setIs_anonymous_proxy(boolean is_anonymous_proxy) {
        this.is_anonymous_proxy = is_anonymous_proxy;
    }

    public boolean isIs_satellite_provider() {
        return is_satellite_provider;
    }

    public void setIs_satellite_provider(boolean is_satellite_provider) {
        this.is_satellite_provider = is_satellite_provider;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getAccuracy_radius() {
        return accuracy_radius;
    }

    public void setAccuracy_radius(int accuracy_radius) {
        this.accuracy_radius = accuracy_radius;
    }

}
