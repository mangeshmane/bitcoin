
package mobi.puut.service.data.impl.chat;

import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.data.UserData;
import mobi.puut.service.data.def.chat.ParticipantData;
import mobi.puut.service.entities.chat.Participant;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@Repository
public class ParticipantDataImpl implements ParticipantData {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    UserData userData;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Transactional(rollbackFor=Exception.class)
    public void save(Participant p) {
        sessionFactory.getCurrentSession().persist(p);
    }

    @Transactional(rollbackFor=Exception.class)
    public void update(Participant p) {
        sessionFactory.getCurrentSession().update(p);
    }

    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public Participant find(long id) {
        return (Participant) sessionFactory.getCurrentSession().get(Participant.class, id);
    }

    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Participant> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT p FROM Participant p")
                .list();
    } 
    
    /**
     * Find a list of participants which take part in a conversation
     * @param convId The specific conversation ID
     * @return A list of Participants
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Participant> findByConv(long convId) {
        List<Participant> participants = sessionFactory.getCurrentSession()
                .createQuery("SELECT p FROM Participant p WHERE p.conversationIdParticipants.id = :convId")
                .setParameter("convId", convId)
                .list();
        
        for (Participant participant : participants) {
            Personal personal = userData.getPersonal(participant.getUser());
            if(personal != null) {
            participant.getUser().setPimage(personal.getImage());
            }
        }
        return participants;           
    }

    /**
     * Find the participant corresponding to a conversation and an user
     * @param convId The conversation ID
     * @param userId The user ID
     * @return A list of participant (maximum size = 1)
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Participant> findWithConvAndUser(long convId, long userId) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT p FROM Participant p WHERE p.conversationIdParticipants.id = :convId AND p.userId.id = :userId")
                .setParameter("convId", convId)
                .setParameter("userId", userId)
                .list();
    }
    
    /**
     * To count the number of participants for a conversation
     * @param convId The conversation ID
     * @return The number of participants
     */
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public Integer countParticipants(long convId) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT p FROM Participant p WHERE p.conversationIdParticipants.id = :convId").setParameter("convId", convId)
                .list().size();
    }
    
    
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public List<Participant> findByUser(User user){
      return sessionFactory.getCurrentSession()
                .createQuery("SELECT p FROM Participant p WHERE p.userId =:user",Participant.class).setParameter("user", user).getResultList();
    }
    
}
