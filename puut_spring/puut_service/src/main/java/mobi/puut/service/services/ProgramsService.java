package mobi.puut.service.services;

import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.documents.Result;

public interface ProgramsService {

	Result save(Program program);

}
