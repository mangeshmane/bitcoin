/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.data.def.chat;

import java.util.List;
import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.users.User;

/**
 *
 * @author Nathan Audin
 */
public interface ConversationData {
    public void save(Conversation c);
    public void update(Conversation c);
    public Conversation find(long id);
    public List<Conversation> findAll();
    public List<Conversation> findByName(String conv);
    public List<Conversation> findByNameAndOwner(String conv, Long owner);
    List<Conversation> findParticipantsByTerm(String term);   
    List<Conversation> findConvByTerm(String term);
    // ADDED FROM API //
    public List<Conversation> findConversationByUser(User user);
}
