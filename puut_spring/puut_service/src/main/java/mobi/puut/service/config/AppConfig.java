package mobi.puut.service.config;

import java.io.IOException;
import java.util.Properties;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.cxf.bus.spring.SpringBus;
//import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.provider.json.JSONProvider;
//import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
//import org.springframework.context.annotation.ImportResource;
//import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import mobi.puut.service.EncodingUtil;
import mobi.puut.service.MailUtil;
import mobi.puut.service.PuutUtil;
import mobi.puut.service.data.CloudMoneyData;
import mobi.puut.service.data.impl.CloudMoneyDataImpl;
import mobi.puut.service.services.ProfileService;
import mobi.puut.service.services.ShoppingWallService;
import mobi.puut.service.services.UserService;
import mobi.puut.service.services.UserServiceImpl;
import mobi.puut.service.services.crypto.def.WalletService;
import mobi.puut.service.services.crypto.impl.WalletServiceImpl;
import mobi.puut.service.services.impl.GeographicServiceImpl;
import mobi.puut.service.services.impl.ProfileServiceImpl;
import mobi.puut.service.services.impl.ShoppingWallServiceImpl;

@Configuration
@ComponentScan(basePackages = {"mobi.puut.service.*"})
@EnableWebMvc
public class AppConfig {

	@Autowired
	Environment env;
	
	@Bean
	public RequestContextListener requestContextListener(){
		return new RequestContextListener();
	}
	
	@Bean
	public mobi.puut.service.ServicesUtil servicesUtil(){
		return new mobi.puut.service.ServicesUtil();
	}
	
	@Bean
	public EncodingUtil encodingUtil(){
		return new EncodingUtil();
	}
	
	@Bean
	public MailUtil mailUtil(){
		return new MailUtil();
	}

@Bean
public PuutUtil puutUtil()
{
	return new PuutUtil();
}



@Bean
public mobi.puut.service.services.GeographicService GeographicService()
{
	return new GeographicServiceImpl();
}


@Bean
public  ProfileService profileService()
{
	return new ProfileServiceImpl();
}

	
	@Bean
	public mobi.puut.service.PatternUtil patternUtil(){
		return new mobi.puut.service.PatternUtil();
	}
	
	@Bean
	DriverManagerDataSource dataSource() {
		DriverManagerDataSource source=new DriverManagerDataSource();
		source.setDriverClassName("com.mysql.jdbc.Driver");
		source.setUrl("jdbc:mysql://localhost:3306/puut_data");
		source.setUrl("jdbc:mysql://localhost/puut_data?createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=UTF-8");
		source.setUsername("root");
		source.setPassword("root");
		
		return source;
		
	}
	
	@Bean(name = "multipartResolver")
    public CommonsMultipartResolver getResolver() throws IOException {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        // no limit
        resolver.setMaxUploadSize(-1);
        return resolver;
    }
	
	@Bean
	SessionFactory sessionFactory()
	{
		LocalSessionFactoryBean factory=new LocalSessionFactoryBean();
		Properties properties=new Properties();
		properties.setProperty("hibernate.show_sql","true");
		properties.setProperty("hibernate.dialect","org.hibernate.dialect.MySQLInnoDBDialect");
		properties.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		properties.setProperty("hibernate.connection.charSet", "UTF-8");
		properties.setProperty("hibernate.jdbc.batch_size", "20");
		properties.setProperty("hibernate.hbm2dll.auto", "update");
		properties.setProperty("javax.persistence.schema-generation.database.action", "create");
		factory.setDataSource(dataSource());
		factory.setPackagesToScan("mobi.puut.service.entities.*");
		factory.setHibernateProperties(properties);

		try {
			factory.afterPropertiesSet();
			
		}catch(IOException ex) {
			
		}
		
		return factory.getObject();
		
		
	}
	
	
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
	
	@Bean
	HibernateTransactionManager transactionManager() {
		HibernateTransactionManager manager=new HibernateTransactionManager();
		manager.setSessionFactory(sessionFactory());
		return manager;
	}
	
	
	@Bean(name="jacksonMapper")
	public ObjectMapper jacksonMapper() {
		return new ObjectMapper();
	}
	
	
	@Bean(name="walletService")
	public WalletService walletService(){
		return new WalletServiceImpl();
	}
		
	@Bean(name="usersService")
	public UserService userService()
	{
		return new UserServiceImpl();
	}
	
	
	@Bean(name="cloudData")
	public CloudMoneyData cloudMoneyData()
	{
		return new CloudMoneyDataImpl();
	}
	
	
	/*@Bean(name="exceptionMapper") 
	public SecurityExceptionMapper exceptionMapper() {
		return new SecurityExceptionMapper();
	}*/
	
	
	@Bean(name="CORSFilter")
	public CORSFilter cORSFilter()
	{
		return new CORSFilter();
	}
	
	
	@Bean(name="jacksonJaxbJsonProvider")
	public JacksonJaxbJsonProvider jacksonJaxbJsonProvider()
	{
		return new JacksonJaxbJsonProvider();
	}
	
	
	@Bean(name="JSONProvider")
	public JSONProvider jSONProvider()
	{
		JSONProvider jSONProvider = new JSONProvider();
		jSONProvider.setDropRootElement(true);
		jSONProvider.setDropCollectionWrapperElement(true);
		jSONProvider.setSerializeAsArray(true);
		jSONProvider.setSupportUnwrapped(true);
		
		return jSONProvider;
		
	}
	
//	@Bean(name="restContainer")
//	@DependsOn("cxf")
//	public Server jaxRs(ApplicationContext appcontext) {
//		List<Object> providers = new ArrayList<Object>();
//		//providers.add(jSONProvider());
//		//providers.add(exceptionMapper());
//		providers.add(cORSFilter());
//	   JacksonJaxbJsonProvider jsonProvider =  jacksonJaxbJsonProvider();
//	   jsonProvider.setMapper(jacksonMapper());
//	   providers.add(jSONProvider());
//	   
//	   
//	   JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(jaxRsApiApplication(), JAXRSServerFactoryBean.class);
//	   factory.setProvider(providers);
//	   factory.setAddress("/");
//	   factory.setBus(cxf());
//	   
//	   
//	   Map<String,Object> properties = new HashMap<String,Object>();
//	   properties.put("attachment-directory", "/temp/bookstore");
//	   properties.put("attachment-memory-threshold", "50000000");
//	   properties.put("attachment-max-size","50000000");
//	   factory.setProperties(properties);
//	   
//	   final List<Object> servicebeans = new ArrayList<Object>();
//	   servicebeans.add(walletService());
//	   servicebeans.add(userService());
//	   
//	   factory.setServiceBean(servicebeans);
//	   return factory.create();
//	   
//		
//	}
	
	
	@ApplicationPath("/*")	
	public class JaxRsApiApplication extends Application{}
	
	@Bean(destroyMethod = "shutdown")	
	public SpringBus cxf() {
		return new SpringBus();
	}
	
	@Bean
	public JaxRsApiApplication jaxRsApiApplication() {
		return new JaxRsApiApplication(); 
		
	}
	
	@Bean
	public JacksonJsonProvider jsonProvider(){
		return new JacksonJsonProvider(); 
		
	}
	
	
	@Bean
	public CustomAuthenticationProvider customAuthenticationProvider(){
		return new CustomAuthenticationProvider(); 
		
	}
	

	@Bean
	public ShoppingWallService shoppingWallService(){
		return new ShoppingWallServiceImpl(); 
		
	}
	
	
	@Bean
	public 	DoNotTruncateMyUrls doNotTruncateMyUrls(){
		return new 	DoNotTruncateMyUrls(); 
		
	}
	

/*	
 * 
 * 
 * 
 * 
	@Bean
	ContentNegotiationManager contentNegotiationManager=new ContentNegotiationManager();
		*/
	
	
	
	
}
