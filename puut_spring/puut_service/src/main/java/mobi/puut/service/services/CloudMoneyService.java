package mobi.puut.service.services;

import java.io.IOException;
import java.util.List;

import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.BankWithdrawal;
import mobi.puut.service.entities.shoppingwall.Transaction;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.User;

public interface CloudMoneyService {

	List<Currency> getCurrencies();

	List<User> getAgents();

	List<CloudMoney> getAccountBalance();

	List<Transaction> getTransactions();

	List<Document> getBills();

	List<Document> getCreditCards();

	Result sendMoney(Transaction transaction);

	Result withdrawMoney(BankWithdrawal bankWithdrawal) throws IOException;

}
