
package mobi.puut.service.data.impl.chat;

import java.util.List;
import mobi.puut.service.data.def.chat.UserToUserData;
import mobi.puut.service.entities.chat.UserToUser;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class UserToUserDataImpl implements UserToUserData {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Transactional(rollbackFor=Exception.class)
    public void save(UserToUser userToUser) {
        sessionFactory.getCurrentSession().persist(userToUser);
    }
    
    @Transactional(rollbackFor=Exception.class)
    public void update(UserToUser userToUser) {
        //sessionFactory.getCurrentSession().update(userToUser);
         sessionFactory.getCurrentSession().saveOrUpdate(userToUser);
    } 
        
    @Transactional(readOnly = true, rollbackFor=Exception.class)
    public UserToUser find(long id){
        return sessionFactory.getCurrentSession().get(UserToUser.class, id);
    }
    
    @Transactional(readOnly =true, rollbackFor=Exception.class)
    public List<UserToUser> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT u FROM UserToUser u",UserToUser.class)
                .list();
    }
    
    /**
     * To find UserToUser relation (for blocking information)
     * @param userId1 First user
     * @param userId2 Second user
     * @return Return the corresponding relation (as a list)
     */
    @Transactional(readOnly =true, rollbackFor=Exception.class)
    public List<UserToUser> findByUsersIds(long userId1, long userId2) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT u FROM UserToUser u WHERE u.user1Id.id = :userId1 AND u.user2Id.id = :userId2",UserToUser.class)
                .setParameter("userId1", userId1)
                .setParameter("userId2", userId2)
                .list();
    }
}
