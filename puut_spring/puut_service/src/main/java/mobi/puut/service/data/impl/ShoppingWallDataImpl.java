package mobi.puut.service.data.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.ShoppingWallData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;
import mobi.puut.service.entities.shoppingwall.ShoppingListUser;
import mobi.puut.service.entities.shoppingwall.ShoppingType;
import mobi.puut.service.entities.users.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
@Transactional(rollbackFor=Exception.class)
public class ShoppingWallDataImpl implements ShoppingWallData {
    
    @Autowired
    SessionFactory sessionFactory;
    
    
    @Lazy
    @Autowired
    ServicesUtil servicesUtil;
    
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }


    public List<ShoppingGroup> getGroups() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List shoppingGroups = session.createQuery("from ShoppingGroup").setMaxResults(2000).getResultList();

        return shoppingGroups;
    }

    public List<ShoppingGroupUser> getUsers(User user) {
        this.init();
        
       Session session = sessionFactory.getCurrentSession();
        
       List shoppingGroups = session.createQuery("from ShoppingGroupUser groupuser where user=:user").setParameter("user", user).getResultList();
       
       return shoppingGroups;
    }

    public Result createGroup(ShoppingGroup group, String login) {
        this.init();
        
        Result result = new Result();
        result.setResult(servicesUtil.getShoppingGroupErrors(group));
        
        if (result.getSuccess()) {
            Session session = sessionFactory.getCurrentSession();
            
            User user = (User) session.createQuery("from User user where username=:login").setParameter("login", login).getSingleResult();
            //group.setCity_name(user.getCity_name());
            if (user.getCity().getCity_name() != null) {
                sessionFactory.getCurrentSession().save(group);

                ShoppingGroupUser shoppinggroupuser = new ShoppingGroupUser();
                shoppinggroupuser.setShoppinggroup(group);
                shoppinggroupuser.setUser(user);
                shoppinggroupuser.setModerator(Boolean.TRUE);
                shoppinggroupuser.setApproved(Boolean.TRUE);
                sessionFactory.getCurrentSession().save(shoppinggroupuser);
            } else {
                result.setSuccess(false);
                result.setError("DONTHAVECITY");
            }
        }
        
        return result;
    }

 
    public Result joinGroup(Long id, String login) {
        this.init();
        
        Result result = new Result();
        result.setSuccess(true);
        
        Session session = sessionFactory.getCurrentSession();
        
        
        User user = (User) session.createQuery("from User user where username=:login").setParameter("login", login).getSingleResult();

        ShoppingGroup group = (ShoppingGroup)sessionFactory.getCurrentSession()
                .get(ShoppingGroup.class, id);

        if (group != null) {
            session = sessionFactory.getCurrentSession();

            ShoppingGroupUser groupuser = (ShoppingGroupUser) session.createQuery("from ShoppingGroupUser groupuser where shoppinggroup=:shoppinggroup and user=:user")
                    .setParameter("shoppinggroup", group)
                    .setParameter("user", user)
                    .uniqueResult();

            //if (group.getCity().getId() == user.getCity().getId()) {
            if (groupuser == null) {
                groupuser = new ShoppingGroupUser();
                groupuser.setShoppinggroup(group);
                groupuser.setUser(user);
                groupuser.setModerator(Boolean.TRUE);
                groupuser.setApproved(Boolean.TRUE);
                sessionFactory.getCurrentSession().save(groupuser);
                result.setSuccess(true);
            } else {
//                    ShoppingGroupUser newShoppingGroupUser = new ShoppingGroupUser();
//                    newShoppingGroupUser.setShoppinggroup(group);
//                    newShoppingGroupUser.setUser(user);
//                    newShoppingGroupUser.setModerator(Boolean.FALSE);
//                    newShoppingGroupUser.setApproved(Boolean.FALSE);
//                    sessionFactory.getCurrentSession().save(newShoppingGroupUser);
//                    result.setSuccess(true);
               
                result.setSuccess(false);
                if(groupuser.isApproved()){
                    result.setError("IN GROUP");
                    result.setSuccessMessage("Already in group");
                }else{
                    result.setError("Already Requested");
                }
                
            }
            /*} else {
                result.setSuccess(false);
                result.setError("NOGROUP");
            }*/
        } else {
            result.setSuccess(false);
            result.setError("NOGROUP");
            
        }
        
        return result;
    }
    
    
    public Result leaveGroup(Long id, String login) {
        this.init();
        
        Result result = new Result();
        result.setSuccess(true);
        
        Session session = sessionFactory.getCurrentSession();
        
        User user = (User) session.createQuery("from User user where login=:login").setParameter("login", login).getSingleResult();

        ShoppingGroup group = (ShoppingGroup)sessionFactory.getCurrentSession()
                .get(ShoppingGroup.class, id);

        if (group != null) {
            session = sessionFactory.getCurrentSession();

            ShoppingGroupUser groupuser = (ShoppingGroupUser) session.createQuery("from ShoppingGroupUser where shoppinggroup=:shoppinggroup and user=:user")
                    .setParameter("shoppinggroup",group)
                    .setParameter("user",user)
                    .uniqueResult();
            //if (group.getCity().getId() == user.getCity().getId()) {
            
            if (groupuser != null) {
                sessionFactory.getCurrentSession()
                        .delete(groupuser);
            } else {
                result.setSuccess(false);
                result.setError("NOTINGROUP");
            }
            /*} else {
                result.setSuccess(false);
                result.setError("NOGROUP");
            }*/
        } else {
            result.setSuccess(false);
            result.setError("NOGROUP");
        }
        
        return result;
    }

    public List<ShoppingList> getLists(User user, Long list_type) {
        this.init();
        
        return sessionFactory.getCurrentSession()
                .createQuery("from ShoppingList as sl "
                + "where sl.shoppingtype.id = :shoppingtype_id and exists"
                + "(from ShoppingGroupUser as sgu "
                + "where sgu.shoppinggroup = sl.shoppinggroup "
                + "and sgu.user = :user)")
                .setParameter("shoppingtype_id", list_type)
                .setParameter("user", user)
                .list();
    }

    public List<ShoppingListUser> getListUsers(User user) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        
        List<ShoppingListUser> users = session.createQuery("from ShoppingListUser listuser where user=:user")
                .setParameter("user", user)
                .getResultList();        
        
        return users;
    }

    public Result createList(ShoppingList list, String login) {
        this.init();
        
        Result result = new Result();
        result.setResult(servicesUtil.getShoppingListErrors(list));
        
        if (result.getSuccess()) {

            Session session = sessionFactory.getCurrentSession();
        

            User user = (User) session.createQuery("from User user where login=:login")
                    .setParameter("login", login)
                    .getSingleResult();

            list.setDiscount(0);
            
            if(list.getPrice() == null){
                list.setPrice(BigDecimal.ZERO);
                list.setOffer(BigDecimal.ZERO);
            }else{
                list.setOffer(list.getPrice());
            }
            
            
            
            list.setShoppers(1);
            Calendar cal = Calendar.getInstance(); 
            cal.add(Calendar.MONTH, 1);
            list.setExpiration(cal.getTime());
            ShoppingType type = new ShoppingType();
            type.setId(1L);
            list.setShoppingtype(type);
            list.setMerchant(user);
            sessionFactory.getCurrentSession()
                    .save(list);
              sessionFactory.getCurrentSession()
                    .saveOrUpdate(list);

            ShoppingListUser shoppinglistuser = new ShoppingListUser();
            shoppinglistuser.setShoppinglist(list);
            shoppinglistuser.setUser(user);
            
            sessionFactory.getCurrentSession()
                    .save(shoppinglistuser);
           sessionFactory.getCurrentSession()
                    .saveOrUpdate(shoppinglistuser);
           
        }
        
        return result;
    }

    public Result joinList(Long id, String login) {
        this.init();
        
        Result result = new Result();
        result.setSuccess(true);
        
        Session session = sessionFactory.getCurrentSession();

        User user = (User) session.createQuery("from User user where login=:login")
                    .setParameter("login", login)
                    .uniqueResult();

        ShoppingList list = (ShoppingList)sessionFactory.getCurrentSession()
                .get(ShoppingList.class, id);
        
        if (list != null) {
            if (list.getShoppingtype().getId() == 1) {
                list.setShoppers(list.getShoppers() + 1);
                sessionFactory.getCurrentSession()
                        .saveOrUpdate(list);
                
                session = sessionFactory.getCurrentSession();

                ShoppingGroupUser shoppinggroupuser = (ShoppingGroupUser) session.createQuery("from ShoppingGroupUser groupuser where shoppinggroup=:shoppinggroup and user=:user")
                        .setParameter("shoppinggroup", list.getShoppinggroup())
                        .setParameter("user", user)
                        .uniqueResult();
                
                if (shoppinggroupuser != null) {
                    session = sessionFactory.getCurrentSession();


                    ShoppingListUser listuser = (ShoppingListUser) session.createQuery("from ShoppingListUser listuser where listuser.shoppinglist=:shoppinglist and listuser.user=:user")
                            .setParameter("shoppinglist", list)
                            .setParameter("user", user)
                            .uniqueResult();

                    if (listuser == null) {
                        listuser = new ShoppingListUser();
                        listuser.setShoppinglist(list);
                        listuser.setUser(user);
                        sessionFactory.getCurrentSession()
                                .save(listuser);
                    } else {
                        result.setSuccess(false);
                        result.setError("INLIST");
                    }
                } else {
                    result.setSuccess(false);
                    result.setError("NOTINGROUPTOOFFER");
                }
            } else {
                result.setSuccess(false);
                result.setError("NOLIST");
            }
        } else {
            result.setSuccess(false);
            result.setError("NOLIST");
        }
        
        return result;
    }

    public Result leaveList(Long id, String login) {
        this.init();
        
        Result result = new Result();
        result.setSuccess(true);
        
        Session session = sessionFactory.getCurrentSession();
        
        User user = (User) session.createQuery("from User user where username=:login")
                    .setParameter("login", login)
                    .getSingleResult();
        
        ShoppingList list = (ShoppingList)sessionFactory.getCurrentSession()
                .get(ShoppingList.class, id);
        
        if (list != null) {
            if (list.getShoppingtype().getId() == 1) {
                Integer shoppers = list.getShoppers();
                list.setShoppers(shoppers);
                sessionFactory.getCurrentSession()
                        .saveOrUpdate(list);

                session = sessionFactory.getCurrentSession();

                ShoppingGroupUser shoppinggroupuser = (ShoppingGroupUser) session.createQuery("from ShoppingGroupUser groupuser where groupuser.shoppinggroup=:shoppinggroup and groupuser.user=:user")
                        .setParameter("shoppinggroup", list.getShoppinggroup())
                        .setParameter("user", user)
                        .uniqueResult();
                
                if (shoppinggroupuser != null) {
                    
                    session = sessionFactory.getCurrentSession();

                    ShoppingListUser listuser = (ShoppingListUser) session.createQuery("from ShoppingListUser listuser where shoppinglist=:shoppinglist and user=:user")
                            .setParameter("shoppinglist", list)
                            .setParameter("user", user)
                            .uniqueResult();

                    if (listuser != null) {
                        sessionFactory.getCurrentSession()
                                .delete(listuser);
                    } else {
                        result.setSuccess(false);
                        result.setError("NOTINLIST");
                    }
                } else {
                    result.setSuccess(false);
                    result.setError("NOTINGROUPTOOFFER");
                }
            } else {
                result.setSuccess(false);
                result.setError("NOLIST");
            }
        } else {
            result.setSuccess(false);
            result.setError("NOLIST");
        }
        
        return result;
    }

    
    public ShoppingGroup getShoppingGroupById(Long id) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        ShoppingGroup group = session.createQuery("from ShoppingGroup group where id=:id",ShoppingGroup.class)
                    .setParameter("id",id)
                    .uniqueResult();
        return group;
    }
    
    
   
    public List<ShoppingGroupUser> getShoppingGroupUsers(ShoppingGroup shoppinggroup) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        List<ShoppingGroupUser> groupuser = session.createQuery("from ShoppingGroupUser groupuser where shoppinggroup=:shoppinggroup")
                    .setParameter("shoppinggroup",shoppinggroup)
                    .getResultList();
        return groupuser;
    }

   
    public List<ShoppingGroup> getShoppingGroupByName(String name) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        List<ShoppingGroup> groupuser = session.createQuery("from ShoppingGroup group where group.name like:name")
                    .setParameter("name","%"+name+"%")
                    .getResultList();
        return groupuser;
    }

    
    public ShoppingGroupUser getGroupUserByGroupAndUser(User user, ShoppingGroup shoppinggroup,Boolean approved) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        ShoppingGroupUser shoppingGroupUser = session.createQuery("from ShoppingGroupUser shoppingGroupUser where shoppingGroupUser.user =:user and shoppingGroupUser.shoppinggroup =:shoppinggroup and shoppingGroupUser.approved =:approved ",ShoppingGroupUser.class)
                                            .setParameter("user",user)
                                            .setParameter("shoppinggroup",shoppinggroup)
                                            .setParameter("approved",approved)
                                            .uniqueResult();
        return shoppingGroupUser;
    }
    
   
    public ShoppingGroupUser getNonModeratorGroupUser(User user, ShoppingGroup shoppinggroup) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        ShoppingGroupUser shoppingGroupUser = session.createQuery("from ShoppingGroupUser shoppingGroupUser where shoppingGroupUser.user =:user and shoppingGroupUser.shoppinggroup =:shoppinggroup and shoppingGroupUser.moderator =:moderator ",ShoppingGroupUser.class)
                                            .setParameter("user",user)
                                            .setParameter("shoppinggroup",shoppinggroup)
                                            .setParameter("moderator",Boolean.FALSE)
                                            .uniqueResult();
        return shoppingGroupUser;
    }

    public ShoppingGroupUser createShoppingGroupUser(ShoppingGroupUser shoppingGroupUser) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(shoppingGroupUser);
        return shoppingGroupUser;
      }
    
    
    public Boolean checkModerator(User user, ShoppingGroup shoppinggroup) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        
        List<ShoppingGroupUser> shoppingGroupUser = session.createQuery("from ShoppingGroupUser shoppingGroupUser where shoppingGroupUser.user =:user and shoppingGroupUser.shoppinggroup =:shoppinggroup and shoppingGroupUser.moderator =:moderator ",ShoppingGroupUser.class)
                                            .setParameter("user",user)
                                            .setParameter("shoppinggroup",shoppinggroup)
                                            .setParameter("moderator",Boolean.TRUE)
                                            .getResultList();
        if(!shoppingGroupUser.isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    public Result editGroup(ShoppingGroup group) {
        this.init();
        
        Result result = new Result();
        result.setResult(servicesUtil.getShoppingGroupErrors(group));
        
        if (result.getSuccess()) {
            sessionFactory.getCurrentSession().saveOrUpdate(group);
            result.setSuccess(Boolean.TRUE);
        }else {
            result.setSuccess(false);
            result.setError("FAILED TO SAVE");
         }
        return result;
    }
    
     public Boolean isShoppingListUser(User user, ShoppingList shoppinglist) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        List<ShoppingListUser> shoppingListUser = session.createQuery("from ShoppingListUser shoppingListUser where shoppingListUser.shoppinglist =:shoppinglist and shoppingListUser.user =:user ",ShoppingListUser.class)
                                            .setParameter("shoppinglist",shoppinglist)
                                            .setParameter("user",user)
                                            .getResultList();
        if(!shoppingListUser.isEmpty()){
          return true;
        }
        else{
          return false;
        }
    }
     
    public Result editShoppingList(ShoppingList shoppinglist){
        this.init();
        
        Result result = new Result();
        result.setResult(servicesUtil.getShoppingListErrors(shoppinglist));
        
        if (result.getSuccess()) {
             sessionFactory.getCurrentSession()
                    .saveOrUpdate(shoppinglist);
                result.setSuccess(Boolean.TRUE);
        }else {
            result.setSuccess(false);
            result.setError("FAILED TO SAVE SHOPPINGLIST");
         }
        return result;
    }
    
    
    public List<User> getGroupUsers(ShoppingGroup shoppinggroup) {
        this.init();
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session.createQuery("select shoppingGroupUser.user from ShoppingGroupUser shoppingGroupUser where shoppingGroupUser.shoppinggroup =:shoppinggroup and shoppingGroupUser.approved =:approved")
                                            .setParameter("shoppinggroup",shoppinggroup)
                                            .setParameter("approved",Boolean.TRUE)
                                            .getResultList();
        return users;
    }

    }
    
