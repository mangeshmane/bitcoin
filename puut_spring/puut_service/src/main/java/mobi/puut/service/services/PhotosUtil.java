package mobi.puut.service.services;

import java.util.Date;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mobi.puut.service.data.PhotosData;

@Service
public class PhotosUtil {

	@Autowired
	PhotosData photoData;
	
	
	private String characters="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private int length_from =5;
	private int length_till=25;
	private int attempts = 5;
	private Boolean photo;
	
	 @PostConstruct
		public void init() {
			SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		}

	public void setPhoto(Boolean photo) {
		this.photo=photo;
	}
	
	
	private String getRandomString(int length) {
		Random rnd=new Random(new Date().getTime());
		StringBuilder builder=new StringBuilder();
		for(int i=0;i<length;i++) {
			int index =rnd.nextInt(characters.length());
			builder.append(characters).substring(index,index+1);
		}

	return builder.toString();	
	}
	
	
	
	
}
