/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
package mobi.puut.service.Controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import javax.mail.MessagingException;
import javax.ws.rs.core.MediaType;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.RegistrationService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

*//**
 *
 * @author Development
 *//*
@RestController
@RequestMapping("/register")
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public mobi.puut.service.entities.users.Result register(@RequestBody User user)
            throws UnsupportedEncodingException, NoSuchAlgorithmException, MessagingException {
        try {
            return registrationService.register(user);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(RegistrationController.class).error(exception);
            throw new Error(exception);
        }

    }

    @RequestMapping(value = "/confirm/{puutnumber}/{userid}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
    public mobi.puut.service.entities.users.Result confirm(@PathVariable("puutnumber") String puutnumber, @PathVariable("userid") String userid) {
        try {
            return registrationService.confirm(puutnumber, userid);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();
            Logger.getLogger(RegistrationController.class).error(exception);
            throw new Error(exception);
        }

    }

}
*/