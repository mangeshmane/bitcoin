package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.photo.Photo;
import mobi.puut.service.entities.photo.PhotoAlbum;
import mobi.puut.service.entities.users.User;

public interface PhotosData {

	public Photo getPhoto(Long id,String login);
	public List<PhotoAlbum> getAlbums(String login); 
	public List<PhotoAlbum> getFriendAlbums(Long friend_id,String login);
	public List<Photo> getPhotos(Long album_id,String login);
	public Result sharePhoto(Long id, Long status,String login);
	public Result voteForPhoto(Long id, Long status,String login);
	public PhotoAlbum get(Long id,User user);
	public void add(Photo photo);
	public Result createAlbum(PhotoAlbum photoAlbum, String login);
	
}
