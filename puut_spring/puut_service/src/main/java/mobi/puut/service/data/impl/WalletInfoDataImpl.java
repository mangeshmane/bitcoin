package mobi.puut.service.data.impl;

import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.persistence.EntityTransaction;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import  javax.persistence.Query;

import mobi.puut.service.data.StatusData;
import mobi.puut.service.data.WalletInfoData;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Secure;
import mobi.puut.service.entities.users.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.security.core.context.SecurityContextHolder;

@Repository
public class WalletInfoDataImpl implements WalletInfoData {

    @Autowired
    private SessionFactory sessionFactory;
    
    //@Autowired
    private StatusData statusData = new StatusDataImpl();
    
     
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    @Override
    public List<Cryptoinfo> getAllWallets() {
        this.init();
         String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = (User)  sessionFactory.getCurrentSession().createQuery("from User user where user.login=:login").setParameter("login", login).uniqueResult();
        return sessionFactory.getCurrentSession()
                .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.user=:user",Cryptoinfo.class).setParameter("user", user).getResultList();
    }

    @Override
    @Transactional
    public Cryptoinfo getById(Long id) {
        this.init();
        
        return sessionFactory.getCurrentSession()
                .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.id=:id",Cryptoinfo.class)
                .setParameter("id", id)
                .uniqueResult();
    }

    @Override
    //@Transactional
    public Cryptoinfo create(String code, String currency, String address, User user,Boolean isDefaultAddress,String identifier) {
       
        this.init();
        Cryptoinfo walletInfo = new Cryptoinfo();
        walletInfo.setAddress(address);
        walletInfo.setCode(code);
        walletInfo.setCurrency(currency);     
        walletInfo.setUser(user);
        walletInfo.setMain(isDefaultAddress);
        walletInfo.setIdentifier(identifier);
        sessionFactory.getCurrentSession().persist(walletInfo);            
        return walletInfo;
    }

    /*@Override
    public void deleteWalletInfoByWalletId(Long walletId) {
        this.init();
        
        if (statusData.getStatusRetentionInfoByWalletId(walletId)) {
            return;
        }

        sessionFactory.getCurrentSession().createQuery("delete Cryptoinfo as cryptoinfo where cryptoinfo.id = :id")
                .setParameter("id", walletId).executeUpdate();
    }*/

    @Override
    public Cryptoinfo getWalletInfoByCurrencyAndAddress(String currency, String address) {
        this.init();
        
        List<Cryptoinfo> walletInfos = sessionFactory.getCurrentSession()
                .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.currency = :currency and cryptoinfo.address = :address")
                .setParameter("currency", currency)
                .setParameter("address", address).getResultList();

        return Objects.isNull(walletInfos) || walletInfos.isEmpty() ?
                null : walletInfos.get(0);
    }

    @Override
    public List<Cryptoinfo> getAddressByUser(User user) {
         this.init();
         List<Cryptoinfo> walletInfos = sessionFactory.getCurrentSession()
                                        .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.user = :user ")
                                        .setParameter("user", user).getResultList();
         return walletInfos;
    }
    
    @Override
    //@Transactional
    public Cryptoinfo updateWallet(Cryptoinfo cryptoInfo) {
        this.init();
        sessionFactory.getCurrentSession().update(cryptoInfo);
        return cryptoInfo;
    }
    
    @Override
    public Cryptoinfo getDefaultWallet(User user) {
         this.init();
         Cryptoinfo walletInfos = sessionFactory.getCurrentSession()
                                        .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.user = :user and main = :main",Cryptoinfo.class)
                                        .setParameter("user", user)
                                        .setParameter("main", true).uniqueResult();
         return walletInfos;
    }
    
    @Override
    public Boolean checkWalletAndUser(Long id,User user){
            this.init();
            Cryptoinfo walletInfos = sessionFactory.getCurrentSession()
                                        .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.id =:id and cryptoinfo.user =:user ",Cryptoinfo.class)
                                        .setParameter("id", id)
                                        .setParameter("user", user)
                                        .uniqueResult();
            if(walletInfos == null){
                return false;
            }
            else{
                return true;
            }
    
    }
    
    @Override
    public Long getWalletIdByddress(String address) {
         this.init();
         Cryptoinfo walletInfos = sessionFactory.getCurrentSession()
                                        .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.address=:address",Cryptoinfo.class)
                                        .setParameter("address", address).uniqueResult();
         return walletInfos.getId();
    }

    @Override    
    public List<Secure> getSecureByUser(User user) {
        this.init();
        return sessionFactory.getCurrentSession()
                                        .createQuery("from Secure as sec where sec.user=:user",Secure.class)
                                      .setParameter("user", user).getResultList();
    }
    @Override
    public void updateSecure(Secure secure) {
        this.init();
        sessionFactory.getCurrentSession().saveOrUpdate(secure);
    }

    @Override
    @Transactional
    public Cryptoinfo getByAddress(String address) {
        this.init();
        
        return sessionFactory.getCurrentSession()
                .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.address=:address",Cryptoinfo.class)
                .setParameter("address", address)
                .uniqueResult();
    }

	@Override
	public Cryptoinfo getWalletById(long id) {
		// TODO Auto-generated method stub
		 return sessionFactory.getCurrentSession()
	                .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.id=:id",Cryptoinfo.class)
	                .setParameter("id", id)
	                .uniqueResult();
	}

	@Override
	public boolean checkWalletAndUser(long id, User user) {
		// TODO Auto-generated method stub
	
		 this.init();
         Cryptoinfo walletInfos = sessionFactory.getCurrentSession()
                                     .createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.id =:id and cryptoinfo.user =:user ",Cryptoinfo.class)
                                     .setParameter("id", id)
                                     .setParameter("user", user)
                                     .uniqueResult();
         if(walletInfos == null){
             return false;
         }
         else{
             return true;
         }
	}

	@Override
	public void deleteWalletInfoByWalletId(Long id) {
		// TODO Auto-generated method stub
		
	}

 /*   @Override
    public void saveBTCRequest(Cryptorequest cryptorequest) {
        this.init();        
        sessionFactory.getCurrentSession().saveOrUpdate(cryptorequest);
    }

    @Override
    public List<Cryptorequest> getAllBTCRequests(User user) {
         this.init();
         return sessionFactory.getCurrentSession()
                .createQuery("from Cryptorequest as cryptorequest where cryptorequest.touser=:user",Cryptorequest.class).setParameter("user", user).getResultList();
    }

    @Override
    public Cryptorequest getBTCRequestByid(long id) {
        this.init();
        return sessionFactory.getCurrentSession()
                .createQuery("from Cryptorequest as cryptorequest where cryptorequest.id=:id",Cryptorequest.class)
                .setParameter("id", id)
                .uniqueResult();
    }

	@Override
	public Cryptoinfo getWalletById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean checkWalletAndUser(long id, User user) {
		// TODO Auto-generated method stub
		return false;
	}
*/
    
}