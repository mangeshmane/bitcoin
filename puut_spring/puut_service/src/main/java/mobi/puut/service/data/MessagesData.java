package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.chat.Chats;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.entities.users.User;

public interface MessagesData {

	public void add(Message message);

	public List<Message> getInboxMessages(User user);

	public List<Message> getSentMessages(User user);


	public List<Message> getUnreadMessages(User user);

	List<Chats> findByConversationId(Long convId);

}
