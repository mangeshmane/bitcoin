package mobi.puut.service.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.ws.rs.core.Response;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Dlicenses;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Login;
import mobi.puut.service.entities.users.Passports;
import mobi.puut.service.entities.users.Personal;

import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.Secure;
import mobi.puut.service.entities.users.Socialcs;
import mobi.puut.service.entities.users.Ubill;
import mobi.puut.service.entities.users.Uploadids;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.ProfileService;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.hibernate.Hibernate;

import mobi.puut.service.EncodingUtil;
import mobi.puut.service.MailUtil;
import mobi.puut.service.PuutUtil;
import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.FriendData;
import mobi.puut.service.data.GeographicData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.data.WalletInfoData;
import mobi.puut.service.data.impl.WalletInfoDataImpl;

import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.ResetPassword;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.multipart.MultipartFile;

@Service("profileService")
public class ProfileServiceImpl implements ProfileService {
      
    @Autowired
    UserData usersData;
    
    @Autowired
    ServicesUtil profileUtil;
    
    @Autowired
    EncodingUtil encodingUtil;
    
    @Autowired
    GeographicData geographicData;
    
    @Autowired
    MailUtil mailUtil;
    
    @Autowired
    PuutUtil puutUtil;
       
    @Autowired
    FriendData friendsData;

    @Autowired
    ServicesUtil servicesUtil;
    
    private WalletInfoData walletInfoData=new WalletInfoDataImpl();
    
    public ProfileServiceImpl() {
        System.out.println("profile");
    
    }
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    private final int pimageWidth = 50;
    private final int pimageHeight = 50;
    
    private final int cimageWidth = 350;
    private final int cimageHeight = 100;

    public User getProfile() throws IOException {
        this.init();
       
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
      //  Hibernate.initialize(user.getFriends());
      
       List<Friend> friends = friendsData.getFriendsByUserId(user);
       user.setFriends(friends);
       Personal personal = usersData.getPersonal(user);
       Coverimage coverimage = usersData.getCoverimage(user);
       byte[] pbyte_img = null;
       byte[] cbyte_img = null;
        
  //WE NEED TO CHECK WHETHER THIS IS NEEDED
        /*   if (user.getCity() == null) {
            Country country = new Country();
            country.setId(65L);
            country.setName("");
            IPLocations city = new IPLocations();
            city.setCity.Id(0L);
            city.setCity_name("");
            city.setCountry();
            user.setCity(city);
        }*/
       
        if (personal != null && personal.getImage() != null) {
            InputStream in = new ByteArrayInputStream(personal.getImage());
            BufferedImage img = ImageIO.read(in);

            int type = img.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : img.getType();
            img = mobi.puut.service.ImageUtil.rotate(img, pimageWidth, pimageHeight);
            if (img.getHeight() > pimageHeight || img.getWidth() > pimageWidth)
                img = ImageUtil.resizeImage(img, type, pimageWidth, pimageHeight);

            ByteArrayOutputStream bas = new ByteArrayOutputStream();
            ImageIO.write(img, "png", bas);
            bas.flush();
            pbyte_img = bas.toByteArray();
            bas.close();

            //user.setPimage(pbyte_img);
            
        }
        
            if (coverimage != null && coverimage.getImage() != null) {
            InputStream coverimagein = new ByteArrayInputStream(coverimage.getImage());
            BufferedImage coverimageimg = ImageIO.read(coverimagein);

            int coverimagetype = coverimageimg.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : coverimageimg.getType();
            coverimageimg =mobi.puut.service.ImageUtil.rotate(coverimageimg, cimageWidth, cimageHeight);
            if (coverimageimg.getHeight() > cimageHeight || coverimageimg.getWidth() > cimageWidth)
                coverimageimg = ImageUtil.resizeImage(coverimageimg, coverimagetype, cimageWidth, cimageHeight);

            ByteArrayOutputStream coverimageimgbas = new ByteArrayOutputStream();
            ImageIO.write(coverimageimg, "png", coverimageimgbas);
            coverimageimgbas.flush();
            cbyte_img = coverimageimgbas.toByteArray();
            coverimageimgbas.close();

            //user.setCimage(cbyte_img);
        }
        user.setPimage(pbyte_img);
        user.setCimage(cbyte_img);
        return user;
    }

    public Result editProfile(User request)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        Result result = new Result();
        result.setResult(profileUtil.getEditProfileErrors(request));
        
        if (result.getSuccess()) {
            User user = usersData.getByUsername(login);
            User user_phone = usersData.getUserByPhone(request.getPhoneno(), login);
            
          //  if (user_phone != null) {
                if (request.getUsername() != null) {
                    if (!request.getUsername().isEmpty()) {
                        user.setUsername(request.getUsername());
                    }
                }
                if (request.getPassword() != null) {
                    if (!request.getPassword().isEmpty()) {
                        if (request.getPassword().length() > 7) {
                            String hash_password = encodingUtil.hash(request.getPassword());
                            if (!hash_password.isEmpty()) {
                                user.setPassword(hash_password);
                            } else {
                                result.setSuccess(false);
                                result.setError("Password wasn't set. Try again");
                            }
                        } else {
                            result.setSuccess(false);
                            result.setError("PASSWORDLESSTHAN8");
                        }
                    }
                }
            /*    user.setPhone(request.getPhone());
                user.setAddress(request.getAddress());
                user.setBirthdate(request.getBirthdate());
                user.setPostcode(request.getPostcode());
                user.setCity(request.getCity());
                user.setOccupation(request.getOccupation());
                user.setEducation(request.getEducation());                           
                //user.setCity_name(request.getCity());
                user.setCity(request.getCity());
                user.setLanguage(request.getLanguage());
                user.setAccounttype(request.getAccounttype());
                user.setCountry(request.getCountry());
                user.setCivilstatus(request.getCivilstatus());
                user.setPin(request.getPin());
                user.setSocialnr(request.getSocialnr());*/
                user.setFirstname(request.getFirstname());
                user.setLastname(request.getLastname());
                user.setMiddlename(request.getMiddlename());
                user.setPhoneno(request.getPhoneno());
                
                if(!"".equals(request.getBirthdate()) && request.getBirthdate() != null)
                {
                    user.setBirthdate(request.getBirthdate());
                }
                user.setAddress(request.getAddress());
                user.setPostcode(request.getPostcode());
                user.setEducation(request.getEducation());
                user.setOccupation(request.getOccupation());
                user.setCivilstatus(request.getCivilstatus());
                user.setStatus(request.getStatus());
                user.setSocialnr(request.getSocialnr());
                user.setGender(request.getGender());
                user.setCity(request.getCity());
                user.setAccounttype(request.getAccounttype());
                user.setLanguage(request.getLanguage());
                user.setCountry(request.getCountry());
                user.setEmail(request.getEmail());
                usersData.edit(user);
            //} else {
            //    result.setSuccess(false);
            //    result.setError("This telephone number is already used");
           // }
        
        }
        return result;
    }

    public Login checkLogin() {
        init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        Login log = new Login();
        
        Role role = usersData.getRole(login);
        //Language language = geographicData.getLanguage(login);
        Language language = usersData.getByUsername(login).getLanguage();
        
        log.setRole(role);
        log.setLanguage(language);
        
        return log;
    }

    public Result remindPassword(String login)  {
        this.init();
        
       Result result = new Result();
        result.setSuccess(Boolean.TRUE);
        
        User user = usersData.getByUsername(login);
        
        if (user == null) {
          result.setSuccess(Boolean.FALSE);
            result.setError("USERNOTEXISTS");
            return result;
        } else {
           if(true){
            String gen_str = "1233432434232";
           /* !gen_str.isEmpty()*/
            if (true) {
                ResetPassword resetPassword = new ResetPassword();
                resetPassword.setEmail(login);
                resetPassword.setGenId(gen_str);
                resetPassword.setUser(user);
                usersData.add(resetPassword);
                
                StringBuilder content = new StringBuilder();
                content.append("Dear ");
                content.append(user.getFirstname());
                content.append("\n\n");
                content.append("To initiate the password reset process for your ");
                content.append(user.getUsername());
                content.append(" Puut Account, click the link below:\n\n");
                content.append("https://puutwallet.com/fget/");
                content.append(gen_str);
                content.append("\n\n");
                content.append("If clicking the link above doesn't work, please ");
                content.append("copy and paste the URL in a new browser window instead.\n\n");
                content.append("If you've received this mail in error, it's ");
                content.append("likely that another user entered\n");
                content.append("your email address by mistake while trying to ");
                content.append("reset a password. If you didn't initiate the request, you don't need to ");
                content.append("take any further action and can safely disregard this email.\n\n");
                content.append("Sincerely,\n");
                content.append("The Puut Accounts Team\n\n");
                content.append("Note: This email address cannot accept replies. ");
                content.append("To fix an issue or learn more about your account, visit our help center:\n\n");
                content.append("https://www.puutwallet.com/support/");
                
                try {
					mailUtil.sendMail("Reset Password", content.toString(), user.getUsername());
					result.setSuccess(Boolean.TRUE);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
             
                result.setSuccess(Boolean.TRUE);
                /*
                mailUtil.sendMail("Reset Password",
                    "If you want to reset your password follow this link " +
                    "http://www.puutwallet.com/puut_site/resetpassword/" + user.getId().toString() + "/" + gen_str,
                    user.getLogin());
                    */
            }
           }else{
        	   result.setSuccess(Boolean.FALSE);
           }
        }
		return result;
     
        }

    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public AccountSetting getProfileSettings() {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
        AccountSetting settings = usersData.getSettings(user);
        settings.setLanguage(user.getLanguage());
        Preference preference = usersData.getPreference(user);
        settings.setPreference(preference);
        settings.setUser(user);
        return settings;
    }

    public Result saveProfileSettings(AccountSetting settings) {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        Result result = new Result();
        result.setSuccess(Boolean.TRUE);
        
        User user = usersData.getByUsername(login);
        AccountSetting test = usersData.getSettings(user);
        
       /* if (test.getInterests().isEmpty()) {
            List<Interest> interests = usersData.getInterests();
            for(Interest interest : interests) {
                UserInterest userInterest = new UserInterest();
                userInterest.setAccountsetting(test);
                userInterest.setInterest(interest);
                userInterest.setChecked(Boolean.FALSE);
                test.getInterests().add(userInterest);
            }
        }*/

        if (test != null) {
            if (settings.getLanguage() != null) {
                //test.setLanguage(settings.getLanguage());
                user.setLanguage(settings.getLanguage());
            }
            if (settings.getFirstname() != null) {
                test.setFirstname(settings.getFirstname());
            }
            if (settings.getMiddlename() != null) {
                test.setMiddlename(settings.getMiddlename());
            }
            if (settings.getLastname() != null) {
                test.setLastname(settings.getLastname());
            }
            if (settings.getPhone() != null) {
                test.setPhone(settings.getPhone());
            }
            if (settings.getBirthdate() != null) {
                test.setBirthdate(settings.getBirthdate());
            }
            if (settings.getGender() != null) {
                test.setGender(settings.getGender());
            }
            if (settings.getCountry() != null) {
                test.setCountry(settings.getCountry());
            }
            /*
            if (settings.getPostcode() != null) {
                test.setPostcode(settings.getPostcode());
            }
            */
            if (settings.getPuutnumber() != null) {
                test.setPuutnumber(settings.getPuutnumber());
            }
            if (settings.getNotification() != null) {
                test.setNotification(settings.getNotification());
            }
           /* if (settings.getInterests() != null) {
                for (UserInterest interest : test.getInterests()) {
                    for (UserInterest setting_interest : settings.getInterests()) {
                        if (interest.getId() == setting_interest.getId()) {
                            if (setting_interest.getChecked()) {
                                interest.setChecked(Boolean.TRUE);
                            }
                            else {
                                interest.setChecked(Boolean.FALSE);
                            }
                        }
                    }
                }
            }*/
        }
        else {
            result.setSuccess(Boolean.FALSE);
            result.setError("CANTSAVESETTINGS");
        }

        usersData.edit(test);
        usersData.edit(user);
        
        return result;
    }
    
    //Preference
    
     public Preference getProfilePreference() {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        User user = usersData.getByUsername(login);
        Preference preference = usersData.getPreference(user);
        preference.setLanguage(user.getLanguage());
        
        return preference;
    }

     
    public Result saveProfilePreference(Preference preference) {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        
        Result result = new Result();
       // result.setSuccess(Boolean.TRUE);
        
        User user = usersData.getByUsername(login);
       // Preference as = usersData.getPreference(user);
        preference.setUser(user);
        if(preference.getLanguage() == null){
          result.setError("Language can not be null");
          result.setSuccess(Boolean.FALSE);
        }
        
        else{
           try{ 
             usersData.addPreference(preference);
             user.setLanguage(preference.getLanguage());
             usersData.edit(user);
             result.setSuccess(Boolean.TRUE);
           }catch(Exception e){
             result.setError("Can not save preference");
             result.setSuccess(Boolean.FALSE);
           }
        }
         return result;
//        if(preference.isIsSport()==true){
//            as.setIsSport(true);
//        }
//        if(preference.isIsSport()==false){
//            as.setIsSport(false);
//        }
//        if(preference.isIsTravel()==true){
//            as.setIsTravel(true);
//        } 
//        if(preference.isIsTravel()==false){
//            as.setIsTravel(false);
//        } 
//        if(preference.isIsBusiness()==true){
//            as.setIsBusiness(true);
//        }
//        if(preference.isIsBusiness()==false){
//            as.setIsBusiness(false);
//        }
//        if(preference.isIsCulture()==true){
//            as.setIsCulture(true);
//        } 
//        if(preference.isIsCulture()==false){
//            as.setIsCulture(false);
//        } 
//        if(preference.isIsNews()==true){
//            as.setIsNews(true);
//        }
//        if(preference.isIsNews()==false){
//            as.setIsNews(false);
//        }
//        if(preference.isIsEducation()==true){
//            as.setIsEducation(true);
//        } 
//        if(preference.isIsEducation()==false){
//            as.setIsEducation(false);
//        }
//        if(preference.isIsShopping()==true){
//            as.setIsShopping(true);
//        }
//        if(preference.isIsShopping()==false){
//            as.setIsShopping(false);
//        }
//        if(preference.isIsHome()==true){
//            as.setIsHome(true);
//        } 
//        if(preference.isIsHome()==false){
//            as.setIsHome(false);
//        }
//        if(preference.isIsHealth()==true){
//            as.setIsHealth(true);
//        } 
//        if(preference.isIsHealth()==false){
//            as.setIsHealth(false);
//        }
//        if(preference.isIsEmail()==true){
//            as.setIsEmail(true);
//        } 
//        if(preference.isIsEmail()==false){
//            as.setIsEmail(false);
//        }
//        if(preference.isIsPuut()==true){
//            as.setIsPuut(true);
//        } 
//        if(preference.isIsPuut()==false){
//            as.setIsPuut(false);
//        }
//        if(preference.isIsSMS()==true){
//            as.setIsSMS(true);
//        } 
//        if(preference.isIsSMS()==false){
//            as.setIsSMS(false);
//        }
//        if(preference.isLang()==false){
//            as.setLang(false);
// 
//     } 
//        
//    else {
//            result.setSuccess(Boolean.FALSE);
//            result.setError("CANTSAVEPREFERENCE");
//        }
//
//    // usersData.edit(as);
//        usersData.edit(user);
//        
       
    }
    
    //preference

    public List<Language> getLanguages() {
        this.init();
        
        return geographicData.getLanguages();
    }

    public Response uploadAccountPicture(MultipartFile uploadedInputStream) throws IOException {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Personal.class.getSimpleName()+randomNumber;
        Personal personal = usersData.getPersonal(user);
        
        if (personal == null) {
            personal = new Personal();
            personal.setUser(user);
            personal.setChoose(false);
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //personal.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
        personal.setImage(uploadedInputStream.getBytes());
        personal.setName(fileName);
        //personal.setName(uploadedInputStream.getContentDisposition().getFilename());
        
        usersData.editPersonal(personal);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }

    public Response uploadUploadIds(MultipartFile uploadedInputStream) throws IOException {
        this.init();
        
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Uploadids.class.getSimpleName()+randomNumber;
        Uploadids uploadids = usersData.getUploadids(user);
        
        if (uploadids == null) {
            uploadids = new Uploadids();
            uploadids.setUser(user);
            uploadids.setChoose(false);
            uploadids.setNumber("");
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //uploadids.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
        uploadids.setImage(uploadedInputStream.getBytes());
          uploadids.setName(fileName);
       // uploadids.setName(uploadedInputStream.getContentDisposition().getFilename());
        usersData.editUploadids(uploadids);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }
    
     public Response uploadSocialcs(MultipartFile uploadedInputStream) throws IOException {
        this.init();
         
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Socialcs.class.getSimpleName()+randomNumber;
        Socialcs socialcs = usersData.getSocialcs(user);
        
        if (socialcs == null) {
            socialcs = new Socialcs();
            socialcs.setUser(user);
            socialcs.setChoose(false);
            socialcs.setNumber("");
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //socialcs.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
         socialcs.setImage(uploadedInputStream.getBytes());
        socialcs.setName(fileName);
       // socialcs.setName(uploadedInputStream.getContentDisposition().getFilename());
        usersData.editSocialcs(socialcs);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }
     
      public Response uploadUbill(MultipartFile uploadedInputStream) throws IOException {
         this.init();
          
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Ubill.class.getSimpleName()+randomNumber;
        Ubill ubill = usersData.getUbill(user);
        
        if (ubill == null) {
            ubill = new Ubill();
            ubill.setUser(user);
            ubill.setChoose(false);
            ubill.setNumber("");
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //ubill.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
        ubill.setImage(uploadedInputStream.getBytes());
        ubill.setName(fileName);
        //ubill.setName(uploadedInputStream.getContentDisposition().getFilename());
        usersData.editUbill(ubill);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }
      
       public Response uploadCoverimage(MultipartFile uploadedInputStream) throws IOException {
        this.init();
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Coverimage.class.getSimpleName()+randomNumber;
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        
        Coverimage coverimage = usersData.getCoverimage(user);
        
        if (coverimage == null) {
            coverimage = new Coverimage();
            coverimage.setUser(user);
            coverimage.setChoose(false);
           
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //coverimage.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
         coverimage.setImage(uploadedInputStream.getBytes());
        coverimage.setName(fileName);
        // coverimage.setName(uploadedInputStream.getContentDisposition().getFilename());
        usersData.editCoverimage(coverimage);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }
  
        public Response uploadDlicenses(MultipartFile uploadedInputStream) throws IOException {
            this.init();
            
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Dlicenses.class.getSimpleName()+randomNumber;
        Dlicenses dlicenses = usersData.getDlicenses(user);
        
        if (dlicenses == null) {
            dlicenses = new Dlicenses();
            dlicenses.setUser(user);
            dlicenses.setChoose(false);
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //dlicenses.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
         dlicenses.setImage(uploadedInputStream.getBytes());
        dlicenses.setName(fileName);
        //dlicenses.setName(uploadedInputStream.getContentDisposition().getFilename());
        usersData.editDlicenses(dlicenses);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }
      
  public Response uploadPassports(MultipartFile uploadedInputStream) throws IOException {
        this.init();
           
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
         int randomNumber = ThreadLocalRandom.current().nextInt();
        String fileName = Passports.class.getSimpleName()+randomNumber;
        Passports passports = usersData.getPassports(user);
        
        if (passports == null) {
            passports = new Passports();
            passports.setUser(user);
            passports.setChoose(false);
            passports.setNumber("");
            //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        
        //passports.setImage(IOUtils.toByteArray(uploadedInputStream.getObject(InputStream.class)));
         passports.setImage(uploadedInputStream.getBytes());
        passports.setName(fileName);
        //passports.setName(uploadedInputStream.getContentDisposition().getFilename());
        
        usersData.editPassports(passports);
        
        String output = "File has been uploaded";
        return Response.status(200).entity(output).build();
    }

     
   
   public Result updatePin(User user){
        this.init();
        Result result = new Result();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User loginUser = usersData.getByUsername(login);
        if(loginUser.getPin() != null){
            if(!user.getConfirmpin1().equals(user.getConfirmpin2())){
                result.setSuccess(Boolean.FALSE);
                result.setError("Pin doesn't match");
                return result;
            }

            if(loginUser.getPin() != null){
                if(!loginUser.getPin().equals(getMD5(user.getPin()))){
                     result.setSuccess(Boolean.FALSE);
                     result.setError("Incorrect Existing Pin");
                     return result;
                }
            }

            if(validatePin(user.getConfirmpin1())){
                loginUser.setPin(getMD5(user.getConfirmpin1()));
                try{
                    usersData.edit(loginUser);   
                    result.setSuccess(Boolean.TRUE);
                    result.setSuccessMessage("Pin updated successfully");
                    return result;           
                }catch(Exception e){
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Failed to update pin");
                    return result;
                }   
            }else{
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Pin must be 4 digit");
                    return result;
            }  
        }else{
            
            if(validatePin(user.getConfirmpin1())){
                
                if(!user.getConfirmpin1().equals(user.getConfirmpin2())){
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Pin doesn't match");
                    return result;
                }else{
                    loginUser.setPin(getMD5(user.getConfirmpin1()));
                    try{
                        usersData.edit(loginUser);           
                        result.setSuccess(Boolean.TRUE);
                        result.setSuccessMessage("Pin updated successfully");
                        return result;             
                    }catch(Exception e){
                        result.setSuccess(Boolean.FALSE);
                        result.setError("Failed to update pin");
                        return result;
                    } 
              }
            }else{
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Pin must be 4 digit");
                    return result;
            }  
        } 
   }   
    
   
   public Result updatePassword(User user){
        this.init();
        Result result = new Result();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User loginUser = usersData.getByUsername(login);
        
        if(!user.getConfirmpassword1().equals(user.getConfirmpassword2())){
            result.setSuccess(Boolean.FALSE);
            result.setError("Password doesn't match");
            return result;
        }
        
        
        if(!loginUser.getPassword().equals(getMD5(user.getPassword()))){
            result.setSuccess(Boolean.FALSE);
            result.setError("Incorrect Existing Password");
            return result;           
        }        
        
        loginUser.setPassword(getMD5(user.getConfirmpassword1()));
         try{
             usersData.edit(loginUser); 
             result.setSuccess(Boolean.TRUE);
             result.setError("Password updated successfully");
             return result;        
         }catch(Exception e){
             result.setSuccess(Boolean.TRUE);
             result.setError("Failed to update Password");
             return result;
         }   
   }  
   
   
   public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
   
   public boolean validatePin(String pin) {

        if (pin.matches("\\d+") && pin.length() == 4) {
            return true;
        }
        return false;
    }
 
    
    @Transactional(rollbackFor=Exception.class)
    public Result verifyPin(String pin) {
        this.init();
        Result result = new Result();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = this.usersData.getByUsername(login); 
        try{
         Secure secure = null;
         List<Secure> secureList = this.walletInfoData.getSecureByUser(user); 
         
         if(!secureList.isEmpty()){
             secure = secureList.get(0);
         }
        
        if(secure == null){           
            secure = new Secure();
            secure.setUser(user);         
            secure.setAttempt(0L);
            secure.setAttempttime(null);
            secure.setDisabled(Boolean.FALSE);
            this.walletInfoData.updateSecure(secure);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
        Date attemptTime = null;
        Date currentDateTime = new Date();
        
        if(user.getPin() == null){
            result.setSuccess(Boolean.FALSE);
            result.setError("Pin is not set for the user");
        }else if(secure.getAttempt().equals(3L)){
            try {
                 attemptTime = dateFormat.parse(secure.getAttempttime());
                 String cuurentDate = servicesUtil.formatTransactionDateTime(new Date().toString());
                 currentDateTime = dateFormat.parse(cuurentDate);
            } catch (ParseException ex) {
                java.util.logging.Logger.getLogger(ProfileServiceImpl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
             
            long differenceInMiliseconds = Math.abs(currentDateTime.getTime() - attemptTime.getTime());
            long difference = TimeUnit.MINUTES.convert(differenceInMiliseconds, TimeUnit.MILLISECONDS);
            if(difference >= 30L){                 
                if(user.getPin().equals(getMD5(pin))){
                    secure.setAttempt(0L);
                    secure.setAttempttime(null);
                    secure.setDisabled(Boolean.FALSE);
                    this.walletInfoData.updateSecure(secure);
                    result.setSuccess(Boolean.TRUE);
                    //result.setError("Pin Verified");
                 }else{                    
                    secure.setAttempt(1L);
                    secure.setDisabled(Boolean.FALSE);
                    try {
                        secure.setAttempttime(servicesUtil.formatTransactionDateTime(currentDateTime.toString()));
                    } catch (ParseException ex) {
                        java.util.logging.Logger.getLogger(ProfileServiceImpl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                    }
                    this.walletInfoData.updateSecure(secure);
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Invalid Pin");
                 }
            }else{           
                 result.setSuccess(Boolean.FALSE);
                 result.setError("Your wallet is disabled for 30 Min,Please try later");
            }    
        }else{
            if(user.getPin().equals(getMD5(pin))){                
                    secure.setAttempt(0L);
                    secure.setDisabled(Boolean.FALSE);
                    secure.setAttempttime(null);
                    this.walletInfoData.updateSecure(secure);
                    result.setSuccess(Boolean.TRUE);
                    //result.setError("Pin Verified");
                 }else{   
                
                   String date = null;
                        try {
                             date = servicesUtil.formatTransactionDateTime(new Date().toString());
                        } catch (ParseException ex) {
                            java.util.logging.Logger.getLogger(ProfileServiceImpl.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                        }
                    
                    secure.setAttempt(secure.getAttempt() + 1L);
                    if(secure.getAttempt().equals(3L)){
                         secure.setDisabled(Boolean.TRUE);
                    }else{
                        secure.setDisabled(Boolean.FALSE);
                    }                    
                    secure.setAttempttime(date);
                    this.walletInfoData.updateSecure(secure);
                    result.setSuccess(Boolean.FALSE);
                    result.setError("Invalid Pin");
                 }
        }
        
        return result;
        }catch(Exception e){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exception = sw.toString();           
            throw new IllegalArgumentException(exception);
        }
    }

	@Override
	public Result remindPassword1(User user1) {
		// TODO Auto-generated method stub
		  this.init();
	       String login=user1.getEmailid(); 
	       Result result = new Result();
	        result.setSuccess(Boolean.TRUE);
	        
	        User user = usersData.getByUsername(login);
	        
	        if (user == null) {
	          result.setSuccess(Boolean.FALSE);
	            result.setError("USERNOTEXISTS");
	            return result;
	        } else {
	           if(true){
	            String gen_str = "1233432434232";
	           /* !gen_str.isEmpty()*/
	            if (true) {
	                ResetPassword resetPassword = new ResetPassword();
	                resetPassword.setEmail(login);
	                resetPassword.setGenId(gen_str);
	                resetPassword.setUser(user);
	                usersData.add(resetPassword);
	                
	                StringBuilder content = new StringBuilder();
	                content.append("Dear ");
	                content.append(user.getFirstname());
	                content.append("\n\n");
	                content.append("To initiate the password reset process for your ");
	                content.append(user.getUsername());
	                content.append(" Puut Account, click the link below:\n\n");
	                content.append("https://puutwallet.com/fget/");
	                content.append(gen_str);
	                content.append("\n\n");
	                content.append("If clicking the link above doesn't work, please ");
	                content.append("copy and paste the URL in a new browser window instead.\n\n");
	                content.append("If you've received this mail in error, it's ");
	                content.append("likely that another user entered\n");
	                content.append("your email address by mistake while trying to ");
	                content.append("reset a password. If you didn't initiate the request, you don't need to ");
	                content.append("take any further action and can safely disregard this email.\n\n");
	                content.append("Sincerely,\n");
	                content.append("The Puut Accounts Team\n\n");
	                content.append("Note: This email address cannot accept replies. ");
	                content.append("To fix an issue or learn more about your account, visit our help center:\n\n");
	                content.append("https://www.puutwallet.com/support/");
	                
	                try {
						mailUtil.sendMail("Reset Password", content.toString(), user.getUsername());
						result.setSuccess(Boolean.TRUE);
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	             
	                result.setSuccess(Boolean.TRUE);
	                /*
	                mailUtil.sendMail("Reset Password",
	                    "If you want to reset your password follow this link " +
	                    "http://www.puutwallet.com/puut_site/resetpassword/" + user.getId().toString() + "/" + gen_str,
	                    user.getLogin());
	                    */
	            }
	           }else{
	        	   result.setSuccess(Boolean.FALSE);
	           }
	        }
			return result;
	}
    
}