package mobi.puut.service.services.impl;

import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;

import mobi.puut.service.ServicesUtil;
import mobi.puut.service.data.MessagesData;
import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.Message;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.services.MessagesService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Service("messagesService")
public class MessagesServiceImpl implements MessagesService {

    @Autowired
    UserData usersData;

    @Autowired
    MessagesData messagesData;

    @Autowired
    ServicesUtil servicesUtil;

    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    public List<Message> getInbox(Long id) {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        List<Message> list = messagesData.getInboxMessages(user);

        return list;
    }

    public List<Message> getSent(Long id) {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        User user = usersData.getByUsername(login);
        List<Message> list = messagesData.getSentMessages(user);

        return list;
    }

    public Result sendMessage(Message message) {
        this.init();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        Result result = new Result();
        result.setResult(servicesUtil.getMessagesErrors(message));

        Logger.getLogger(MessagesServiceImpl.class).error("asdfasdf");

        if (result.getSuccess()) {
            Logger.getLogger(MessagesServiceImpl.class).error(message.getBody());
            User userfrom = usersData.getByUsername(login);
            User userto = usersData.getByUsername(message.getUserTo());

            if (userto == null) {
                result.setSuccess(false);
                result.setError("USERNOTEXISTS");
            } else {
                message.setUserfrom(userfrom);
                message.setUserto(userto);
                message.setMessagedate(new Date());
                messagesData.add(message);
            }
        }

        return result;
    }

    @Override
    public List getUnreadMessages() {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        List<Message> messeges = messagesData.getUnreadMessages(user);
        return messeges;
    }

    @Override
    public String getUnreadMessagesCount() {

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = usersData.getByUsername(login);
        List<Message> messeges = messagesData.getUnreadMessages(user);
        return String.valueOf(messeges.size());
    }

}
