package mobi.puut.service.entities.cloudmoney;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mobi.puut.service.entities.users.User;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
//@XmlRootElement
@Table(name="cloudmoney")
public class CloudMoney implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @ManyToOne(cascade = {CascadeType.ALL})   
    @JoinColumn(name="user")
    private User user;
    
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="currency")
    private Currency currency;
    
     @Column(name="amount")
    private String amount;
    
    @Column(name="message")
    private int message;
    
    @Column(name="sent")
    private int sent;
    
    @Column(name="docmessage")
    private int docmessage;
    
    @Column(name="docsent")
    private int docsent;
    
    @Column(name="reloadamount")
    private boolean reloadamount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getDocmessage() {
        return docmessage;
    }

    public void setDocmessage(int docmessage) {
        this.docmessage = docmessage;
    }

    public int getDocsent() {
        return docsent;
    }

    public void setDocsent(int docsent) {
        this.docsent = docsent;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public boolean isReloadamount() {
        return reloadamount;
    }

    public void setReloadamount(boolean reloadamount) {
        this.reloadamount = reloadamount;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }
     @XmlTransient
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}