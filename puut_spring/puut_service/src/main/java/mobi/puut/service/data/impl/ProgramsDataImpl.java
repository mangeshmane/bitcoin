/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobi.puut.service.data.impl;

import javax.annotation.PostConstruct;

import mobi.puut.service.data.ProgramsData;
import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.users.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 *
 * @author Jay
 */
@Repository
public class ProgramsDataImpl implements ProgramsData{
    
    @Autowired
    SessionFactory sessionFactory;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void save(Program program) {
        sessionFactory.getCurrentSession().saveOrUpdate(program);
    }
    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public Program findByName(String name) {
       return sessionFactory.getCurrentSession().createQuery("from Program p where p.name=:name",Program.class).setParameter("name", name).uniqueResult();
    }

    @Override
    public Program findByUser(User user) {
       return sessionFactory.getCurrentSession().createQuery("from Program p where p.user=:user",Program.class).setParameter("user", user).uniqueResult();
    }
    
}
