package mobi.puut.service.services;

import java.util.List;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.shoppingwall.ShoppingGroup;
import mobi.puut.service.entities.shoppingwall.ShoppingGroupUser;
import mobi.puut.service.entities.shoppingwall.ShoppingList;


public interface ShoppingWallService {


	  public List<ShoppingGroup> getShoppingGroups();
	  public Result createShoppingGroup(ShoppingGroup shoppinggroup);
	  public Result joinShoppingGroup(Long id);
	  public Result leaveShoppingGroup(Long id);
	  public List<ShoppingList> getShoppingLists();
	  public Result createShoppingList(ShoppingList shoppinglist);
	  public Result joinShoppingList(Long id);
	  public Result leaveShoppingList(Long id) ;
	  public List<ShoppingList> getOfferLists();
	  public List<ShoppingGroupUser> getShoppingGroupUser(Long id); 
	  public List<ShoppingGroup> getShoppingGroupbyName(String name);
	  public ShoppingGroupUser approveUserToGroup(Long userid, Long groupid);
	  public Result assignModerator(Long userid, Long groupid);
	  public Result editShoppingGroup(ShoppingGroup shoppinggroup) ;
	  public Result editShoppingList(ShoppingList shoppinglist);
	  
}
