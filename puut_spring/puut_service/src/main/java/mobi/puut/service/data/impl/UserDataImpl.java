package mobi.puut.service.data.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.ApiKeys;
import mobi.puut.service.entities.users.Coverimage;
import mobi.puut.service.entities.users.Cryptoinfo;
import mobi.puut.service.entities.users.Dlicenses;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Passports;
import mobi.puut.service.entities.users.Personal;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.ResetPassword;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.entities.users.Socialcs;
import mobi.puut.service.entities.users.Ubill;
import mobi.puut.service.entities.users.Uploadids;
import mobi.puut.service.entities.users.User;
import mobi.puut.service.entities.users.UserRole;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.query.spi.ReturnMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


@EnableTransactionManagement
@Repository("userData")
public class UserDataImpl implements UserData {

 
    @Autowired
    SessionFactory sessionFactory;

    
    @PostConstruct
	public void init() {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}
    
    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<User> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        List<User> list = session.createQuery("from User").list();

        return list;
    }

    @Transactional
    @Override
    public User getById(Long id) {

        return (User) sessionFactory.getCurrentSession()
                .createQuery("from User where id = :id")
                .setParameter("id", id)
                .uniqueResult();

    }
    @Transactional(rollbackFor=Exception.class)
    public void editSocialcs(Socialcs socialcs) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .saveOrUpdate(socialcs);
    }
   

    
    @Transactional(rollbackFor=Exception.class)
    public Language getLanguageById(Long id) {
        this.init();Session session = sessionFactory.getCurrentSession(); 
        Language language = (Language) session.createQuery("from Language language where language.id=:id").setParameter("id", id).uniqueResult();
        return language;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public User getByUsername(String username) {
         return sessionFactory.getCurrentSession()
                .createQuery("from User user where user.username =:username",User.class)
                .setParameter("username",username)
                .uniqueResult();
        
        
    }
    @Transactional(rollbackFor=Exception.class)
    public Passports getPassports(User user) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        try {
            Passports passports = (Passports) session.createQuery("from Passports as passport where user=:user",Passports.class)
                .setParameter("user", user)
                .uniqueResult();
            
            return passports;
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
 
    
    @Transactional(rollbackFor=Exception.class)
    public void editPassports(Passports passports) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .saveOrUpdate(passports);
    }

    
 @Transactional(rollbackFor=Exception.class)
 public Preference getPreference(Preference preference) {
     this.init();
     
     Session session = sessionFactory.getCurrentSession();
     
     
     Preference preference2 = (Preference) session.createQuery("from Preference preference where preference=:preference")
             .setParameter("preference", preference)
             .uniqueResult();
     
     return preference2;
 }
 
 @Transactional(rollbackFor=Exception.class)
 public Preference getPreference(User user) {
     this.init();
     
     Preference preference = (Preference) sessionFactory.getCurrentSession()
             .createQuery("from Preference where user = :user")
             .setParameter("user", user)
             .uniqueResult();
        return preference;
 }   
 @Transactional(rollbackFor=Exception.class)  
 public void addPreference(Preference preference) {
     this.init();
     
     sessionFactory.getCurrentSession()
             .saveOrUpdate(preference);
 }
   


 @Transactional(rollbackFor=Exception.class)
 public void edit(AccountSetting account_setting) {
     this.init();
     
     sessionFactory.getCurrentSession()
             .saveOrUpdate(account_setting);
 }

 
 
 @Transactional(rollbackFor=Exception.class)
 public void editUploadids(Uploadids uploadids) {
     this.init();
     
     sessionFactory.getCurrentSession()
             .saveOrUpdate(uploadids);
 }
 
    @Override
    public void updateById(User user) {
        
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public void delete(User user) {
       
        sessionFactory.getCurrentSession().delete(user);
    }

    @Transactional
    @Override
    public UserRole getRole(User user) {
        
            return  sessionFactory.getCurrentSession().createQuery("from UserRole where user=:user",UserRole.class).setParameter("user", user).uniqueResult();
    }

	@Override
	public User getUserByPhone(String phone) {
		// TODO Auto-generated method stub
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User as user where user.phoneno=:phone")
				.setParameter("phoneno", phone)
				.uniqueResult();
	}

	@Override
	public Cryptoinfo getDefualtWallet(User phoneUser) {
		// TODO Auto-generated method stub
		return (Cryptoinfo) sessionFactory.getCurrentSession()
				.createQuery("from Cryptoinfo as cryptoinfo where cryptoinfo.user=:phoneUser")
				.setParameter("user",phoneUser)
				.uniqueResult();
	}

	@Override
	public void updateImage(Personal personal) {
		// TODO Auto-generated method stub
		 sessionFactory.getCurrentSession().save(personal);
	}

	@Transactional
	@Override
	public void updateCoverImage(Coverimage coverimage) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(coverimage);
	}

	
	@Transactional
	@Override
	public User get(Long id) {
		// TODO Auto-generated method stub
		return (User) sessionFactory.getCurrentSession().createQuery("from User as user  where user.id=:id").setParameter("id", id).uniqueResult();
	}

	@Override
	public Personal getPersonal(User user) {
		// TODO Auto-generated method stub
		return (Personal) sessionFactory.getCurrentSession().createQuery("from Personal where user=:user").setParameter("user", user).setMaxResults(1).uniqueResult();
	}

	@Override
	@Transactional
	public ApiKeys getApiKeyByUser(String apikey) {
		// TODO Auto-generated method stub
		this.init();
		return (ApiKeys) sessionFactory.getCurrentSession().createQuery("from ApiKeys api where api.apikey=:apikey").setParameter("apikey", apikey).uniqueResult();
	}

	@Override
	public void add(AccountSetting settings) {
		// TODO Auto-generated method stub
		this.init();
		
		Session session=(Session) sessionFactory.getCurrentSession().save(settings);
	}

	@Override
	public AccountSetting getSettings(User user) {
		// TODO Auto-generated method stub
		return (AccountSetting) sessionFactory.getCurrentSession().createQuery("from AccountSetting as accountSetting where accountSetting.user=:user")
				.setParameter("user", user).uniqueResult();
				
	}

	/*@Override
	public Role getRole(String username) {
		// TODO Auto-generated method stub
		return (Role) sessionFactory.getCurrentSession().createQuery("select role from UserRole as r where r.user.username=:username")
				.setParameter("username", username).uniqueResult();
	}*/

	@Transactional(rollbackFor=Exception.class)
    public Role getRole(String login) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        Role role = ((UserRole)session.createQuery("from UserRole userole where user.username=:login and (role.name=:role1 or role.name=:role2)")
                .setParameter("login", login)
                .setParameter("role1", "puutpremium")
                .setParameter("role2", "puutclient")
                .uniqueResult()).getRole();
        
        return role;
    }
	
	

	@Override
	public User getUser(Long id) {
		// TODO Auto-generated method stub
		return (User) sessionFactory.getCurrentSession().createQuery("from User as u where u.friends.id=:id")
				.setParameter("id", id).uniqueResult();
	}


	 @Override
	    @Transactional(rollbackFor=Exception.class)
	    public List<User> getByTerm(String term) {
	        return sessionFactory.getCurrentSession().createQuery("select u from User u where ((CONCAT(u.firstname, ' ' ,u.lastname) LIKE :term) OR (u.lastname LIKE :term))", User.class)
	        .setParameter("term", term)
	        .getResultList();
	    }

	    @Override
	    @Transactional(rollbackFor=Exception.class)
	    public List<User> getByTermBis(String term) {
	        return sessionFactory.getCurrentSession().createQuery("select u from User u where ((u.firstname LIKE :term) OR (u.lastname LIKE :term))", User.class)
	                .setParameter("term", term)
	                .getResultList();
	    }

		@Override
		@Transactional(rollbackFor=Exception.class)
		public void edit(User user) {
			sessionFactory.getCurrentSession().update(user);
			
		}
		
	    @Transactional(rollbackFor=Exception.class)
	    public User getUserByPuutNumber(String puut_number) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        try {
	            User user = (User) session.createQuery("from User user where puutnumber=:puutnumber")
	                    .setParameter("puutnumber",puut_number)
	                    .uniqueResult();

	            return user;
	        } catch(Exception ex) {
	            return null;
	        }
	        
	        
	    }

	    @Transactional(rollbackFor=Exception.class)
	    public void editCoverimage(Coverimage coverimage) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .saveOrUpdate(coverimage);
	    }
	    
	    
	    @Transactional(rollbackFor=Exception.class)
	    public Dlicenses getDlicenses(User user) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        Dlicenses dlicenses = (Dlicenses) session.createQuery("from Dlicenses dlicenses where user=:user")
	                .setParameter("user", user)
	                .uniqueResult();
	        
	        return dlicenses;
	    }
	    
	    
	    @Transactional(rollbackFor=Exception.class)
	    public Ubill getUbill(User user) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        Ubill ubill = (Ubill) session.createQuery("from Ubill ubill where user=:user")
	                .setParameter("user", user)
	                .uniqueResult();
	        
	        return ubill;
	    }
	    
	    @Transactional(rollbackFor=Exception.class)
	    public Socialcs getSocialcs(User user) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        Socialcs socialcs = (Socialcs) session.createQuery("from Socialcs socialcs where user=:user")
	                .setParameter("user", user)
	                .uniqueResult();
	        
	        return socialcs;
	    }
	    
	    
	    @Transactional(rollbackFor=Exception.class)
	    public Uploadids getUploadids(User user) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        Uploadids uploadid = (Uploadids) session.createQuery("from Uploadids as uploadids where user=:user")
	                .setParameter("user", user)
	                .uniqueResult();
	        
	        return uploadid;
	    }        

	    @Transactional(rollbackFor=Exception.class)
	    public void editPersonal(Personal personal) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .saveOrUpdate(personal);
	    }

		@Override
		public void add(ResetPassword resetPassword) {
			// TODO Auto-generated method stub
			
		}

	    @Transactional(rollbackFor=Exception.class)
	    public void editDlicenses(Dlicenses dlicenses) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .saveOrUpdate(dlicenses);
	    }

	    @Transactional(rollbackFor=Exception.class)
	    public Coverimage getCoverimage(User user) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        
	        Coverimage coverimage = (Coverimage) session.createQuery("from Coverimage coverimage where user=:user")
	                .setParameter("user", user)
	                .uniqueResult();
	        
	        return coverimage;
	    } 
	    
	    
	    @Transactional(rollbackFor=Exception.class)
	    public void editUbill(Ubill ubill) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .saveOrUpdate(ubill);
	    }
	    
	    
	    @Transactional(rollbackFor=Exception.class)
	    public User getUserByPhone(String phone, String login) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        User user = (User) session.createQuery("from User user where user.login=:login and user.phone=:phone")
	                .setParameter("login", login)
	                .setParameter("phone", phone)
	                .uniqueResult();
	        
	        return user;
	    }
	    
	    @Transactional(rollbackFor=Exception.class)
	    public void add(UserRole userrole) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .save(userrole);
	    }



	    @Transactional(rollbackFor=Exception.class)
		@Override
		public List<User> getUsersByRole(Long role_id) {
	        this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	       
	        List<UserRole> roles = (List<UserRole>) session.createQuery("from UserRole userrole where userrole.role.id=:role_id").setParameter("role_id", role_id).getResultList();
	       // List<UserRole> roles = (List<UserRole>) session.createQuery("from UserRole userrole where userrole.roleid=:role_id").setParameter("role_id", role_id).getResultList();
	        
	        return roles.stream().map(r->r.getUser()).collect(Collectors.toList());
	    }
	    

	    @Transactional(rollbackFor=Exception.class)
	    public void add(User user) {
	        this.init();
	        
	        sessionFactory.getCurrentSession()
	                .save(user);
	    }
}
