package mobi.puut.service.crypto.util;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.bitcoinj.core.NetworkParameters;	
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.Wallet;
import org.slf4j.Logger;
import org.bitcoinj.params.MainNetParams;
import java.util.concurrent.TimeUnit;
import mobi.puut.service.crypto.util.WalletSetupCompletedListener;;


public class WalletManager {

	private final Logger logger=org.slf4j.LoggerFactory.getLogger(WalletManager.class);
	public WalletAppKit bitcoin;
	private WalletModel model=new WalletModel();
	public NetworkParameters networkParameters=TestNet3Params.get();
	
	public final String APP_NAME="WalletTemplate";
	public final String WALLET_FILE_NAME=APP_NAME.replaceAll("[^a-zA-Z0-9.-]","_")
			+networkParameters.getPaymentProtocolId();
	
	String walletIdentifier=null;
	String walletCode=null;
	private List<WalletSetupCompletedListener> setupCompletedListeners=Collections.synchronizedList(new LinkedList<>());
	
	public WalletManager setupWallet() {
		logger.info("Setup Wallet");
		
		this.setupWalletKit();
		
		
		try {
			if(this.bitcoin.isChainFileLocked()) {
				return this;
			}
			
		}catch(IOException e){
			e.printStackTrace();
			return this;
			
		}catch (Exception e) {
			return this;
		}
		
		this.bitcoin.startAsync();
		return this;
	}
	
	public WalletManager(String identifier,String code) {
		this.walletIdentifier=APP_NAME+identifier;
		this.walletCode=code;
	}

	
	public WalletManager() {
	}
	

	protected File getWalletDirectory() {
		File dir=new File("/home/btc/"+this.walletCode);
		
		if(!dir.exists())
		{
			dir.mkdir();
		}
		
		return dir;
	}
	
	
	public WalletAppKit getBitcoin() {
	
		return bitcoin;
	}
	
	public WalletModel getModel(){
		return model;
	}
	
	public void addWalletSetupCompletedListener(final WalletSetupCompletedListener listener) {
		setupCompletedListeners.add(listener);
	}
	
	private void setupWalletKit() {
		File directory=getWalletDirectory();
		bitcoin= new WalletAppKit(networkParameters, directory, walletIdentifier) {
			
			@Override
			protected void onSetupCompleted() {
				bitcoin.wallet().allowSpendingUnconfirmedTransactions();
				Wallet wallet=bitcoin.wallet();
				model.setWallet(wallet);
				setupCompletedListeners.forEach(listener ->listener.onSetupCompleted(wallet));
			}
		};
		
		if(networkParameters == RegTestParams.get()) {
			bitcoin.connectToLocalHost();
		}
		
		bitcoin.setDownloadListener(model.getSyncProgressUpdater())
		.setBlockingStartup(false)
		.setUserAgent(APP_NAME,"1.0");
		
	}
	
	
}
