package mobi.puut.service.services.chat;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import mobi.puut.service.entities.chat.Conversation;
import mobi.puut.service.entities.users.User;

public interface chatService {

	Boolean delete(String convId);

    List<User> chatCreation();

    public Map submitUsers(String name, String[] friendIds);

    List<Conversation> display();

    public List<Conversation> getConvList(String term);

    public Conversation updateConverSation(Conversation conversation);

    public List getUserQuery(String term, String conv);

    public Boolean isExistingConversation(String conv);

    public Boolean changeTimeJoined(String conv, String currentUser);

    public List<List> loadMessages(String conv, Long userId);

    public Boolean updateLastReadMessage(Date d, String conv, String username);

    public Integer countParticipants(String conv);

    public String getParticipantsFullnames(String conv, String currentUser);

    public String getFriendFullname(String conv, String currentUser);

    public Boolean createConversationWithUsers(String conv, Date timeJoined);

    public Date getLastMessageDate(String conv, String currentUser);

    public Boolean checkVisibility(String conv, String currentUser);

    public String getLastMessageUsername(String conv, String currentUser);

    public Boolean addMessageWithUsers(String username, String conv, String message, Date d);

    public Boolean loadBlockingRelation(String friendname, String currentUser);

    public Boolean checkActive(String conv, String currentUser);

    public Date getTimeJoined(String conv, String currentUser);

    public Boolean blockUser(String friendname, String currentUser);

    public Boolean unblockUser(String friendname, String currentUser);

    public Boolean verifyParticipantInConv(String conversation, String term);

    public Boolean addParticipantInConv(String conversation, String term, String currentUser);

    public String openExistingConv(String conv, String newParticipant);

    public String getFullnameByLogin(String login);

    public Boolean leaveConversation(String conversation, String currentUser);

    public Boolean modifyConvName(String previousName, String newName, String owner);

    public Boolean storeNewConvPicture(String image, String conv);

    public Boolean disableConversation(String conv, String currentUser);

    // ADDED //
    public List<String> getConversationsNames(String currentUser);

    public List<String> getButtonsIdentifiers(String currentUser);

    public List<String> getConversationsIdentifiers(String currentUser);

    public List<String> getRecentConv(String currentUser);

    public List<String> getRecentMessages(String currentUser);

    public List<List> getUserList(String term, String currentUser);

    public List<Integer> getUnreadMessages(String currentUser);

    public List<List> findByTerm(String term, String currentUser);

    public List<String> getConversationsPictures(String currentUser);

    public Boolean verifyNameAndOwner(String convName, String username);

	List getMembersInConversation(org.json.JSONObject object);
	 	 
}
