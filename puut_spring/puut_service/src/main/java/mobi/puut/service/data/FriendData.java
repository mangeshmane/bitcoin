package mobi.puut.service.data;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.Preference;
import mobi.puut.service.entities.users.User;

public interface FriendData {

	public List<Friend>  getFriends(User user);
	public List<Friend>  getInvitations(User user);
	public Result  acceptInvitations(Long id);
	public User  getFriendProfile(Long id);
	public Friend get(Long id, User user);
	public void edit(Friend friend);
	Friend get(Long id, String login, int accepted);
	public List<User> searchFriend(User user, String login);
	public void add(Friend friend);
	public Result deleteFriend(List<User> users, String login);
	public Preference getPreference(User user_profile);
	public List<Preference> getAccept(Preference preference);
	public List<Preference> getFind(Preference preference);
	public Boolean isFriend(User user, User user2);
	 public List<Friend> getFriendsByUserId(User user);
	
}
