package mobi.puut.service.data.impl;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mobi.puut.service.data.GeographicData;
import mobi.puut.service.entities.geographic.IPCheck;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.Country;
import mobi.puut.service.entities.users.IPlocations;
import mobi.puut.service.entities.users.Language;
import mobi.puut.service.entities.users.Preference;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
public class GeographicDataImpl implements GeographicData {

    @Autowired
    SessionFactory sessionFactory;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    
    @Transactional(rollbackFor=Exception.class)
    public List<Country> getCountries() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Country> countries = session.createQuery("from Country as country order by country.name",Country.class).getResultList();

        return countries;
    }
/*
    @Transactional(rollbackFor=Exception.class)
    public List<City> getCities() {
        return sessionFactory.getCurrentSession()
                .createCriteria(City.class)
                .addOrder(Order.asc("name"))
                .list();
    }*/
    
    @Transactional (rollbackFor=Exception.class)
    public List<IPlocations> getCities() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<IPlocations> iplocations = session.createQuery("from IPlocations as ip order by ip.city_name").getResultList();

        return iplocations;
    }

 /*   @Transactional(rollbackFor=Exception.class)
    public List<City> getCitiesByCountry(Long country_id) {
        return sessionFactory.getCurrentSession()
                .createCriteria(City.class)
                .createAlias("country", "c")
                .add(Restrictions.eq("c.id", country_id))
                .list();
    }*/
    
    @Transactional (rollbackFor=Exception.class)
    public List<IPlocations> getCitiesbyid(String country_iso_code) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        //criteria.where( builder.equal(root.get("country.iso"), country_iso_code));
        
        List<IPlocations> iplocations = session.createQuery("from IPLocations as iplocations where iplocations.country.iso=:iso_code")
                .setParameter("iso_code", country_iso_code)
                .getResultList();

        return iplocations;
 
  }     
    /*
    @Transactional(rollbackFor=Exception.class)
    public Language getLanguage(String login) {
        return (Language)sessionFactory.getCurrentSession()
                .createCriteria(AccountSetting.class)
                .createAlias("user", "u")
                .add(Restrictions.eq("u.login", login))
                .setProjection(Projections.property("language"))
                .uniqueResult();
    }
    */

    @Transactional(rollbackFor=Exception.class)
    public List<Language> getLanguages() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List<Language> languages = session.createQuery("from Language as language order by language.name").getResultList();
        
        return languages;
    }
    
    @Transactional (rollbackFor=Exception.class)
    public IPCheck getIP(String ipnumber) {
        this.init();
        
        Integer ipAdd = Integer.parseInt(ipnumber);
        IPCheck ip = (IPCheck)sessionFactory.getCurrentSession()
                .createQuery("from IPCheck where ip_from <= :ipnumber and ip_to >= :ipnumber")
                .setParameter("ipnumber", ipAdd)
                .uniqueResult();       
            return ip;
    }

  
    @Transactional (rollbackFor=Exception.class)
    public List<IPlocations> getCitiesByCountry(String country_iso_code) {
    	
         List<IPlocations> iplocations = sessionFactory.getCurrentSession().createQuery("from IPlocations as ip where ip.country_iso_code=:iso_code",IPlocations.class)
                .setParameter("iso_code", country_iso_code)
                .setMaxResults(2000)
                .getResultList();
         return iplocations;         
    }
    
   
    @Transactional (rollbackFor=Exception.class)
    public Country getCountryByIso(char country_iso_code) {
         Country country = sessionFactory.getCurrentSession().createQuery("from Country as c where c.iso=:iso_code",Country.class)
                .setParameter("iso_code", country_iso_code)
                .uniqueResult();
         return country;         
    }


    @Transactional
	@Override
	public List<IPlocations> getCitiesByCountry(char country_iso_code) {
		// TODO Auto-generated method stub
		List<IPlocations> city = sessionFactory.getCurrentSession().createQuery("from IPlocations as i where i.country_iso_code	=:iso_code",IPlocations.class)
	                .setParameter("iso_code", country_iso_code)
	                .getResultList();
	         return city;   
	}


	@Override
	public Country getCountryByIso(String country_iso_code) {
		// TODO Auto-generated method stub
		return null;
	}

}