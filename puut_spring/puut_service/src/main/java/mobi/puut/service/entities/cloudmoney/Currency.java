package mobi.puut.service.entities.cloudmoney;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mobi.puut.service.entities.users.Country;

@Entity
@Table(name="currencies")
public class Currency implements Serializable {


	private static final long serialVersionUID=1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	
	@Column(name="name")
	private String name;
	
	@Column(name="symbol")
	private String Symbol;
	
	
	@ManyToOne(cascade= {CascadeType.REFRESH})
	@JoinColumn(name="country")
	private Country country;
	
	
	@Column(name="currencylogo")
	private byte[] currencylogo;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSymbol() {
		return Symbol;
	}


	public void setSymbol(String symbol) {
		Symbol = symbol;
	}


	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}


	public byte[] getCurrencylogo() {
		return currencylogo;
	}


	public void setCurrencylogo(byte[] currencylogo) {
		this.currencylogo = currencylogo;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
}
