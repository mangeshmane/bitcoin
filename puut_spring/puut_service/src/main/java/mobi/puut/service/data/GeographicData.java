package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.geographic.IPCheck;
import mobi.puut.service.entities.users.Country;
import mobi.puut.service.entities.users.IPlocations;
import mobi.puut.service.entities.users.Language;


public interface GeographicData {
    
    public List<Country> getCountries();
   // public List<City> getCities();
     public List<IPlocations> getCities();
   // public List<City> getCitiesByCountry(Long country_id);
    public List<IPlocations> getCitiesByCountry(char country_iso_code);
    public List<Language> getLanguages();
    public IPCheck getIP(String ipnumber);
    public Country getCountryByIso(String country_iso_code);
    
    //public Language getLanguage(String login);
    
}