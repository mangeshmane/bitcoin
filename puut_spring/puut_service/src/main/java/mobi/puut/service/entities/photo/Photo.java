package mobi.puut.service.entities.photo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import mobi.puut.service.entities.users.User;

@Entity
@Table(name="photos")
public class Photo implements Serializable{

private static final long serialVersionUID=1L;



@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private Long id;


@Column(name="description")
private String description;


@ManyToOne(cascade= {CascadeType.ALL})
@JoinColumn(name="photoalbum")
private PhotoAlbum photoalbum;

@ManyToOne(cascade= {CascadeType.ALL})
@JoinColumn(name="user")
private User user;


@Column(name="shared",nullable=false,columnDefinition="TINYINT(1)")
private Boolean shared;



@Column(name="image")
private byte[] picture;


@Transient
private Integer likes;


@Transient
private Integer notlikes;


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public String getDescription() {
	return description;
}


public void setDescription(String description) {
	this.description = description;
}


public PhotoAlbum getPhotoalbum() {
	return photoalbum;
}


public void setPhotoalbum(PhotoAlbum photoalbum) {
	this.photoalbum = photoalbum;
}


public Boolean getShared() {
	return shared;
}


public void setShared(Boolean shared) {
	this.shared = shared;
}


public byte[] getPicture() {
	return picture;
}


public void setPicture(byte[] picture) {
	this.picture = picture;
}


public Integer getLikes() {
	return likes;
}


public void setLikes(Integer likes) {
	this.likes = likes;
}


public Integer getNotlikes() {
	return notlikes;
}


public void setNotlikes(Integer notlikes) {
	this.notlikes = notlikes;
}


}
