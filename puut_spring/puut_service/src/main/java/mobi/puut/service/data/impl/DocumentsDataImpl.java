package mobi.puut.service.data.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mobi.puut.service.data.DocumentData;
import mobi.puut.service.entities.cloudmoney.CloudMoney;
import mobi.puut.service.entities.documents.Availability;
import mobi.puut.service.entities.documents.Btempimg;
import mobi.puut.service.entities.documents.Category;
import mobi.puut.service.entities.documents.Program;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.documents.Rtempimg;
import mobi.puut.service.entities.documents.Status;
import mobi.puut.service.entities.documents.Subtype;
import mobi.puut.service.entities.documents.Tempimg;
import mobi.puut.service.entities.documents.Type;
import mobi.puut.service.entities.users.Document;
import mobi.puut.service.entities.users.Friend;
import mobi.puut.service.entities.users.TypeProperty;
import mobi.puut.service.entities.users.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
public class DocumentsDataImpl implements DocumentData {
    
    @Autowired
    SessionFactory sessionFactory;
    
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
   
    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocuments(String login, Long type_id, Long status_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List<Document> list = (List<Document>) session.createQuery("from Document as doc where doc.user.username=:login and doc.type.id=:type_id and doc.status.id=:status_id",Document.class)
                .setParameter("login", login)
                .setParameter("type_id",type_id)
                .setParameter("status_id",status_id)
                .getResultList();


        return list;
    }

    
    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocuments(String login, Long type_id, Date cdate) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Document> list = (List<Document>) session.createQuery("from Document as doc where doc.user.username=:login and doc.type.id=:type_id and doc.validfrom <= :cdate and doc.validtill >= :cdate", Document.class)
                .setParameter("cdate",cdate)
                .setParameter("login",login)
                .setParameter("type_id",type_id)
                .getResultList();
        
        return list;
    }

    @Transactional
   
    public Document get(Long document_id, String login, Long type_id, Long status_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        Document document  = (Document)         session.createQuery("from Document as doc where doc.id=:id and doc.user.username = :login and doc.type.id=:type_id and doc.status.id=:status_id",Document.class)
                .setParameter("id",document_id)
                .setParameter("doc.user.login",login)
                .setParameter("doc.type.id",type_id)
                .setParameter("doc.status.id", status_id)
                .getSingleResult();
;
        
        return document;
    }
   
    @Transactional
   
    public void edit(Document document) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .saveOrUpdate(document);
    }

    @Transactional
    public Document get(Long document_id, String login, Long type_id, Date cdate) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        Document document  = (Document) session.createQuery("from Document as doc where doc.user.username=:login and doc.type.id=:type_id and doc.validfrom <= :cdate and doc.validtill >= :cdate", Document.class)
                .setParameter("cdate",cdate)
                .setParameter("login",login)
                .setParameter("type_id",type_id)
                .getSingleResult();
        
        return document;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Type> getTypes() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List<Type> list = (List<Type>) session.createQuery("from Type as type order by type.name", Type.class).getResultList();
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Subtype> getSubtypes(Long type_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Subtype> list = (List<Subtype>) session.createQuery("from Subtype as subtype where subtype.type.id=:type_id", Subtype.class)
                .setParameter("type_id", type_id).getResultList();

        return list;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Category> getCategories(Long type_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Category> list = (List<Category>) session.createQuery("from Category as category where category.type.id=:type_id",Category.class)
                .setParameter("type_id", type_id).getResultList();

        return list;
    }
    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public List<Status> getStatuses() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Status> list = (List<Status>) session.createQuery("from Status").getResultList();
        
        return list;
    }

	
    @Transactional(rollbackFor=Exception.class)
    public List<Availability> getAbailabilities() {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        List<Availability> list = (List<Availability>) session.createQuery("from Availability").getResultList();
        
        return list;
    }
    

    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocuments(String login, Long type_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        User user = (User) session.createQuery("from User as user where user.username=:login", User.class).setParameter("login", login).getSingleResult();
        
        List<TypeProperty> typeProperties = session.createQuery("from TypeProperty as typeproperty where typeproperty.type.id=:type_id",TypeProperty.class)
                .setParameter("type_id",type_id)
                .getResultList();
                
        StringBuilder fields = new StringBuilder();
        
        for (TypeProperty property : typeProperties) {
            if (property.getProperty().getName().equals("all")) {
                break;
            }
            if (fields.toString().isEmpty()) {
                fields.append("select d.id as id, ");
            } else {
                fields.append(", ");
            }
            fields.append("d.");
            fields.append(property.getProperty().getName());
            fields.append(" as ");
            fields.append(property.getProperty().getName());
        }
        if (!fields.toString().isEmpty()) {
            fields.append(" ");
        }
        fields.append("from Document as d where d.user = :user and d.type.id = :type");
        
        List<Document> list = new ArrayList<Document>();
//        if (fields.toString().equals("from Document as d where d.user = :user and d.type.id = :type")) {
//            list = sessionFactory.getCurrentSession()
//                .createQuery(fields.toString())
//                .setParameter("user", user)
//                .setParameter("type", type_id)
//                .list();
//        } else {
//            list = sessionFactory.getCurrentSession()
//                .createQuery(fields.toString())
//                .setParameter("user", user)
//                .setParameter("type", type_id)
//                .list();
//        }
        list = (List<Document>) sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .list();
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Document> getFriendDocuments(String login, Long friend_id) {
        this.init();
        
        Long friend_user_id = null;
        Friend friend = (Friend)sessionFactory.getCurrentSession()
                .get(Friend.class, friend_id);
        if (friend != null) {
            friend_user_id = friend.getFriend().getId();
        }
        
        List<Document> list = sessionFactory.getCurrentSession()
                .createQuery("from Document as d where d.user.id = :user and d.type.id = 2 " +
                    "and exists(from Friend as f where f.user.username = :login and f.friend.id = d.user.id and f.accepted = 1) " +
                    "and d.shared = 1")
                .setParameter("user", friend_user_id)
                .setParameter("login", login)
                .list();
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public Result shareDocument(Long document_id, String login, Long status) {
        this.init();
        
        Result result = new Result();
        result.setSuccess(Boolean.TRUE);
        
        Session session = sessionFactory.getCurrentSession();
        
        Document document = (Document) session.createQuery("from Document as doc where doc.id=:id and doc.user.username=:login and doc.status.id=:status_id", Document.class)
                .setParameter("id", document_id)
                .setParameter("login", login)
                .setParameter("status_id", status)
                .getSingleResult();
        
        if (document == null) {
            result.setSuccess(Boolean.FALSE);
        } else {
            document.setShared(status.equals(0L) ? false : true);
            sessionFactory.getCurrentSession()
                    .saveOrUpdate(document);
        }
        
        return result;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocumentsByAvailability(String login, Long type_id, Long availability_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        User user = (User) session.createQuery("from User as user where user.username=:login", User.class).setParameter("login", login).getSingleResult();
        
        List<TypeProperty> typeProperties = session.createQuery("from TypeProperty as typeproperty where typeproperty.type.id=:type_id",TypeProperty.class)
                .setParameter("type_id",type_id)
                .getResultList();
                
        
        StringBuilder fields = new StringBuilder();
        for (TypeProperty property : typeProperties) {
            if (property.getProperty().getName().equals("all")) {
                break;
            }
            if (fields.toString().isEmpty()) {
                fields.append("select d.id as id, ");
            } else {
                fields.append(", ");
            }
            fields.append("d.");
            fields.append(property.getProperty().getName());
            fields.append(" as ");
            fields.append(property.getProperty().getName());
        }
        if (!fields.toString().isEmpty()) {
            fields.append(" ");
        }
        fields.append("from Document as d where d.user = :user and d.type.id = :type and d.availability.id = :availability");
        
        List<Document> list;
        if (fields.toString().equals("from Document as d where d.user = :user and d.type.id = :type and d.availability.id = :availability")) {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("availability", availability_id)
                .list();
        } else {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("availability", availability_id)
                .list();
        }
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocumentsByStatus(String login, Long type_id, Long status_id) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

        User user = (User) session.createQuery("from User as user where user.username=:login", User.class).setParameter("login", login).getSingleResult();
        
        List<TypeProperty> typeProperties = session.createQuery("from TypeProperty as typeproperty where typeproperty.type.id=:type_id",TypeProperty.class)
                .setParameter("type_id",type_id)
                .getResultList();
                
        
        StringBuilder fields = new StringBuilder();
        for (TypeProperty property : typeProperties) {
            if (property.getProperty().getName().equals("all")) {
                break;
            }
            if (fields.toString().isEmpty()) {
                fields.append("select d.id as id, ");
            } else {
                fields.append(", ");
            }
            fields.append("d.");
            fields.append(property.getProperty().getName());
            fields.append(" as ");
            fields.append(property.getProperty().getName());
        }
        if (!fields.toString().isEmpty()) {
            fields.append(" ");
        }
        fields.append("from Document as d where d.user = :user and d.type.id = :type and d.status.id = :status");
        
        List<Document> list;
        if (fields.toString().equals("from Document as d where d.user = :user and d.type.id = :type and d.status.id = :status")) {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("status", status_id)
                .list();
        } else {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("status", status_id)
                .list();
        }
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public List<Document> getDocumentsByExpiration(String login, Long type_id, Boolean valid) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.createQuery("from User as user where user.username=:login", User.class).setParameter("login", login).getSingleResult();
        
        StringBuilder query = new StringBuilder("from Document as d where d.user = :user and d.type.id = :type ");
        if (valid) {
            //query.append("and validfrom <= :date and validtill >= :date");
            query.append("and validtill >= :date");
        } else {
            query.append("and validtill < :date");
        }
        
        List<TypeProperty> typeProperties = session.createQuery("from TypeProperty as typeproperty where typeproperty.type.id=:type_id",TypeProperty.class)
                .setParameter("type_id",type_id)
                .getResultList();
                
        StringBuilder fields = new StringBuilder();
        for (TypeProperty property : typeProperties) {
            if (property.getProperty().getName().equals("all")) {
                break;
            }
            if (fields.toString().isEmpty()) {
                fields.append("select d.id as id, ");
            } else {
                fields.append(", ");
            }
            fields.append("d.");
            if(property.getProperty().getName() != null && !property.getProperty().getName().isEmpty()){
                 fields.append(property.getProperty().getName());
            }else{
                 fields.append("id ");
            }
           
            
            fields.append(" as ");
            if(property.getProperty().getName() != null && !property.getProperty().getName().isEmpty()){
                fields.append(property.getProperty().getName());
            }else{
                fields.append("id ");
            }
        }
        if (!fields.toString().isEmpty()) {
            fields.append(" ");
        }
        fields.append(query);
        
        List<Document> list;
        if (fields.equals(query)) {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("date", new Date())
                .list();
        } else {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type_id)
                .setParameter("date", new Date())
                .list();
        }
        
        return list;
    }

    @Transactional(rollbackFor=Exception.class)
    public Document get(Long document_id, String login) {
        this.init();
        
        Session session = sessionFactory.getCurrentSession();

//        Document document = (Document) session.createQuery("from Document as doc where doc.id=:id and doc.user.login=:login and doc.status.id=:status_id", Document.class)
//                .setParameter("id", document_id)
//                .setParameter("login", login)
//                .getSingleResult();
        Document document = (Document) session.createQuery("from Document as doc where doc.id=:id and doc.user.login=:login ", Document.class)
                .setParameter("id", document_id)
                .setParameter("login", login)
                .getSingleResult();
        
        return document;
    }

    @Transactional(rollbackFor=Exception.class)
    public void add(Document document) {
        this.init();
        
        sessionFactory.getCurrentSession()
                .save(document);
    }

    @Transactional(rollbackFor=Exception.class)
    public Type getType(Long type_id) {
        this.init();
        
        return (Type)sessionFactory.getCurrentSession()
                .get(Type.class, type_id);
    }

    @Transactional
    public byte[] getTempImage(Document document) {
        this.init();
        
        int cnd = Integer.parseInt(document.getType().getId().toString());
        
        byte[] img = null;
        
        switch(cnd) {
            case 2:
            case 1:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                Tempimg t_img = (Tempimg)sessionFactory.getCurrentSession()
                    .get(Tempimg.class, document.getTempimg().getId());
                img = t_img.getImage();
                
                break;
            case 10:
                Btempimg bt_img = (Btempimg)sessionFactory.getCurrentSession()
                    .get(Btempimg.class, document.getBtempimg().getId());
                img = bt_img.getImage();
                break;
            case 11:
                Rtempimg rt_img = (Rtempimg)sessionFactory.getCurrentSession()
                    .get(Rtempimg.class, document.getRtempimg().getId());
                img = rt_img.getImage();
                break;
            case 12:   
            default:
        }
        
        return img;
    }

 

 /*   @Transactional(rollbackFor=Exception.class)
    public void addPublictimg(Publictimg publictimg) {
        sessionFactory.getCurrentSession()
                .save(publictimg);
    }
    
    @Transactional(rollbackFor=Exception.class)
    public void editPublictimg(Publictimg publictimg) {
        sessionFactory.getCurrentSession()
                .saveOrUpdate(publictimg);
    }
*/
  /*  @Override
    public byte[] getPublictimg(Document document) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
 
/*
    @Override
    public List<Publictimg> getPublictimg() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
*/
    @Transactional(rollbackFor=Exception.class)
    public Boolean checkMobile(String mobile) {
        this.init();        
        Session session = sessionFactory.getCurrentSession();
        
        
        Document document  = (Document) session.createQuery("from Document as doc where doc.mobile=:mobile", Document.class)
                .setParameter("mobile",mobile).uniqueResult();
        if(document == null){
          return false;
        }else{
          return true;
        }
    }
    
    @Override
    @Transactional(rollbackFor=Exception.class)
    public Boolean checkTicket(String number) {
        this.init();        
        Session session = sessionFactory.getCurrentSession();                
        Document document  = (Document) session.createQuery("from Document as doc where doc.number=:number", Document.class)
                .setParameter("number",number).uniqueResult();
        if(document == null){
          return false;
        }else{
          return true;
        }
    }

    @Override
     @Transactional(rollbackFor=Exception.class)
    public void addLoyaltyCrad(Document document, User user) {
         sessionFactory.getCurrentSession().save(document);
         mobi.puut.service.entities.users.Miles miles = new mobi.puut.service.entities.users.Miles();
         miles.setUser(user);
         miles.setCardnumber(document.getNumber());
         miles.setMiles("0");
         miles.setPoints("0");
         sessionFactory.getCurrentSession().save(miles);         
    }

    @Override
     @Transactional(rollbackFor=Exception.class)
    public CloudMoney getSales() {
       CloudMoney sales = sessionFactory.getCurrentSession().createQuery("from CloudMoney where user.id=10 and currency=1",CloudMoney.class).uniqueResult();
       return sales;
    }

    @Override
     @Transactional(rollbackFor=Exception.class)
    public CloudMoney getVoucher(User user) {
       CloudMoney voucher = sessionFactory.getCurrentSession().createQuery("from CloudMoney where user=:user and currency=1",CloudMoney.class).setParameter("user", user).uniqueResult();
       return voucher;
    }

    @Override
     @Transactional(rollbackFor=Exception.class)
    public void updateMoney(User user, CloudMoney cm, CloudMoney sales) {
       Double d1 = Double.parseDouble(cm.getAmount());
       double d2 = 0.08;
       double d =d1-d2;
       int i1= cm.getDocsent(); int i2 = 1; int i3 = i1 + i2;
       int i4= cm.getDocmessage(); int i5 =1; int i6 = i4 - i5;
       
       cm.setDocsent(i3);
       cm.setDocsent(i6);
       cm.setAmount(String.valueOf(d));
       cm.setUser(user);
       cm.setCurrency(cm.getCurrency());
       sessionFactory.getCurrentSession().saveOrUpdate(cm);
       
       sales.setCurrency(cm.getCurrency());
       
       Double saleAmount = Double.parseDouble(sales.getAmount()) + 0.08;
       sales.setAmount(String.valueOf(saleAmount));
       sessionFactory.getCurrentSession().saveOrUpdate(sales);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public void updateMoney(User user, CloudMoney sales) {
        CloudMoney cloudmoney = sessionFactory.getCurrentSession().createQuery("from CloudMoney where user=:user",CloudMoney.class).setParameter("user", user).uniqueResult();
         Double d1 = Double.parseDouble(cloudmoney.getAmount());
         double d2 = 0.08;
         double d =d1-d2;
         int i1= cloudmoney.getDocsent(); int i2 = 1; int i3 = i1 + i2;
         int i4= cloudmoney.getDocmessage(); int i5 =1; int i6 = i4 - i5;
         cloudmoney.setDocsent(i3);
         cloudmoney.setDocsent(i6);
         cloudmoney.setAmount(String.valueOf(d));
         cloudmoney.setUser(user);
         cloudmoney.setCurrency(cloudmoney.getCurrency());
         sessionFactory.getCurrentSession().saveOrUpdate(cloudmoney);
        
        sales.setCurrency(cloudmoney.getCurrency());
        
       Double d3 = Double.parseDouble(sales.getAmount());
       double d4 = 0.08;
       double d5 =d3+d4;        
       sales.setAmount(String.valueOf(d5));
       sessionFactory.getCurrentSession().saveOrUpdate(sales);
        
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Document getById(Long id) {
         this.init();
        
        Session session = sessionFactory.getCurrentSession();

        Document document  =  session.createQuery("from Document as doc where doc.id=:id",Document.class)
                .setParameter("id",id)
                .getSingleResult();

        
        return document;
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public boolean checkProgramAndType(Type type, User partner,Program program) {
        this.init();        
        Session session = sessionFactory.getCurrentSession();
        List<Document> documents  =  session.createQuery("from Document as doc where doc.partner=:partner and doc.program=:program",Document.class)
                .setParameter("partner",partner)
                .setParameter("program",program)
                .getResultList();
        
        if(documents.isEmpty() || documents.size()==0){
            return true;
        }
        
        for (Document document : documents) {
            if(document.getType().getId().equals(type.getId())){
              return true;
            }
        }        
        return false;        
    }

	@Override
	public List<Subtype> getSubTypes(Long type_id) {
		// TODO Auto-generated method stub
		   this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        List<Subtype> list = (List<Subtype>) session.createQuery("from Subtype as subtype where subtype.type.id=:type_id", Subtype.class)
	                .setParameter("type_id", type_id).getResultList();

	        return list;
	}

	@Override
	public List<Availability> getAvailabilities() {
		// TODO Auto-generated method stub
		  this.init();
	        
	        Session session = sessionFactory.getCurrentSession();
	        
	        List<Availability> list = (List<Availability>) session.createQuery("from Availability").getResultList();
	        
	        return list;
	}

	@Override
	public List<Document> getDocument(Long type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Document> getFriendBusinessCard(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Document> getDocumentsByAvailability(Long type, String login, Long availability) {
		// TODO Auto-generated method stub
        this.init();
        
        Session session = sessionFactory.getCurrentSession();
        
        User user = (User) session.createQuery("from User as user where user.username=:login", User.class).setParameter("login", login).getSingleResult();
        
        List<TypeProperty> typeProperties = session.createQuery("from TypeProperty as typeproperty where typeproperty.type.id=:type_id",TypeProperty.class)
                .setParameter("type_id",type)
                .getResultList();
                
        
        StringBuilder fields = new StringBuilder();
        for (TypeProperty property : typeProperties) {
            if (property.getProperty().getName().equals("all")) {
                break;
            }
            if (fields.toString().isEmpty()) {
                fields.append("select d.id as id, ");
            } else {
                fields.append(", ");
            }
            fields.append("d.");
            fields.append(property.getProperty().getName());
            fields.append(" as ");
            fields.append(property.getProperty().getName());
        }
        if (!fields.toString().isEmpty()) {
            fields.append(" ");
        }
        fields.append("from Document as d where d.user = :user and d.type.id = :type and d.availability.id = :availability");
        
        List<Document> list;
        if (fields.toString().equals("from Document as d where d.user = :user and d.type.id = :type and d.availability.id = :availability")) {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type)
                .setParameter("availability", availability)
                .list();
        } else {
            list = sessionFactory.getCurrentSession()
                .createQuery(fields.toString())
                .setParameter("user", user)
                .setParameter("type", type)
                .setParameter("availability", availability)
                .list();
        }
        
        return list;
	}

	@Override
	public List<Document> getDocumentsByStatus(Long type, String login, Long status) {
		// TODO Auto-generated method stub
this.init();
        
        Session session = sessionFactory.getCurrentSession();

        List<Document> list = (List<Document>) session.createQuery("from Document as doc where doc.user.username=:login and doc.type.id=:type_id and doc.status.id=:status_id",Document.class)
                .setParameter("login", login)
                .setParameter("type_id",type)
                .setParameter("status_id",status)
                .getResultList();


        return list;
	}

	@Override
	public byte[] getPublictimg(Document document) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Document get(long document_id, String login, long type_id, long status_id) {
		// TODO Auto-generated method stub
		 this.init();
	        
	        Session session = sessionFactory.getCurrentSession();

	        Document document  = (Document)         session.createQuery("from Document as doc where doc.id=:id and doc.user.username = :login and doc.type.id=:type_id and doc.status.id=:status_id",Document.class)
	                .setParameter("id",document_id)
	                .setParameter("login",login)
	                .setParameter("type_id",type_id)
	                .setParameter("status_id", status_id)
	                .getSingleResult();
	        
	        return document;
	}


	




	




}