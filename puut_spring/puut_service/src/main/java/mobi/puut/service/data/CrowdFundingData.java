package mobi.puut.service.data;

import java.util.List;

import mobi.puut.service.entities.cloudmoney.Currency;
import mobi.puut.service.entities.crowdfunding.Area;
import mobi.puut.service.entities.crowdfunding.CrowdList;
import mobi.puut.service.entities.crowdfunding.Phase;
import mobi.puut.service.entities.users.AccountSetting;
import mobi.puut.service.entities.users.User;

public interface CrowdFundingData {

	  public List getCrowdFounding(User user);

	public void add(CrowdList crowdList);

	public List getPreviousCrowd(User user);

	public List getProjecttitle(Long id);

	public boolean checkTitle(String projecttitle);

	public boolean checkphone(String phone);

	public AccountSetting getAccountSettings(User user);

	public List<Phase> getPhase();

	public List<Area> getArea();

	public CrowdList getDifferentDate(Long id);

	public List<Currency> getCurrency();
	  
	
}
