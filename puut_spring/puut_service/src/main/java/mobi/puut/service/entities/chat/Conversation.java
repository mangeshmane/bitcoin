package mobi.puut.service.entities.chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import mobi.puut.service.entities.users.User;

@Entity
//@XmlRootElement
@Table(name="conversation")
public class Conversation implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;
    
    @Column(name = "name", columnDefinition="VARCHAR(30)")
    private String name;
    
    @Lob
    @Column(name = "image", length=16777215)
    private byte[] convPicture;  
    
    @ManyToOne
    @JoinColumn(name = "owner")
    private User owner;
    
    @OneToMany(mappedBy="conversationIdParticipants",cascade = {CascadeType.ALL}, fetch=FetchType.EAGER)
    @Transient
    private List<Participant> participants = new ArrayList<Participant>();
   
    @OneToMany(mappedBy= "conversationIdMessages",cascade = {CascadeType.ALL})
    @Transient
    private List<Chats> messages = new ArrayList<Chats>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public byte[] getConvPicture() {
        return convPicture;
    }

    public void setConvPicture(byte[] convPicture) {
        this.convPicture = convPicture;
    }
    
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Chats> getMessages() {
        return messages;
    }

    public void setMessages(List<Chats> messages) {
        this.messages = messages;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conversation)) {
            return false;
        }
        Conversation other = (Conversation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "Conversation{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' + 
                    ", owner='" + owner + '\'' + 
                    ", participants='" + participants.toString() + '\'' +
                    ", conversationIdMessages='" + messages.toString() + '\'' +       
                "}";
    }
}
