
package mobi.puut.service.entities.chat;

import mobi.puut.service.entities.users.User;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
//@XmlRootElement
@Table(name="usertouser")
public class UserToUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user1")
    private User user1Id;
    
    @ManyToOne
    @JoinColumn(name = "user2")
    private User user2Id;
    
    @OneToMany(mappedBy = "usertouserId")
    private List<BlockInterval> blockIntervals;
    
    @Column(name = "blocked", columnDefinition="TINYINT(1)")
    private Boolean blocked;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public User getUser1Id() {
        return user1Id;
    }

    public void setUser1Id(User user1Id) {
        this.user1Id = user1Id;
    }

    public User getUser2Id() {
        return user2Id;
    }

    public void setUser2Id(User user2Id) {
        this.user2Id = user2Id;
    }
    
    public List<BlockInterval> getBlockIntervals() {
        return blockIntervals;
    }

    public void setBlockIntervals(List<BlockInterval> blockIntervals) {
        this.blockIntervals = blockIntervals;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }
    
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserToUser)) {
            return false;
        }
        UserToUser other = (UserToUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "UserToUser{" +
                    "id='" + id + '\'' +
                    ", user1Id='" + user1Id.toString() + '\'' + 
                    ", user2Id='" + user2Id.toString() + '\'' +
                    ", blockIntervals='" + blockIntervals.toString() + '\'' + 
                    ", blocked='" + blocked + '\'' +
                "}";
    }
}
