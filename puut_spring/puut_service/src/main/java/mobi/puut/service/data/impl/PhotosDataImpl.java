package mobi.puut.service.data.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mobi.puut.service.data.PhotosData;
import mobi.puut.service.entities.documents.Result;
import mobi.puut.service.entities.photo.Photo;
import mobi.puut.service.entities.photo.PhotoAlbum;
import mobi.puut.service.entities.users.User;

@Service("photosData")
public class PhotosDataImpl implements PhotosData{

	
@Autowired
SessionFactory sessionFactory;
	

	@Transactional
	@Override
	public Photo getPhoto(Long id, String login) {
		// TODO Auto-generated method stub
		
		Session session=sessionFactory.getCurrentSession();

		return (Photo) session.createQuery("from Photo as photo where photo.id=:id and photo.user.username=:login")
				.setParameter("id", id)
				.setParameter("login", login)
				.uniqueResult();
		
		
	}
	
	
	@Transactional
	@Override
	public List<PhotoAlbum> getAlbums(String login) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		
		return session.createQuery("from PhotoAlbum as photoalbum where photoalbum.user.username=:login")
				.setParameter("login", login).getResultList();
		
				}

	
	@Transactional
	@Override
	public List<PhotoAlbum> getFriendAlbums(Long friend_id, String login) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		
		return session.createQuery("from PhotoAlbum as photoalbum where photoalbum.id=:friend_id and photoalbum.user.username=:login")
				.setParameter("friend_id", friend_id)
				.setParameter("login",login).getResultList();
				
		
	}
	
	
	@Transactional
	@Override
	public List<Photo> getPhotos(Long album_id, String login) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();

		 List<Photo> photos= session.createQuery("from Photo as photo where photo.id=:id and photo.user.username=:login")
				.setParameter("id", album_id)
				.setParameter("login", login)
				.getResultList();
		 
		 return photos;
	}

		
@Transactional
@Override
	public Result sharePhoto(Long id, Long status, String login) {
		// TODO Auto-generated method stub
		Result result=new Result();
		result.setSuccess(Boolean.FALSE);
		
		Session session=sessionFactory.getCurrentSession();
		
		Photo photo=(Photo) session.createQuery("from Photo as photo where photo.id=:id and photo.user.username=:login")
		.setParameter("id", id)
		.setParameter("login", login).uniqueResult();
		
		
		if(photo==null) {
			result.setSuccess(Boolean.FALSE);		
		}else {
			result.setSuccess(Boolean.TRUE);
		}
		
		return result;
	}

	@Override
	public Result voteForPhoto(Long id, Long status, String login) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Transactional
	@Override
	public PhotoAlbum get(Long id, User user) {
		// TODO Auto-generated method stub
		PhotoAlbum photoAlbum=(PhotoAlbum) sessionFactory.getCurrentSession().createQuery("from PhotoAlbum as photoalbum  where photoalbum.id=:id and photoalbum.user=:user")
				.setParameter("id", id)
				.setParameter("user", user)
				.uniqueResult();
		return photoAlbum;
	}

	
	@Transactional
	@Override
	public void add(Photo photo) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(photo);
	}

	
	@Transactional
	@Override
	public Result createAlbum(PhotoAlbum photoAlbum, String login) {
		// TODO Auto-generated method stub
		Result result=new Result();
		result.setSuccess(Boolean.FALSE);
		
		Session session=sessionFactory.getCurrentSession();
		
		if(photoAlbum!=null) {
		session.save(photoAlbum);
		result.setSuccess(Boolean.TRUE);
		}
		
		return result;
		
	}

	
	
	
	
	
}
