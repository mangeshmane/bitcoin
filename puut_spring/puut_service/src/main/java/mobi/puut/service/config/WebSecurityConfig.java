package mobi.puut.service.config;


import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.expression.SecurityExpressionOperations;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.ApiKeys;
import mobi.puut.service.entities.users.User;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan("mobi.puut.service.data.*")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	DriverManagerDataSource datasource;
	
	@Autowired
	CustomAuthenticationProvider provider;
		
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		// TODO Auto-generated method stub
		
		
		
		
		httpSecurity.addFilterBefore(new JWTFilter(), UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(authenticationProcessingFilter(),UsernamePasswordAuthenticationFilter.class)
		.httpBasic()
		.and()
		.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
		.antMatchers("/login").permitAll()
		.antMatchers("/profile/remind/*").permitAll()
		.antMatchers("/profile/remind").permitAll()
		.antMatchers("/endpoint/**").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		//.loginPage("/").permitAll()
		.successHandler(new CustomRequestAuthenticationSuccessHandler())
		.and()		
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.csrf().disable();
		
	}

	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManagerBean();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		//super.configure(web);
		web.ignoring()
		.antMatchers("/login")
		.antMatchers("/profile/remind/*")
		.antMatchers("/profile/remind")
		.antMatchers("/endpoint/**");
		
		
	   web.expressionHandler(new DefaultWebSecurityExpressionHandler(){
		
		@Override
		protected SecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication,FilterInvocation fi) {
			WebSecurityExpressionRoot root=(WebSecurityExpressionRoot) super.createSecurityExpressionRoot(authentication, fi);
			root.setDefaultRolePrefix("");
			return root;
			
		}
	});
}
	public AccessDecisionManager accessDecisionManager() {
		List<AccessDecisionVoter<? extends Object>> decisionVoters
		=Arrays.asList(
				new WebExpressionVoter(),
				new RoleVoter(),
				new AuthenticatedVoter());
			
		return new AffirmativeBased(decisionVoters);	
	}
	
	
	
	@Bean(name="authenticationDetailsSourceImp")
	public AuthenticationDetailsSourceImpl authenticationDetailsSourceImpl() {
		return new AuthenticationDetailsSourceImpl();
	}
	
	
	
	@Bean(name="customSuccessHandler")
	public CustomRequestAuthenticationSuccessHandler customSuccessHandler() {
		return new CustomRequestAuthenticationSuccessHandler();
	}
	
	
	

	@Bean(name="authenticationProcessingFilter")
	public UsernamePasswordAuthenticationFilter authenticationProcessingFilter() throws Exception{
		
		UsernamePasswordAuthenticationFilter filter=new UsernamePasswordAuthenticationFilter();
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setAuthenticationDetailsSource(authenticationDetailsSourceImpl());
		filter.setAuthenticationSuccessHandler(customSuccessHandler());
		
		return filter;
	}
	
	
	
	}
	
	
	
	


