
package mobi.puut.service.utils.chat;


public enum DateEnum {
  
	Time,
    DateTime,
    DayMonthTime,
    Full;
}
