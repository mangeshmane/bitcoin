
package mobi.puut.service.config;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import mobi.puut.service.data.UserData;
import mobi.puut.service.entities.users.Role;
import mobi.puut.service.utils.chat.DateScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.socket.server.standard.SpringConfigurator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
//@ServerEndpoint(value = "/endpoint/{login}/{password}", configurator = SpringConfigurator.class, encoders = {MessageEncoder.class})
@ServerEndpoint(value = "/endpoint/{token}", configurator = SpringConfigurator.class, encoders = {MessageEncoder.class})
public class WebsocketServer {

    private static final Map<String, Session> openSessions = Collections.synchronizedMap(new HashMap<String, Session>());

    @Autowired
    private mobi.puut.service.services.chat.chatService chatService;

    //@Autowired
    //private UserService userService;
    @Autowired
    UserData userData;

    @Autowired
    private DateScanner dateScanner;

    @Autowired
    public HttpServletRequest request;

    @ExceptionHandler(value = MultipartException.class)
    public void handleFileUploadException(MultipartException mpex, HttpServletRequest request) {
    }

    @ExceptionHandler(value = Exception.class)
    public void handleException(Exception ex, HttpServletRequest request) {
    }

    /**
     * Function called when we open the socket
     *
     * @param session To get the current session
     * @throws java.io.IOException
     */
    
    public Authentication getUserFromToken(String token) {
    	String userToken = token.substring(7).replace("]", "");
		Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(userToken).getBody();
		Authentication authentication = new JWTFilter().getAuthentication(claims);
		return authentication;
    }
    
    @OnOpen
    //public void onOpen(Session session,@PathParam("login") String login,@PathParam("password") String password) throws IOException {
    public void onOpen(Session session,@PathParam("token") String token) throws IOException {	 
    		if (token != null) { 
    			Authentication authentication = getUserFromToken(token);
				String username = authentication.getName();
				mobi.puut.service.entities.users.User user = userData.getByUsername(username);
	            //verifyUsingUsernamePassword(login, password, user);
				 
	            session.setMaxTextMessageBufferSize(2000000);
				openSessions.put(user.getUsername(), session); 
				/*
	            if(claims.getExpiration().getTime()==(new Date().getTime()))
	            	System.out.println("Token expired");*/
    		}
    	    /*mobi.puut.service.entities.users.User user = userData.getByUsername(login);
            verifyUsingUsernamePassword(login, password, user);
            openSessions.put(user.getUsername(), session);  
            session.setMaxTextMessageBufferSize(2000000);*/
            
    }

    private UserDetails verifyUsingUsernamePassword(String username, String password, mobi.puut.service.entities.users.User user) {
        String exceptionMessage  = "";
        if (user.getEnabled()) {
            if (password.equals(user.getPassword())) {
                return buildUserFromUserEntity(user);
            } else {
                exceptionMessage = "Please enter correct password";
                throw new BadCredentialsException(exceptionMessage);
            }
        } else {
            exceptionMessage = "Please activate account from registered email";
            throw new BadCredentialsException(exceptionMessage);
        }
    }
    
    private UserDetails buildUserFromUserEntity(mobi.puut.service.entities.users.User userEntity) {
        // convert model user to spring security user

        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        Role role = userData.getRole(userEntity.getUsername());

        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getName());
        authorities.add(grantedAuthority);

        String username = userEntity.getUsername();
        String password = userEntity.getPassword();

        User springUser = new User(username, password, authorities);
        return springUser;
    }    
   
    /**
     * Function called when we send an event from the client through the socket
     *
     * @param message The message that we get
     * @param session The current user session
     * @throws java.io.IOException
     * @throws javax.websocket.EncodeException
     */
    @PostConstruct
    public void init() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }
    
    @OnMessage
    public void onMessage(String message, Session session) throws IOException, EncodeException {
        
         String token = session.getRequestParameterMap().get("token").toString();
         Authentication authentication = getUserFromToken(token);
         String username = authentication.getName();
        //String username = "";
        
        /*Map<String,Session> temp = new HashMap<String,Session>();
        temp = getSessionFromMap(session);
        
        for(Map.Entry<String,Session> entry: temp.entrySet()){
           session =  entry.getValue();
           username =  entry.getKey();            
        }
        

        JSONObject eventObject = new JSONObject(message);
        */
        		 
         JsonReader reader = Json.createReader(new StringReader(message));       
         JsonObject eventObject = reader.readObject();
         reader.close();

        // If the user just opens a pop-up, we get the messages stored for this conversation and we display them
        if (eventObject.getString("name").equals("connecting")) {

            List<List> messages;
            String conv = eventObject.getString("activeConv");

            if (chatService.isExistingConversation(conv)) {
                chatService.changeTimeJoined(conv, username);
                messages = chatService.loadMessages(conv, userData.getByUsername(username).getId());

                chatService.updateLastReadMessage(new Date(), conv, username);

                if (chatService.countParticipants(conv) > 2) {

                    for (int i = 0; i < messages.get(0).size(); i++) {

                        String fullnamesList = chatService.getParticipantsFullnames(conv, username);

                        List<String> params = new ArrayList<>();
                        params.add(Integer.toString((Integer) messages.get(1).get(i)));
                        params.add("");
                        params.add(conv);
                        params.add("");
                        params.add(fullnamesList);
                        params.add((String) messages.get(0).get(i));
                        params.add((String) messages.get(2).get(i));

                        WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);

                        session.getBasicRemote().sendObject(wsmessage);

                        if (i == messages.get(0).size() - 1) {
                            WebsocketMessage wsconnecting = new WebsocketMessage("connecting", new ArrayList<>());

                            session.getBasicRemote().sendObject(wsconnecting);
                        }
                    }
                } else {
                    for (int i = 0; i < messages.get(0).size(); i++) {
                        String fullname = chatService.getFriendFullname(conv, username);

                        String fullnamesList = chatService.getParticipantsFullnames(conv, username);

                        List<String> params = new ArrayList<>();
                        params.add(Integer.toString((Integer) messages.get(1).get(i)));
                        params.add("");
                        params.add(conv);
                        params.add(fullname);
                        params.add(fullnamesList);
                        params.add((String) messages.get(0).get(i));
                        params.add("");

                        WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);
                        session.getBasicRemote().sendObject(wsmessage);

                        if (i == messages.get(0).size() - 1) {
                            WebsocketMessage wsconnecting = new WebsocketMessage("connecting", new ArrayList<>());

                            session.getBasicRemote().sendObject(wsconnecting);
                        }
                    }
                }
            }
        } // When a user sends a message in the pop-up, we display it and we are going to store it in the database
        else if (eventObject.getString("name").equals("message")) {

            String conv = eventObject.getString("activeConv");

            if (!chatService.isExistingConversation(conv)) {
                chatService.createConversationWithUsers(conv, new Date());
            }

            Date d = new Date();
            Date lastMessageDate = chatService.getLastMessageDate(conv, username);
            chatService.checkVisibility(conv, username);
            String previousMessage = chatService.getLastMessageUsername(conv, username);
            Date previousMessageDate = chatService.getLastMessageDate(conv, username);
            chatService.addMessageWithUsers(username, conv, eventObject.getString("message"), d);
            chatService.updateLastReadMessage(d, conv, username);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(d);

            String dateString = null;
            if (lastMessageDate != null) {
                dateString = dateScanner.compareDates(d.getTime(), lastMessageDate.getTime());
            }

            for (Session s : openSessions.values()) {

                String[] convParams = conv.split("-");
                String[] friends = convParams[1].split("_");
                List<String> friendsPseudos = new ArrayList<>();
                String fullname = "";

                for (String fr : friends) {
                    friendsPseudos.add(fr);
                }

                if (s.isOpen() && friendsPseudos.contains("" + userData.getByUsername(getUserNameFromSession(s.getUserProperties())).getId()) && chatService.checkActive(conv, getUserNameFromSession(s.getUserProperties()))) {

                    boolean blocking = false;
                    if (friendsPseudos.size() == 2) {
                        if (chatService.loadBlockingRelation(username, getUserNameFromSession(s.getUserProperties()))) {
                            blocking = true;
                        }
                        fullname = chatService.getFriendFullname(conv, getUserNameFromSession(s.getUserProperties()));
                    }

                    if (!blocking) {
                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                        timeFormat.setCalendar(calendar);

                        String fullnamesList = chatService.getParticipantsFullnames(conv, getUserNameFromSession(s.getUserProperties()));

                        if (dateString != null) {
                            List<String> params = new ArrayList<>();
                            params.add(Integer.toString(0));
                            params.add(eventObject.getString("sender"));
                            params.add(conv);
                            params.add(fullname);
                            params.add(fullnamesList);
                            params.add(dateString);
                            params.add("");

                            WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);

                            s.getBasicRemote().sendObject(wsmessage);

                        } else {
                            if (lastMessageDate == null) {
                                dateString = dateScanner.compareDatesFirstMessage(new Date().getTime(), d.getTime());

                                List<String> params = new ArrayList<>();
                                params.add(Integer.toString(0));
                                params.add(eventObject.getString("sender"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(dateString);
                                params.add("");

                                WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);

                                s.getBasicRemote().sendObject(wsmessage);
                            }
                        }

                        List<String> params = new ArrayList<>();
                        if (username.equals(getUserNameFromSession(s.getUserProperties()))) {
                            params.add(Integer.toString(1));
                            params.add(eventObject.getString("sender"));
                            params.add(conv);
                            params.add(fullname);
                            params.add(fullnamesList);
                            params.add(eventObject.getString("message"));
                            params.add("");
                        } else {
                            if ((previousMessageDate.after(chatService.getTimeJoined(conv, getUserNameFromSession(s.getUserProperties()))) && username.equals(previousMessage)) || friendsPseudos.size() == 2) {
                                params.add(Integer.toString(2));
                                params.add(eventObject.getString("sender"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(eventObject.getString("message"));
                                params.add("");
                            } else {
                                String fullnameSender = userData.getByUsername(username).getFirstname() + " " + userData.getByUsername(username).getLastname();
                                params.add(Integer.toString(2));
                                params.add(eventObject.getString("sender"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(eventObject.getString("message"));
                                params.add(fullnameSender);
                            }
                        }
//                        WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);
//                        List members = chatService.getMembersInConversation(eventObject);
//                         for (int i = 0; i < members.size(); i++) {
//                               if(members.get(i).equals(s.getUserPrincipal().getName())){
//                                     openSessions.get(s.getUserPrincipal().getName()).getBasicRemote().sendObject(wsmessage);
//                                }
//                         }
//                        for (Map.Entry<String, Session> entry : openSessions.entrySet()) {
//                                String key = entry.getKey();
//                                Session value = entry.getValue();
//                                    for (int i = 0; i < members.size(); i++) {
//                                         if(members.get(i).equals(key)){
//                                             openSessions.get(key).getBasicRemote().sendObject(wsmessage);
//                                         }
//                                     }
//                         openSessions.get(key).getBasicRemote().sendObject(wsloadblocking);
//                            }
                        WebsocketMessage wsmessage = new WebsocketMessage("simpleMessage", params);
                        s.getBasicRemote().sendObject(wsmessage);
                    }
                }
            }
        } // When an user is typing in a pop-up, it sends an event through the socket and other users are able to see that this user is typing inside this pop-up
        else if (eventObject.getString("name").equals("typing")) {

            String[] convParams = eventObject.getString("activeConv").split("-");
            String[] participantTable = convParams[1].split("_");

            ArrayList<String> participantList = new ArrayList<>();
            for (int i = 0; i < participantTable.length; i++) {
                participantList.add(participantTable[i]);
            }
            for (Session s : openSessions.values()) {

                if (!username.equals(getUserNameFromSession(s.getUserProperties()))) {

                    if (participantList.contains("" + userData.getByUsername(getUserNameFromSession(s.getUserProperties())).getId())) {

                        boolean blocked = false;
                        if (participantList.size() == 2) {
                            if (chatService.loadBlockingRelation(username, getUserNameFromSession(s.getUserProperties()))) {
                                blocked = true;
                            }
                        }

                        if (!blocked) {
                            List<String> params = new ArrayList<>();
                            params.add(String.valueOf(eventObject.getBoolean("typing")));
                            params.add(eventObject.getString("activeConv"));
                            params.add(username);

                            WebsocketMessage wstyping = new WebsocketMessage("isTyping", params);
                            s.getBasicRemote().sendObject(wstyping);
                        }
                    }
                }
            }
        } // To block an user
        else if (eventObject.getString("name").equals("blocking")) {

            String[] convParams = eventObject.getString("activeConv").split("-");
            String[] participants = convParams[1].split("_");
            String friendname = "";
            Boolean status = null;
            List<String> params = new ArrayList<>();
            for (String s : participants) {
                if (!s.equals("" + userData.getByUsername(username).getId())) {
                    friendname = userData.get(Long.valueOf(s)).getUsername();
                }
            }

            if (!friendname.equals("")) {
                if (eventObject.getBoolean("block")) {
                    status = chatService.blockUser(friendname, username);
                } else {
                    status = chatService.unblockUser(friendname, username);
                }
            }
            String fullname = "";

            if (participants.length == 2) {
                fullname = chatService.getFriendFullname(eventObject.getString("activeConv"), username);
            }

            ArrayList<String> blockedList = new ArrayList<>();

            for (int i = 0; i < participants.length; i++) {
                if (!userData.get(Long.valueOf(participants[i])).getUsername().equals(username)) {
                    if (chatService.loadBlockingRelation(userData.get(Long.valueOf(participants[i])).getUsername(), username)) {
                        blockedList.add(userData.get(Long.valueOf(participants[i])).getFirstname() + " " + userData.get(Long.valueOf(participants[i])).getLastname());
                    }
                }
            }

            String blockedString = "";
            for (int j = 0; j < blockedList.size(); j++) {
                if (j == blockedList.size() - 1) {
                    blockedString += blockedList.get(j);
                } else {
                    blockedString += blockedList.get(j) + "_";
                }
            }

            String fullnamesList = chatService.getParticipantsFullnames(eventObject.getString("activeConv"), username);
            params.add(String.valueOf(status));
            params.add(fullname);
            params.add(fullnamesList);
            params.add(blockedString);

            WebsocketMessage wsblocking = new WebsocketMessage("blocking", params);
            session.getBasicRemote().sendObject(wsblocking);
        } // To load the blocking relations for a particular conversation
        else if (eventObject.getString("name").equals("loadBlocking")) {

            String conv = eventObject.getString("activeConv");

            String[] convParams = conv.split("-");
            String[] participants = convParams[1].split("_");

            String fullname = "";

            if (participants.length == 2) {
                fullname = chatService.getFriendFullname(conv, username);
            }

            boolean blocking = false;
            ArrayList<String> blockedList = new ArrayList<>();

            for (int i = 0; i < participants.length; i++) {
                if (!userData.get(Long.valueOf(participants[i])).getUsername().equals(username)) {
                    if (chatService.loadBlockingRelation(participants[i], username)) {
                        blocking = true;
                        blockedList.add(userData.get(Long.valueOf(participants[i])).getFirstname() + " " + userData.get(Long.valueOf(participants[i])).getLastname());
                    }
                }
            }

            String blockedString = "";
            for (int j = 0; j < blockedList.size(); j++) {
                if (j == blockedList.size() - 1) {
                    blockedString += blockedList.get(j);
                } else {
                    blockedString += blockedList.get(j) + "_";
                }
            }

            String fullnamesList = chatService.getParticipantsFullnames(conv, username);

            List<String> params = new ArrayList<>();
            params.add(conv);
            params.add(fullname);
            params.add(fullnamesList);
            params.add(blockedString);
            params.add(String.valueOf(blocking));

            WebsocketMessage wsloadblocking = new WebsocketMessage("blockingRelation", params);
            session.getBasicRemote().sendObject(wsloadblocking);
        } else if (eventObject.getString("name").equals("openConv")) {

            List<String> params = new ArrayList<>();
            params.add(eventObject.getString("term"));

            WebsocketMessage wsopenconv = new WebsocketMessage("openConv", params);
            session.getBasicRemote().sendObject(wsopenconv);
        } else if (eventObject.getString("name").equals("image")) {

            String conv = eventObject.getString("activeConv");
            String[] convParams = conv.split("-");
            String[] friendsString = convParams[1].split("_");

            List<String> friendsPseudos = new ArrayList<>();
            String fullname = "";

            for (String tabFriend : friendsString) {
                friendsPseudos.add(tabFriend);
            }

            if (friendsPseudos.size() == 2) {
                fullname = chatService.getFriendFullname(conv, username);
            }

            if (!chatService.isExistingConversation(conv)) {
                chatService.createConversationWithUsers(conv, new Date());
            }

            Date d = new Date();
            Date lastMessageDate = chatService.getLastMessageDate(conv, username);

            chatService.checkVisibility(conv, username);
            String previousMessage = chatService.getLastMessageUsername(conv, username);
            chatService.addMessageWithUsers(username, conv, eventObject.getString("image"), d);
            chatService.updateLastReadMessage(d, conv, username);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(d);

            String dateString = null;
            if (lastMessageDate != null) {
                dateString = dateScanner.compareDates(d.getTime(), lastMessageDate.getTime());
            }

            for (Session s : openSessions.values()) {
            	
            	String usertoken = s.getRequestParameterMap().get("token").toString();
                Authentication auth = getUserFromToken(usertoken);
                //if (s.isOpen() && friendsPseudos.contains("" + userData.getUserByLogin(s.getUserPrincipal().getName()).getId()) && chatService.checkActive(conv, s.getUserPrincipal().getName())) {
                   //if (s.isOpen() && friendsPseudos.contains("" + userData.getByUsername(getUserNameFromSession(s.getUserProperties())).getId()) && chatService.checkActive(conv, getUserNameFromSession(s.getUserProperties()))) {
                if (s.isOpen() && friendsPseudos.contains("" + userData.getByUsername(auth.getName()).getId()) && chatService.checkActive(conv, auth.getName())) {    
                boolean blocking = false;
                    if (friendsPseudos.size() == 2) {
                        if (chatService.loadBlockingRelation(username, getUserNameFromSession(s.getUserProperties()))) {
                            blocking = true;
                        }
                    }

                    if (!blocking) {
                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                        timeFormat.setCalendar(calendar);

                        String fullnamesList = chatService.getParticipantsFullnames(conv, username);

                        if (dateString != null) {
                            List<String> params = new ArrayList<>();
                            params.add(Integer.toString(0));
                            params.add(eventObject.getString("user"));
                            params.add(conv);
                            params.add(fullname);
                            params.add(fullnamesList);
                            params.add(dateString);
                            params.add("");

                            WebsocketMessage wsimage = new WebsocketMessage("simpleMessage", params);

                            s.getBasicRemote().sendObject(wsimage);
                        } else {
                            if (lastMessageDate == null) {
                                dateString = dateScanner.compareDatesFirstMessage(new Date().getTime(), d.getTime());

                                List<String> params = new ArrayList<>();
                                params.add(Integer.toString(0));
                                params.add(eventObject.getString("user"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(dateString);
                                params.add("");

                                WebsocketMessage wsimage = new WebsocketMessage("simpleMessage", params);

                                s.getBasicRemote().sendObject(wsimage);
                            }
                        }

                        List<String> params = new ArrayList<>();
                        if (username.equals(getUserNameFromSession(s.getUserProperties()))) {
                            params.add(Integer.toString(1));
                            params.add(eventObject.getString("user"));
                            params.add(conv);
                            params.add(fullname);
                            params.add(fullnamesList);
                            params.add(eventObject.getString("image"));
                            params.add("");
                        } else {

                            if (username.equals(previousMessage) || friendsPseudos.size() == 2) {
                                params.add(Integer.toString(2));
                                params.add(eventObject.getString("user"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(eventObject.getString("image"));
                                params.add("");
                            } else {
                                params.add(Integer.toString(2));
                                params.add(eventObject.getString("user"));
                                params.add(conv);
                                params.add(fullname);
                                params.add(fullnamesList);
                                params.add(eventObject.getString("image"));
                                params.add(username);
                            }
                        }

                        WebsocketMessage wsimage = new WebsocketMessage("displayImage", params);
                        s.getBasicRemote().sendObject(wsimage);
                    }
                }
            }
        } else if (eventObject.getString("name").equals("addParticipant")) {

            String conv = eventObject.getString("conv");
            String term = eventObject.getString("username");

            if (chatService.verifyParticipantInConv(conv, term)) {

                if (chatService.addParticipantInConv(conv, term, username)) {

                    String newConv = chatService.openExistingConv(conv, term);

                    for (Session s : openSessions.values()) {

                        if (s.isOpen() && !chatService.verifyParticipantInConv(newConv, getUserNameFromSession(s.getUserProperties()))) {

                            String fullnamesList = chatService.getParticipantsFullnames(newConv, getUserNameFromSession(s.getUserProperties()));

                            /*JsonObject json = Json.createObjectBuilder()
                                    .add("name", "simpleMessage")
                                    .add("sender", "")
                                    .add("conv", newConv)
                                    .add("oldConv", conv)
                                    .add("type", 4)
                                    .add("fullname", chatService.getFullnameByLogin(term))
                                    .add("fullnamesList", fullnamesList)
                                    .add("username", term).build();

                            StringWriter w = new StringWriter();
                            try (JsonWriter writer = Json.createWriter(w)) {
                                writer.write(json);
                            }
                            SocketIOFrame fMessage = new SocketIOFrame(SocketIOFrame.FrameType.JSON_MESSAGE, SocketIOFrame.JSON_MESSAGE_TYPE, w.toString());
                            s.getBasicRemote().sendObject(fMessage);*/
                            List<String> params = new ArrayList<>();
                            params.add(Integer.toString(4));
                            params.add("");
                            params.add(newConv);
                            params.add(conv);
                            params.add(chatService.getFullnameByLogin(term));
                            params.add(fullnamesList);
                            params.add(term);

                            WebsocketMessage wsaddparticipant = new WebsocketMessage("simpleMessage", params);

                            s.getBasicRemote().sendObject(wsaddparticipant);
                        }
                    }
                }
            }
        } else if (eventObject.getString("name").equals("leaveConversation")) {

            String conv = eventObject.getString("conv");

            if (chatService.leaveConversation(conv, username)) {

                for (Session s : openSessions.values()) {

                    if (!username.equals(getUserNameFromSession(s.getUserProperties()))) {

                        if (s.isOpen() && !chatService.verifyParticipantInConv(conv, getUserNameFromSession(s.getUserProperties()))) {

                            String fullnamesList = chatService.getParticipantsFullnames(conv, username);

                            /*JsonObject json = Json.createObjectBuilder()
                                    .add("name", "simpleMessage")
                                    .add("type", 3)
                                    .add("sender", "")
                                    .add("conv", conv)
                                    .add("fullname", chatService.getFullnameByLogin(username))
                                    .add("fullnamesList", fullnamesList)
                                    .add("username", username).build();

                            StringWriter w = new StringWriter();
                            try (JsonWriter writer = Json.createWriter(w)) {
                                writer.write(json);
                            }
                            SocketIOFrame fMessage = new SocketIOFrame(SocketIOFrame.FrameType.JSON_MESSAGE, SocketIOFrame.JSON_MESSAGE_TYPE, w.toString());
                            s.getBasicRemote().sendObject(fMessage);*/
                            List<String> params = new ArrayList<>();
                            params.add(Integer.toString(3));
                            params.add("");
                            params.add(conv);
                            params.add(chatService.getFullnameByLogin(username));
                            params.add(fullnamesList);
                            params.add(username);

                            WebsocketMessage wschangename = new WebsocketMessage("simpleMessage", params);
                            s.getBasicRemote().sendObject(wschangename);
                        }
                    }
                }
            }
        } else if (eventObject.getString("name").equals("modifyConvName")) {
            String previousName = eventObject.getString("previousName");
            String newName = eventObject.getString("newName");
            String conv = eventObject.getString("conv");

            String convParams[] = conv.split("-");
            String nameAndOwner[] = convParams[0].split("_");

            if (chatService.modifyConvName(previousName, newName, nameAndOwner[1])) {

                String newConv = newName + "_" + nameAndOwner[1] + "-" + convParams[1];

                for (Session s : openSessions.values()) {

                    if (s.isOpen() && !chatService.verifyParticipantInConv(newConv, getUserNameFromSession(s.getUserProperties()))) {

                        String fullnamesList = chatService.getParticipantsFullnames(newConv, getUserNameFromSession(s.getUserProperties()));

                        /*JsonObject json = Json.createObjectBuilder()
                                .add("name", "simpleMessage")
                                .add("sender", "")
                                .add("conv", newConv)
                                .add("oldConv", conv)
                                .add("type", 5)
                                .add("fullname", "")
                                .add("fullnamesList", fullnamesList).build();

                        StringWriter w = new StringWriter();
                        try (JsonWriter writer = Json.createWriter(w)) {
                            writer.write(json);
                        }
                        SocketIOFrame fMessage = new SocketIOFrame(SocketIOFrame.FrameType.JSON_MESSAGE, SocketIOFrame.JSON_MESSAGE_TYPE, w.toString());
                        s.getBasicRemote().sendObject(fMessage);*/
                        List<String> params = new ArrayList<>();
                        params.add(Integer.toString(5));
                        params.add("");
                        params.add(newConv);
                        params.add(conv);
                        params.add("");
                        params.add(fullnamesList);

                        WebsocketMessage wschangename = new WebsocketMessage("simpleMessage", params);
                        s.getBasicRemote().sendObject(wschangename);
                    }
                }
            }
        } else if (eventObject.getString("name").equals("convPicture")) {
            chatService.storeNewConvPicture(eventObject.getString("image"), eventObject.getString("conv"));
        }
    }

    /**
     * Function called when we close the socket
     *
     * @param session To get the current session
     * @throws java.io.IOException
     */
    @OnClose
    public void onClose(Session session) throws IOException {

        if (session != null && session.getUserPrincipal() != null) {
            openSessions.remove(session.getUserPrincipal().getName(), session);
        }
    }

    @OnError
    public void onError(Session session, Throwable thr) {
        Logger.getLogger(WebsocketServer.class).error("Websocket error: " + thr.getMessage());
    }
    
    public Map getSessionFromMap(Session session){
        Map<String,Session> temp = new HashMap<String,Session>();
        for(Map.Entry<String,Session> entry: openSessions.entrySet()){
            if(entry.getValue().getId().equals(session.getId())){
                temp.put(entry.getKey(), entry.getValue());
            }
        }
        
        return temp;
    }
    
    public String getUserNameFromSession(Map<String,Object> map){
        
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey().toString();
            
            Map<String,Object> value = (Map<String,Object>) entry.getValue();
            
             for (Map.Entry<String, Object> entry1 : value.entrySet()) {
                 if(entry1.getKey().equals("login")){
                     return entry1.getValue().toString();
                 }
             }
        }
        return null;
    }
}