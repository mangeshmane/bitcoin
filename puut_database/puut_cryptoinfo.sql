-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: puut
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cryptoinfo`
--

DROP TABLE IF EXISTS `cryptoinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cryptoinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `code` varchar(25) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `user` bigint(20) NOT NULL,
  `main` tinyint(4) DEFAULT '0',
  `identifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_cryptoinfo_1_idx` (`user`),
  CONSTRAINT `fk_cryptoinfo_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cryptoinfo`
--

LOCK TABLES `cryptoinfo` WRITE;
/*!40000 ALTER TABLE `cryptoinfo` DISABLE KEYS */;
INSERT INTO `cryptoinfo` VALUES (3,'mz1KtgvuAAHPuUtcGLfAxQjiKeAc88a553','BTC','BITCOIN',1,0,'SJTfz66sLqQxhyiWoU7wk'),(4,'muYwfF3N6fNMrgPD5hg2R7W3FU4hMcULEv','BTC','BITCOIN',1,0,'xZ7uCmmvd19xA5N9elVhB'),(5,'mqBVQuUw7MMG5nQ4TFLEAGKnsdARmcnK5S','BTC','BITCOIN',1,0,'DISxj8Tgd9r2VeHSdxWT3'),(6,'moZHyPqFLBS1R8QdiJPtwZLbbKn877b45i','BTC','BITCOIN',1,0,'tuVIfY679um9NDm0xkXdJ'),(7,'miwgzgRGZw1h2GyydDoU5HhVUwLFs1FxGv','BTC','BITCOIN',1,0,'NHwVMGCk8tPS1uJiHrFuJ'),(8,'mpHERMxpnq9vo6Q5VdWmnjNLVPg8JYfX2t','BTC','BITCOIN',1,0,'JigFZbGZnx2cOPHexN0Hz'),(9,'mi7DUK6XyudgcSbpiVijYfoNmWbFjMcQrV','BTC','BITCOIN',1,0,'49HBXXgw1B5bsKlEfPl4H'),(10,'my36hZSfrdSiguvRQoh6LHcMfCdAYFGjBC','BTC','BITCOIN',1,0,'wVXbzXotdvZtDSvuqhc8C'),(11,'mvGqnZ686xm3HHR32ZoaSDbwLBaaYr8GVC','BTC','BITCOIN',2,1,'tfaT3u9YS8tczefbLfB4h'),(12,'mnRxv5yoEHa9LDD6Kd8sNL6D9MVXoRPvWK','BTC','BITCOIN',2,0,'VBwriUlKNbCyzv8e2bcYm'),(13,'mumWGG8SH3zUN6N4esnrVCo4rK1sqEgXnH','BTC','BITCOIN',1,0,'klgice6eIL1rcMTK4h5vA'),(14,'mxaD9tB3XETjPP4RMqLzToE1Cj4PcLo737','BTC','BITCOIN',1,0,'mL3gV9BgDgPUlJbuiqonc'),(15,'munDimpjakX7tdA6ubWY9vScfV4mjm158R','BTC','BITCOIN',1,0,'F1boJMqnx1wUkybK58l3J'),(16,'mjJBMVpb9d9B4RorXhbon32cdTHJWZf7KK','BTC','BITCOIN',1,0,'BujlpH8A7eCoWbtCzmElF');
/*!40000 ALTER TABLE `cryptoinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 12:15:32
