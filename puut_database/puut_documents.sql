-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: puut
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` bigint(20) NOT NULL,
  `type` bigint(20) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `holder` varchar(120) DEFAULT NULL,
  `number` varchar(120) DEFAULT NULL,
  `cvv` varchar(45) DEFAULT NULL,
  `validfrom` date DEFAULT NULL,
  `validtill` date DEFAULT NULL,
  `country` bigint(20) DEFAULT NULL,
  `city` varchar(120) DEFAULT NULL,
  `designation` varchar(120) DEFAULT NULL,
  `companyname` varchar(120) DEFAULT NULL,
  `companyaddress` varchar(120) DEFAULT NULL,
  `phone` varchar(120) DEFAULT NULL,
  `mobile` varchar(120) DEFAULT NULL,
  `fax` varchar(120) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `authority` varchar(120) DEFAULT NULL,
  `subtype` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `category` bigint(20) DEFAULT NULL,
  `itinerary` varchar(120) DEFAULT NULL,
  `units` varchar(120) DEFAULT NULL,
  `seats` varchar(120) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `vaccinationname` varchar(120) DEFAULT NULL,
  `vaccinationdate` date DEFAULT NULL,
  `vaccinationcode` varchar(120) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `nationality` bigint(20) DEFAULT NULL,
  `debtor` varchar(120) DEFAULT NULL,
  `creaditor` varchar(120) DEFAULT NULL,
  `vatnumber` varchar(120) DEFAULT NULL,
  `invoicenumber` varchar(120) DEFAULT NULL,
  `amount` varchar(120) DEFAULT NULL,
  `currency` bigint(20) DEFAULT NULL,
  `payment` int(11) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL,
  `dateofissue` date DEFAULT NULL,
  `availability` bigint(20) DEFAULT NULL,
  `client` bigint(20) DEFAULT NULL,
  `merchant` bigint(20) DEFAULT NULL,
  `partner` bigint(20) DEFAULT NULL,
  `program` bigint(20) DEFAULT NULL,
  `shared` tinyint(1) DEFAULT NULL,
  `firstname` varchar(120) DEFAULT NULL,
  `lastname` varchar(120) DEFAULT NULL,
  `venue` varchar(120) DEFAULT NULL,
  `shop` varchar(120) DEFAULT NULL,
  `title` varchar(120) DEFAULT NULL,
  `airline` varchar(120) DEFAULT NULL,
  `address` varchar(120) DEFAULT NULL,
  `docdate` date DEFAULT NULL,
  `departure` date DEFAULT NULL,
  `boarding` date DEFAULT NULL,
  `location` varchar(120) DEFAULT NULL,
  `price` varchar(120) DEFAULT NULL,
  `flightnumber` varchar(120) DEFAULT NULL,
  `totalamount` varchar(120) DEFAULT NULL,
  `gate` varchar(120) DEFAULT NULL,
  `vatamount` varchar(120) DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `image` longblob,
  `photo` longblob,
  `tempimg` bigint(20) DEFAULT NULL,
  `btempimg` bigint(20) DEFAULT NULL,
  `rtempimg` bigint(20) DEFAULT NULL,
  `placeofbirth` varchar(120) DEFAULT NULL,
  `town` varchar(120) DEFAULT NULL,
  `web` varchar(120) DEFAULT NULL,
  `postzipcode` varchar(120) DEFAULT NULL,
  `companyslogan` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documents_1_idx` (`type`),
  KEY `fk_documents_2_idx` (`country`),
  KEY `fk_documents_3_idx` (`subtype`),
  KEY `fk_documents_4_idx` (`category`),
  KEY `fk_documents_6_idx` (`currency`),
  KEY `fk_documents_7_idx` (`status`),
  KEY `fk_documents_8_idx` (`availability`),
  KEY `fk_documents_9_idx` (`client`),
  KEY `fk_documents_10_idx` (`merchant`),
  KEY `fk_documents_11_idx` (`partner`),
  KEY `fk_documents_12_idx` (`program`),
  KEY `fk_documents_13_idx` (`tempimg`),
  KEY `fk_documents_14_idx` (`btempimg`),
  KEY `fk_documents_15_idx` (`rtempimg`),
  KEY `fk_documents_5_idx` (`nationality`),
  CONSTRAINT `fk_documents_1` FOREIGN KEY (`type`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_10` FOREIGN KEY (`merchant`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_11` FOREIGN KEY (`partner`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_12` FOREIGN KEY (`program`) REFERENCES `programs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_13` FOREIGN KEY (`tempimg`) REFERENCES `tempimages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_14` FOREIGN KEY (`btempimg`) REFERENCES `btempimages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_15` FOREIGN KEY (`rtempimg`) REFERENCES `rtempimages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_2` FOREIGN KEY (`country`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_3` FOREIGN KEY (`subtype`) REFERENCES `subtypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_4` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_5` FOREIGN KEY (`nationality`) REFERENCES `nationality` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_6` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_7` FOREIGN KEY (`status`) REFERENCES `statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_8` FOREIGN KEY (`availability`) REFERENCES `availabilities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_9` FOREIGN KEY (`client`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,1,'dsa','sd','1','sd','2016-11-03','2016-11-09',1,'sd','sd','asd','sd','ds','sd','sda','sd','sd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 12:15:31
